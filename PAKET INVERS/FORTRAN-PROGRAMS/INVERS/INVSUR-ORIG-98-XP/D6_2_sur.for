$LARGE                                                                  
C     ============================================================      
C     =              �PO�PAMMA INVGRAF                           =      
C     = PE�EH�E O�PATHO� K�HEMAT��ECKO� �B�XMEPHO� �A�A��        =      
C     = B ��HEAP��OBAHHO�  �OCTAHOBKE  ��� PE�PA��POBAHH�X BO�H  =      
C     = *** PE����TATOM PA�OT� �PO�PAMM� �B��ETC� CKOPOCTHO�     =
C     = PA�PE� V(X,Z), KOTOP�� ����� ���� ����������� � ����     = 
C     = ���������, �A��C�BAE��� � ����� ������������ ����������� =      
C     = (������������ ������� ����������� ���������� GRAF6.EXE)  =      
C     ============================================================      
C                                                                       
C     ���K �PO�PAMM�POBAH�� FORTRAN-Microsoft 5.0                       
C     System   INVERS   Version 6.3                                   
C     Copyright (C)  Sheludko 1993. All rights reserved.                
C     OP�AH��A��� - ��� CO �AH                                      
C                                                                       
C                  BXO�H�E �APAMETP�                                    
C     *****************************************************************
C     * SECTNAME - CHARACTER, �������� �������                        *
C     * G - INTEGER, ��C�O ��O��H�� �O�� BPEMEH           ( G < 33 )  * 
C     * NN- INTEGER, ��C�O OTC�ETOB �O�� BPEMEH �O OC� X  (NN < 150)  * 
C     * MIX - REAL, �EBA� �PAH��A �A�AH�� �O�� BPEMEH �O OC� X, KM    * 
C     * MAX - REAL, �PABA� �PAH��A �A�AH�� �O�� BPEMEH �O OC� X, KM   * 
C     * HX1 - REAL, �A� �A�AH�� �O�� BPEMEH �O OC� X, KM              * 
C     * MIZ - REAL, A�CO��THA� OTMETKA �POBH� �P�BE�EH�� �HA�EH��     * 
C     *             �O�� BPEMEH (� COOTBETCTBEHHO PA�PE�A) KM         * 
C     * T1(G,NN) - REAL,MACC�B �HA�EH�� �O�� BPEMEH, C                * 
C     * L0(G) - REAL, MACC�B BE����H �A� �OH��POBAH��, KOTOP�M        * 
C     *               COOTBETCTB��T ��O��H�� �O�� BPEMEH, KM          * 
C     * N - INTEGER,KO���ECTBO ���OB CETK� �HTEP�O����� OC� X         * 
C     * M - INTEGER,KO���ECTBO ���OB CETK� �HTEP�O����� OC� Z         * 
C     * XI(N)- REAL, X-KOOP��HAT� TO�EK CETK� �HTEP�O�����            * 
C     * ZI(M) - REAL, Z-KOOP��HAT� �AHH�X TO�EK                       * 
C     * HX2 - REAL, �A� CETK� �O X, �A�AETC� EC�� OH �OCTO�HEH        * 
C     * HZ2 - REAL, �A� CETK� �O Z, �A�AETC� EC�� OH �OCTO�HEH        * 
C     * MBX - REAL, MAC�TA� ��O�PA�EH�� CKOPOCTHO�O PA�PE�A �O OC� X  * 
C     * MBZ - REAL, MAC�TA� ��O�PA�EH�� CKOPOCTHO�O PA�PE�A �O OC� Z  *
C     * IFRAG - INTEGER, ������ (=1) ��� �������� (=0) ������         *
C     *                  ������������ ����������� ����������� ������� *
C     * ISYMP - INTEGER, ������ (=1) ��� �������� (=0) ������         *
C     *         ������������ ����������� �������-����������. �������  *
C     * IFLTR - INTEGER, ������ (=1) ��� �������� (=0) ������         *
C     *                  ������������ ����������� ����������� ������� *
C     * KOD - INTEGER, EC�� KOD=1,TO PA�PE�AETC� ����������HA� �E�AT�,* 
C     *                EC�� KOD=0 - �E�AT� HET                        * 
C     * KOD1- INTEGER,(0/1), EC�� KOD1=1, TO BBO��TC� VPMAX(G),       * 
C     *                      EC�� KOD1=0, -  MACC�B �� ��������       * 
C     * VPMAX(G) - REAL, MACC�B MAKC�MA��HO �O��CT�M�X �HA�EH��       * 
C     *         KA����XC� CKOPOCTE� (��� COOTB. ��O��H�� �O�� BPEMEH) * 
C     *                                                               * 
C     ***************************************************************** 
C                                                                       
C                  �������� ������                                      
C     ***************************************************************** 
C     *        ���������� ������, ������������ �� "INVERS"            *    
C     * ============================================================= * 
C     * WR(NR) - REAL, MACC�B KOOP��HAT TO�EK X,Z                     * 
C     *          � �HA�EH�� ��HK��� V B �AHH�X TO�KAX;                * 
C     * �HA�EH�� MACC�BA WR �A�A�TC� B C�E����EM �OP��KE X,Z,V;       * 
C     * HEO�PE�E�EHH�E Z � V �P�PABH�BA�TC� K H���                    * 
C     * NR - INTEGER, KO���ECTBO ��EMEHTOB MACC�BA WR                 * 
C     * ------------------------------------------------------------- *  
C     * G - INTEGER, ��C�O �POBHE� O�PE�E�EH�� ��CKPETHO�O CKOPOCT-   * 
C     *              HO�O PA�PE�A, ������������� �� "INVERS"          * 
C     * NN- INTEGER, ��C�O KO�OHOK CKOPOCTHO�O PA�PE�A �O OC� X       * 
C     * V(G,NN) - REAL, �HA�EH�� CKOPOCT�, PAC��TAHH�E �O "INVERS"    * 
C     * Z(G,NN) - REAL, COOTBETCTB����E �M �����H�                    * 
C     * ZMAX(NN)- REAL, MACC�B  MAKC�MA��H�X �����H O�PE�E�EH��       * 
C     *    PA�PE�A �OCPE�CTBOM INVERS (B�O�� OC� X, C �A�OM - HX1)    * 
C     * IZMAX(NN) - REAL, MACC�B �H�EKCOB H��HE� �PAH��� O��ACT�      * 
C     * ============================================================= *
C     *        ������, ������������� �� �����������  �����            *
C     * ============================================================= *  
C     * MN - INTEGER, PA�MEPHOCT� O�HOMEPHO�O MACC�BA, �����������-   * 
C     *               ����� ���� ��������� �����                      * 
C     * VI(MN) - REAL, O�HOMEPH�� MACC�B �HTEP�O��POBAHH�X �HA�EH��   * 
C     *      CKOPOCT�; HEO�PE�E�EHH�M �HA�EH��M COOTBETCTB��T H���    * 
C     * VF(MN) - REAL, O�HOMEPH�� MACC�B ���������� �HA�EH�� ��������;*
C     *                HEO�PE�E�EHH�M �HA�EH��M COOTBETCTB��T H���    * 
C     *                                                               * 
C     ***************************************************************** 
C                                                                       
C     +++ O��CAH�� ��� �PO�PAMM� INVERS +++                             
      DIMENSION T1(33,150),L0(33),VPMAX(33),WR(10000),                  
     *          ZM0(33,150),ZMAX(150),IZMAX(150)                        
      INTEGER G,NN,KOD,KOD1,NR,IZMAX                                    
      REAL MIX,MAX,HX1,L0,MBX,MBZ,MIZ                                   
C                                                                       
C     +++ O��CAH�� CETK� �HTEP�O����� � ����TPA��� +++                  
      DIMENSION  XI(640),ZI(100),VI(12288),V(100,200),Z(100,200)        
      INTEGER M,N                                                       
      REAL XS,ZS,VS,HX2,HZ2                                             
C                                                                       
C*    *** O��CAH�� ��� �/� ����TPA��� (�ACT� ��E O��CAHA B��E) ***      
      DIMENSION VF(12288),IZFMAX(640),                                  
     *          ZFMAX(200)                                              
      REAL DXF1,DZF1,DXF2,DZF2,ZF2                                      
C                                                                       
C     +++ O��CAH�� ��� ��OKA FRAG � �/� RERYCOLOR +++                   
      DIMENSION X1P(640),X2P(640)                                       
      INTEGER MP,NP,IFRAG,ISYMP,IFILTR,MINV,MAXV                        
      INTEGER*2 IVGRAF(640)                                             
      REAL HXP,HZP,PMIX,PMAX,PMIZ,PMAZ                                  
C                                                                       
      CHARACTER*60 FNAMEIN,FNAMEOUT1,FNAMEOUT2                          
      CHARACTER*65 SECTNAME
C                                                                       
C                   ���� ����� 
C     ================================================================
      WRITE(*,1000)                                                     
 1000 FORMAT(//1X,'�� ��������� � ���������� ��������� ������ '/,
     *         1X,'��������������� ����  -  "INVERS", Version 6.3 '/,
     *       1X,'Copyright (C) Sheludko 1982-1993.',
     *          ' All rights reserved.'/////)   
C     ================================================================
C                                                                       
      WRITE(*,1)                                                        
    1 FORMAT(1X,'������� ��� ����� ������ ����� - '\)                   
      READ(*,'(A)') FNAMEIN                                             
C                                                                        
      WRITE(*,2)                                                        
    2 FORMAT(1X,'������� ��� ����� ������ �������� ������ - '\)
      READ(*,'(A)') FNAMEOUT1
C                                                                       
      OPEN(5,FILE=FNAMEIN,STATUS='OLD',FORM='FORMATTED',RECL=80)
      OPEN(6,FILE=FNAMEOUT1,STATUS='NEW',FORM='FORMATTED')
C
      READ(5,3) SECTNAME,G,NN,MIX,MAX,HX1,MIZ
    3 FORMAT(/5X,A65/1X,I7/1X,I7/1X,F8.3/1X,F8.3/1X,F7.3/1X,F7.3)
C                                                                       
C     *** BBO� MACC�BA L0(G) ***
      READ(5,4)
    4 FORMAT(/)
      READ(5,30)(L0(J),J=1,G)                                           
   30 FORMAT(10F8.3)                                                    
C                                                                       
C     *** BBO� �O�� BPEMEH T(G,NN) ***
      READ(5,5)
    5 FORMAT(/) 
      DO 60 J=1,G                                                       
      READ(5,70)(T1(J,I),I=1,NN)                                        
   60 CONTINUE                                                          
   70 FORMAT(10F7.3)                                                    
C
C
      READ(5,6) HX2,HZ2,HXP,HZP,DXF1,DZF1,DXF2,DZF2,ZF2,
     *          IFRAG,ISYMP,IFILTR,KOD,KOD1
    6 FORMAT(/1X,F7.3/1X,F7.3/1X,F7.4/1X,F7.4/1X,F7.3/1X,F7.3/
     *        1X,F7.3/1X,F7.3/1X,F7.3/
     *        1X,I7/1X,I7/1X,I7/1X,I7/1X,I7//) 
C
      IF(KOD1.EQ.1) READ(5,7) (VPMAX(I),I=1,G)
    7 FORMAT(10F6.3)
C     ================================================================
C     ================================================================
      write(6,10) SECTNAME,G,NN,MIX,MAX,HX1,MIZ
   10 format(5x,'�E�AT� BXO�H�X �AHH�X'/
     #  5x,A65/
     # 1x,i7,5x,'G - �᫮ �������� ���� �p����, integer'/
     # 1x,i7,4x,'NN - �᫮ ����⮢ ���� �p���� �� �� x, integer'/
     # 1x,f8.3,2x,
     # 'MIX - ����� �࠭�� ������� ���� �६�� �� �� x, real [��] '/
     # 1x,f8.3,2x,
     # 'MAX - �ࠢ�� �࠭�� ������� ���� �६�� �� �� x, real [��]'/
     # 1x,f7.3,3x,'HX1 - 蠣 ������� ���� �p���� �� �� x, real [��]'/
     # 1x,f7.3,3x,
     # 'MIZ - �p����� �p�������� ���� �p���� � p��p��� �� �� z, [��]')
      write(6,931)
  931 format(/8x,'L0(G) - ���ᨢ ��� �����஢���� (10F8.3)') 
      write(6,941) (L0(J),J=1,G)
      write(6,932)
  932 format(/6x,'T(G,NN) - ���� �६�� (10F7.3)') 
      do 940 J=1,G
      write(6,942) (T1(J,I),I=1,NN)
  940 continue
  941 format(10f8.3)
  942 format(10f7.3)
      write(6,950) HX2,HZ2,HXP,HZP,DXF1,DZF1,DXF2,DZF2,ZF2,
     #             IFRAG,ISYMP,IFILTR,KOD,KOD1
  950 format(/
     # 1x,f7.3,3x,
     #   'HX2 - 蠣 �� �� x ࠢ����୮� �⪨ ���௮��樨, real [��]'/
     # 1x,f7.3,3x,
     #   'HZ2 - 蠣 �� �� z ࠢ����୮� �⪨ ���௮��樨, real [��]'/
     # 1x,f7.4,3x,
     #   'MBX = HXP / ����⠡� ����᪮�� ����ࠦ���� �� ��� x � z'/
     # 1x,f7.4,3x,
     #   'MBZ = HZP \ (�᫮ �� � ����� ���ᥫ�), real [��]'/
     # 1x,f7.3,2x,'DXF1 - \ ࠧ���� ���� ��।���饣� ᣫ��������,[��]'/
     # 1x,f7.3,2x,'DZF1 - /         1-� ����                       '/
     # 1x,f7.3,2x,'DXF2 - \                                        '/
     # 1x,f7.3,2x,'DZF2 - /         2-� ����                       '/
     # 1x,f7.3,3x,'ZF2 - �஢���, ࠧ�����騩 ������ ࠧ��筮�� ',
     #            'ᣫ��������, [��]'/
     # 1x,i7,1x,'IFRAG - ������(=1) ��� �⬥��� (=0) ���� ',
     #          '���.��ࠧ� ����. ࠧ१�'/
     # 1x,i7,1x,'ISYMP - ������(=1) ��� �⬥��� (=0) ���� ',
     #          '���.��ࠧ� ���.���. ࠧ१�'/
     # 1x,i7,1x,'IFLTR - ������(=1) ��� �⬥��� (=0) ���� ',
     #          '���.��ࠧ� ᣫ����. ࠧ१�'/
     # 1x,i7,3x,
     #   'KOD - ࠧp�蠥� (=1) ��� ���p�頥� (=0) �p������. �����'/
     # 1x,i7,2x,
     #   'KOD1 - p��p�蠥� (=1) ��� ���p�頥� (=0) ���� VPMAX(G)')
C
      IF(KOD1.EQ.1) WRITE(6,8)
    8 FORMAT(/4x,'VPMAX(G) - ���ᨢ ', 
     #    '��࠭�祭�� ���⭮�� ����. �ࠤ���� ���� �६�� 10F6.3')
C
      IF(KOD1.EQ.1) WRITE(6,7) (VPMAX(I),I=1,G)
  
      WRITE(*,15000)
15000 FORMAT(/1X,'PROCESSING IS BEING EXECUTED, ...  WAIT')
C=
C=    WRITE(*,15000)
C=15000 FORMAT(/1X,'���� ���������,  ...  �����')
C     =================================================================
C                                                                       
      CALL INVERS(T1,L0,MIX,MAX,HX1,G,NN,VPMAX,KOD,KOD1,WR,NR,ZM0,ZMAX) 
C                                                                       
C     *** �A�AH�E MACC�BOB Z(G,NN),V(G,NN) ***                          
      DO 111 J=1,G                                                      
      DO 111 I=1,NN                                                     
      Z(J,I)=WR(((J-1)*NN+I-1)*3+2)                                     
      V(J,I)=WR(((J-1)*NN+I-1)*3+3)                                     
  111 CONTINUE                                                          
C                                                                       
C     *** B�BO� MACC�BOB V(G,NN),Z(G,NN),ZM0(G,NN),ZMAX(NN) ***         
C*    WRITE(6,120)                                                      
C*120 FORMAT(/1X,'V(G,NN)'//)                                           
C*    DO 121 J=1,G                                                      
C*    WRITE(6,130) (V(J,I),I=1,NN)                                      
C*121 CONTINUE                                                          
  130 FORMAT(10F7.2)                                                    
C                                                                       
C*    WPITE(6,140)                                                      
C*140 FORMAT(/1X,'Z(G,NN)'//)                                           
C*    DO 141 J=1,G                                                      
C*    WRITE(6,150) (Z(J,I),I=1,NN)                                      
C*141 CONTINUE                                                          
  150 FORMAT(10F7.2)                                                    
C                                                                       
C     *** �E�AT� �����H H��H�X KPOMOK ��EEK ***                         
C*    DO 151 J=1,G                                                      
C*    WRITE(6,152) (ZM0(J,I),I=1,NN)                                    
C*151 CONTINUE                                                          
C*152 FORMAT(10F7.2)                                                    
C                                                                       
C                                                                       
      WRITE(6,160)                       !!! new without C                               
  160 FORMAT(/1X,'ZMAX(NN)'//)           !!! new without C                                 
C+    WRITE(6,161) (ZMAX(I),I=1,NN)                                     
C+161 FORMAT(20F6.3)                                                    
C
C***      OPEN(12,FILE='ZMAX.DAT',STATUS='NEW',FORM='FORMATTED',RECL=80)
      OPEN(12,FILE='ZMAX.DAT')		   !!! new
C									   !!! new
	WRITE(12,1612) NN       		   !!! new
	DO 1611 I=1,NN  				   !!! new
	II=NN-I+1						   !!! new
	XZM=MIX+(II-1)*HX1				   !!! new
	WRITE(12,1613) XZM,ZMAX(II)		   !!! new
 1611 CONTINUE                           !!! new
 1612 FORMAT(1X,I7)                      !!! new
 1613 FORMAT(2F10.3)                     !!! new
C                                        !!! new                               
C***      CLOSE(12,STATUS='KEEP')        !!! new  	
C                                                                       
C     *** O�PE�E�EH�E MACC�BA �H�EKCOB IZMAX(NN) ***                    
      DO 162 I=1,NN                                                     
      IZMAX(I)=G                                                        
      DO 162 J=2,G                                                      
      IF(Z(J,I).EQ.0.0.AND.Z(J-1,I).NE.0.0) IZMAX(I)=J-1                
  162 CONTINUE                                                          
C                                                                       
C+    WRITE(6,170)                                                      
C+170 FORMAT(/1X,'IZMAX(NN)'//)                                         
C+    WRITE(6,171) (IZMAX(I),I=1,NN)                                    
C+171 FORMAT(20I6)                                                      
C                                                                       
C     *** O�PE�E�EH�E PA�MEPHOCT� �P�MO��O��HO� CETK� M,N ***           
C                                                                       
      YMAX=0.0                                                          
      DO 172 I=1,NN                                                     
      IF(ZMAX(I).GT.YMAX) YMAX=ZMAX(I)                                  
  172 CONTINUE                                                          
C                                                                       
      M=INT(YMAX/HZ2+1.E-5)+1                                           
      N=INT(HX1*(NN-1)/HX2+1.E-5)+1                                     
C     *** O�PE�E�EH�E PA�MEPHOCT� CETK� PERYCOLOR MP,NP ***             
      MP=INT(YMAX/HZP+1.E-5)+1                                          
      NP=INT(HX1*(NN-1)/HXP+1.E-5)+1                                    
      MPFR=MP+30                                                        
      PMIX=MIX                                                          
      PMAX=MIX+(NP-1)*HXP                                               
      PMIZ=MIZ                                                          
      PMAZ=MIZ+(MPFR-1)*HZP                                             
C                                                                       
C     ### ��������� ��� ������� ������� �� ���������� �������� ������ ###
C     ### � ���������� � �������� �������������� ������ ��� ��������� ###                                                                 
C     ### ��������, �������� ������ ������ MIZ=0.0,                   ###
C     ### �������� PMIZ=MIZ                                           ###
      MIZ=0.0                                                                 
C                                                                       
      IF(IFRAG.EQ.0) GOTO 2007                                          
C     ============================================================      
C     *** PAC�ET �PA�MEHTAPHO �A�AHHO�O PAC�PE�E�EH�� CKOPOCT� ***      
C     *** ��� P�-COLOR �O �POBH�M CETK� JJ*640 � �A��C� HA     ***      
C     *** HA ����, � ����� ���������� (*100)                   ***      
C     ============================================================      
      XH=MIX                                                            
      NN2=NN/2                                                          
      MINV=INT(V(1,NN2)*100+1.E-5)                                      
      MAXV=MINV                                                         
C                                                                       
C     *** ��ABH�E JJ- � II-��K�� �EPE�OPA KOOP��HAT CETK� ***           
      DO 2005 JJ=1,MPFR                                                 
      X2P(JJ)=MIZ+(JJ-1)*HZP                                            
      DO 3000 II=1,NP                                                   
      IVGRAF(II)=0                                                      
 3000 CONTINUE                                                          
C                                                                       
      DO 2003 II=1,NP                                                   
      X1P(II)=XH+(II-1)*HXP                                             
C     *** �CTAHOB�EH�E X-�O�O�EH�� TO�K� ***                            
      DO 2001 I=1,NN                                                    
      IF(X1P(II).GE.XH+HX1*((I-1)-0.5).AND.                             
     *  X1P(II).LT.XH+HX1*((I-1)+0.5)) IX=I                             
 2001 CONTINUE                                                          
      I=IX                                                              
C                                                                       
C     *** �CTAHOB�EH�E Z-�O�O�EH�� TO�K� ***                            
      IF(X2P(JJ).GE.0.0.AND.X2P(JJ).LT.ZM0(1,I)) IVGRAF(II)=            
     #   INT(V(1,I)*100+1.E-5)                                          
C**** IF(X2P(JJ).LT.ZM0(1,I  ??? KPA�H�� �EB�� ???                      
      DO 2002 J=2,G                                                     
      IF(X2P(JJ).GE.ZM0(J-1,I).AND.X2P(JJ).LT.ZM0(J,I)) IVGRAF(II)=     
     #   INT(V(J,I)*100+1.E-5)                                          
 2002 CONTINUE                                                          
C                                                                       
C     ***  B���C�EH�E MIN-�O � MAX-�O �HA�EH�� MACC�BA IVGRAF(640) ***  
      IF(IVGRAF(II).NE.0.AND.IVGRAF(II).LT.MINV) MINV=IVGRAF(II)        
      IF(IVGRAF(II).GT.MAXV) MAXV=IVGRAF(II)                            
 2003 CONTINUE                                                          
C     *** KOHE� II-��K�A ******                                         
C                                                                       
C     ================================================================= 
C     *** �OCTPO�HA� �A��C� HA ���� MACC�BA IVGRAF(640) - JJ-CTPOKA *** 
C                                                                       
      OPEN(10,FILE='DISC.DAT',STATUS='NEW',FORM='BINARY',RECL=640)
      WRITE(10) (IVGRAF(I),I=1,NP)                                      
      IF(JJ.NE.1.AND.JJ.NE.MPFR) GOTO 2005                              
C++   WRITE(6,2004)                                                     
C2004 FORMAT(//2X,'FRAG=1  MACC�B IVGRAF(640) 1-� � �OC�E�H�� �POBH�'/) 
C++   WRITE(6,2105) (IVGRAF(I),I=1,NP)                                  
C2105 FORMAT(10F6.2)                                                    
C     ================================================================= 
C                                                                       
 2005 CONTINUE                                                          
C     *** KOHE� JJ-��K�A *************                                  
C                                                                       
      WRITE(*,2009)          
 2009 FORMAT(/1X,'GRAPHICAL ARRAY OF DISCRETE SECTION '/
     *         1X,'HAS BEEN WRITTEN TO THE FILE   DISC.DAT'/)
C                                                                       
      CLOSE(10,STATUS='KEEP')                                           
C
C     *** �E�AT� MIN-�O � MAX-�O �HA�EH�� IRERY ****                    
C++   WRITE(6,2006) MINV,MAXV                                           
C2006 FORMAT(2X,'MINVP = ',I5,5X,'MAXVP = ',I5)                         
C     ================================================================= 
 2007 CONTINUE                                                          
C                                                                       
      IF(ISYMP.NE.1) GOTO 2020                                          
      FNAMEOUT2='LINT.DAT'                                              
      OPEN(10,FILE='LINT.DAT',STATUS='NEW',FORM='BINARY',RECL=1280)     
      CALL PERICL(MP,NP,MIX,MIZ,HXP,HZP,MINV,MAXV,            
     *              G,NN,V,Z,HX1,ZMAX,IZMAX)                            
C                                                                       
      WRITE(*,2010)          
 2010 FORMAT(/1X,'GRAPHICAL ARRAY OF LINEAR-INTERPOLATED'/        
     *        1X,'SECTION HAS BEEN WRITTEN TO THE FILE   LINT.DAT'/)                  
C                                                                       
      CLOSE(10,STATUS='KEEP')                                           
C                                                                       
C     *** PAC�ET MACC�BOB KOOP��HAT �P�MO��O��HO� CETK� ***             
 2020 DO 173 I=1,N                                                      
      XI(I)=MIX+(I-1)*HX2                                               
  173 CONTINUE                                                          
C                                                                       
      DO 174 J=1,M                                                      
      ZI(J)=MIZ+(J-1)*HZ2                                               
  174 CONTINUE                                                          
C                                                                       
C     *** BBO� MACC�BOB �P�MO��O��HO� CETK� XI(N),ZI(M) � �X �E�AT� *** 
C*    READ(5,180) (XI(I),I=1,N)                                         
C*    READ(5,180) (ZI(J),J=1,M)                                         
  180 FORMAT(10F7.1)                                                    
      WRITE(6,190)                                                      
  190 FORMAT(/1X,'KOOP��HAT� TO�EK �HTEP�O����� XI(N),ZI(M)'//)         
      WRITE(6,180) (XI(I),I=1,N)                                        
      WRITE(6,180) (ZI(J),J=1,M)                          
C                                                                       
      MN=M*N                                                            
C                                                                       
      IF(MN.GT.12288) WRITE(*,195)          
  195 FORMAT(/1X,'��������!'/
     *        1X,'����� �� �࠭��� ���ᨢ��  (VF(MN), M*N=MN > 12288)'/
     *        1X,'��������� 蠣 ������� �⪨ HX2 ��� HZ2'
     *        1X,'(� ����� ���� ⠪�� ࠧ��� ���� ᣫ��������)'/
     *        1X,'��������� �ணࠬ�� ᭮��'/)
      IF(MN.GT.12288) goto 2070          
C                                                                       
C     *** �A�AH�E TEK��E� TO�K� �HTEP�O����� HA �P�MO��O��HO� CETKE *** 
C     *** � PAC�ET �HTEP�O��POBAHH�X �HA�EH��  VI(JI)=VS            *** 
      DO 200 JJ=1,M                                                     
      ZS=ZI(JJ)                                                         
      DO 200 II=1,N                                                     
      XS=XI(II)                                                         
      VS=0.0                                                            
C                                                                       
      CALL SYMPL(NN,G,V,Z,MIX,MIZ,HX1,ZMAX,IZMAX,XS,ZS,VS)              
C                                                                       
      JI=(JJ-1)*N+II                                                    
      VI(JI)=VS                                                         
  200 CONTINUE                                                          
C                                                                       
C     *** �E�AT� PE����TATOB �HTEP�O����� ***                           
      WRITE(6,210)                                                      
  210 FORMAT(/1X,'PE����TAT 2-X MEPHO� ��H. �HTEP�O����� VI(MN)'//)     
      DO 211 J=1,M                                                      
      JI1=(J-1)*N+1                                                     
      JI2=J*N                                                           
C++   WRITE(6,220) (VI(I),I=JI1,JI2)                                    
  211 CONTINUE                                                          
  220 FORMAT(1X,20F6.2)                                                 
C      
C         ----------------------------------------------------                                                              
C     *** ������������ � ������ � ���� LINEAR-INTERPOLATED    ***
C     *** ����������� ������������� ��� ��������� ����������� *** 
C     *** ������ SURFER                                       *** 
C                                                                
      ISURF=1                                                                       
      IF(ISURF.NE.1) GOTO 6000                                                                       
C***      OPEN(13,FILE='SURLINT',STATUS='NEW',FORM='FORMATTED',RECL=80)                                                                       
      OPEN(13,FILE='SURLINT')                         !!! new                                              
C                                                     !!! new 
	Nsur=0
      DO 221 J=1,M                                                                 
      DO 221 I=1,N                                                                 
      JI=(J-1)*N+I                                                                 
      ZSUR=-ZI(J)
      IF(VI(JI).EQ.0.0) GOTO 221
C==>  WRITE(13,222) XI(I),ZI(J),VI(JI)                                                                 
      WRITE(13,222) XI(I),ZSUR,VI(JI) 
	Nsur=Nsur+1                                                                
  221 CONTINUE                                                                       
  222 FORMAT(3F10.3) 
C                                                                  
      WRITE(13,223) Nsur 
  223 FORMAT(1X,I7) 
C                                                                       
C***      CLOSE(13,STATUS='KEEP')                       !!! new                                                             
 6000 CONTINUE                                                                      
C     *** �������� ����� ������ LINT-������ ��� ������ SURFER  ***                                                                  
C         ----------------------------------------------
C                                                                       
C     *** ��OK ����TPA��� ***                                           
C                                                                       
C     *** O�PE�E�EH�E �H�EKCOB H��HE� �PAH��� O��ACT� �HTEP�O����� ***  
      DO 230 I=1,N                                                      
      IZFMAX(I)=M                                                       
  230 CONTINUE                                                          
      DO 240 J=2,M                                                      
      DO 240 I=1,N                                                      
      JI=(J-1)*N+I                                                      
      J1=(J-2)*N+I                                                      
      IF(VI(JI).EQ.0.0.AND.VI(J1).NE.0.0) IZFMAX(I)=J-1                 
  240 CONTINUE                                                          
C                                                                       
C     *** ����TPA��� C OKHOM DXF,DZF ***                                
      CALL FILTR(M,N,MN,VI,HX2,HZ2,IZFMAX,DXF1,DZF1,DXF2,DZF2,ZF2,VF)   
C                                                                       
C     *** �E�AT� PE����TATA ����TPA��� ***                              
      IF(KOD.EQ.0) GOTO 25000                                           
      WRITE(6,250)                                                      
  250 FORMAT(/1X,'PE����TAT ����TPA��� VF(MN)'//)
      DO 251 J=1,M                                                      
      JI1=(J-1)*N+1                                                     
      JI2=J*N                                                           
      WRITE(6,260) (VF(I),I=JI1,JI2)                                    
  251 CONTINUE                                                          
  260 FORMAT(10F6.3)                                          
C      
C         ----------------------------------------------------                                                              
C     *** ������������ � ������ � ���� ����������� �����������  ***                                                                  
C     *** ������������� ��� ��������� ����������� ������ SURFER ***                                                                        
C                                                                       
      ISURF=1                                                                       
      IF(ISURF.NE.1) GOTO 5000                                                                       
C***      OPEN(11,FILE='SURFLTR',STATUS='NEW',FORM='FORMATTED',RECL=80)                                                                       
      OPEN(11,FILE='SURFLTR')                         !!! new                                              
C                                                     !!! new                     
	Nsur=0
      DO 261 J=1,M                                                                 
      DO 261 I=1,N                                                                 
      JI=(J-1)*N+I                                                                 
      ZSUR=-ZI(J)
      IF(VF(JI).EQ.0.0) GOTO 261
C==>  WRITE(11,262) XI(I),ZI(J),VF(JI)                                                                 
      WRITE(11,262) XI(I),ZSUR,VF(JI) 
	Nsur=Nsur+1                                                                
  261 CONTINUE                                                                       
  262 FORMAT(3F10.3) 
C                                                                  
      WRITE(11,263) Nsur 
  263 FORMAT(1X,I7) 
C                                                                       
C***      CLOSE(11,STATUS='KEEP')                       !!! new                                                             
 5000 CONTINUE                                                                      
C     *** �������� ����� ������ ������ ��� ������ SURFER        ***                                                                  
C         ----------------------------------------------
C                                                              
C     *** O�PE�E�EH�E �APAMETPOB, HEO�XO��M�X SYMP()  ***               
C     *** ��� �HTEP�O����� PE����TATOB ����TPA���     ***               
C                                                                       
25000 DO 270 I=1,N                                                      
      ZFMAX(I)=MIZ+(IZFMAX(I)-1)*HZ2                                    
C     ( �O�T�C� HEO�OCHOBAHHO �A�AHH�X IZFMAX(I)=M )                    
C                                                                       
      DO 270 J=1,M                                                      
      Z(J,I)=0.0                                                        
      V(J,I)=0.0                                                        
      IF(J.GT.IZFMAX(I)) GOTO 270                                       
      Z(J,I)=MIZ+(J-1)*HZ2                                              
      V(J,I)=VF((J-1)*N+I)                                              
  270 CONTINUE                                                          
C                                                                       
C     *** �A��C� HA M� ��� PERYCOLOR  ***                               
C                                                                       
      IF(IFILTR.NE.1) GOTO 2040                                         
      FNAMEOUT2='FILTR.DAT'                                             
      OPEN(10,FILE='FILTR.DAT',STATUS='NEW',FORM='BINARY',RECL=1280)
      CALL PERICL(MP,NP,MIX,MIZ,HXP,HZP,MINV,MAXV,            
     *              M,N,V,Z,HX2,ZFMAX,IZFMAX)                           
C                                                                       
      WRITE(*,2030)          
 2030 FORMAT(/1X,'GRAPHICAL ARRAY OF SMOOTHED SECTION'/
     *         1X,'HAS BEEN WRITTEN TO THE FILE   FILTR.DAT'/)
C                                                                       
      CLOSE(10,STATUS='KEEP')                                           
C
C     *** ������ ����� ���������� ������� ***
 2040 HXC=100.0
      HZC=10.0                                                                       
      OPEN(8,FILE='PARSEC.DAT',STATUS='NEW',FORM='FORMATTED',RECL=80)
       WRITE(8,2050) SECTNAME,PMIX,PMAX,PMIZ,PMAZ,HXP,HZP,NP,MP,
     # MINV,MAXV,HXC,HZC
 2050 FORMAT(5X,'��������� ������������ �����������'/
     #  5X,'��� ����������� �������'/
     #  5X,A65/
     #  1X,'��砫� �p�䨫� [��]                  XH  - ',F6.1/
     #  1X,'����� ��䨫� [��]                   XK  - ',F6.1/
     #  1X,'�������쭠� ��㡨��  [��]            ZH  - ',F6.1/
     #  1X,'���ᨬ��쭠� ��㡨�� [��]            ZK  - ',F6.1/
     #  1X,'蠣 ���p�⨧�樨 �� �� x [��]      HX2 - ',F6.1/
     #  1X,'蠣 ���p�⨧�樨 �� �� z [��]      HZ2 - ',F6.2/
     #  1X,'�᫮ ����⮢ �� �� x               NI - ',I3/
     #  1X,'�᫮ ����⮢ �� �� z               MI - ',I3/
     #  1X,'�������쭠� ᪮p���� [��/�] * 100   MINV - ',I4/
     #  1X,'���ᨬ��쭠� ᪮p���� [��/�] * 100  MAXV - ',I4/
     #  1X,'蠣 ���஢�� �⪨ �� �� x [��]    HXC - 'F6.1/
     #  1X,'蠣 ���஢�� �⪨ �� �� z [��]    HZC - 'F6.1)
C                                                                       
      WRITE(*,2060)
 2060 FORMAT(/1X,'PARAMETERS OF SECTIONS '/
     *         1X,'HAVE BEEN WRITTEN TO THE FILE   PARSEC.DAT'/)
C
      CLOSE(8,STATUS='KEEP')
C
 2070 CLOSE(5,STATUS='KEEP')
      CLOSE(6,STATUS='KEEP')
C                                                                       
      STOP                                                              
      END                                                               
C                                                                       
C     ----------------------------------------------------------------- 
C                         INVERS                                        
C     ================================================================= 
C     = �O��PO�PAMMA PE�EH�� O�PATHO� K�HEMAT��ECKO� �B�XMEPHO� �A�A��= 
C     = B ��HEAP��OBAHHO�  �OCTAHOBKE  ��� PE�PA��POBAHH�X BO�H       = 
C     ================================================================= 
C                                                                       
C                  BXO�H�E �APAMETP�                                    
C     ***************************************************************** 
C     * T1(G,NN) - REAL,MACC�B �HA�EH�� �O�� BPEMEH, C                * 
C     * L0(G) - REAL, MACC�B BE����H �A� �OH��POBAH��, KOTOP�M        * 
C     *               COOTBETCTB��T ��O��H�� �O�� BPEMEH, KM          * 
C     * MIX - REAL, �EBA� �PAH��A �A�AH�� �O�� BPEMEH �O OC� X, KM    * 
C     * MAX - REAL, �PABA� �PAH��A �A�AH�� �O�� BPEMEH �O OC� X, KM   * 
C     * HX1 - REAL, �A� �A�AH�� �O�� BPEMEH �O OC� X, KM              * 
C     * G - INTEGER, ��C�O ��O��H�� �O�� BPEMEH                       * 
C     * NN- INTEGER, ��C�O OTC�ETOB �O�� BPEMEH �O OC� X              * 
C     * VPMAX(G) - REAL, MACC�B MAKC�MA��HO �O��CT�M�X �HA�EH��       * 
C     *         KA����XC� CKOPOCTE� (��� COOTB. ��O��H�� �O�� BPEMEH) * 
C     * KOD - INTEGER, EC�� KOD=1,TO PA�PE�AETC� KOHTPO��HA� �E�AT�,  * 
C     *                EC�� KOD=1 - �E�AT� HET                        * 
C     * KOD1- INTEGER,(0/1), EC�� KOD1=1, TO BBO��TC� VPMAX(G),       * 
C     *                      EC�� KOD1=1, - BBO� MACC�BA ��HOP�P�ETC� * 
C     ***************************************************************** 
C                                                                       
C                     B�XO�H�E �APAMETP�                                
C     ***************************************************************** 
C     * WR(NR) - REAL, MACC�B KOOP��HAT TO�EK X,Z                     * 
C     *          � �HA�EH�� ��HK��� V B �AHH�X TO�KAX;                * 
C     * �HA�EH�� MACC�BA WR �A�A�TC� B C�E����EM �OP��KE X,Z,V;       * 
C     * HEO�PE�E�EHH�E Z � V �P�PABH�BA�TC� K H���                    * 
C     * NR - INTEGER, KO���ECTBO ��EMEHTOB MACC�BA WR                 * 
C     * G - INTEGER, ��C�O �POBHE� O�PE�E�EH�� ��CKPETHO�O CKOPOCT-   * 
C     *              HO�O PA�PE�A                                     * 
C     * NN- INTEGER, ��C�O KO�OHOK CKOPOCTHO�O PA�PE�A �O OC� X       * 
C     * ZMAX(NN)- REAL, MACC�B  MAKC�MA��H�X �����H O�PE�E�EH��       * 
C     *    PA�PE�A �OCPE�CTBOM INVERS (B�O�� OC� X, C �A�OM - HX1)    * 
C     * ZM0(NN) - REAL, ������ ������ ������ ������ �����             * 
C     ***************************************************************** 
      SUBROUTINE INVERS(T1,L0,MIX,MAX,HX1,G,NN,VPMAX,KOD,KOD1,WR,NR,    
     *                  ZM0,ZMAX)                                       
      INTEGER G,VOLN1,VOLN2,SVOLN,NR                                    
      REAL MU,MIX,MAX,L0                                                
      DIMENSION T1(33,150),Z(33,150),ZM0(33,150),V(33,150),L0(G),VPMAX( 
     *G),VP(33,150),V0P(33,150),BETA(33,150),V0(33,150),T0(33,150),     
     *Q(33,150),NH(33),NK(33),VR(150),ZR(150),VOLN1(3,150),VOLN2(3,150) 
     *,IND1(150),IND2(150),WR(10000),SVOLN(150),ZMAX(150),XHJ(33),      
     * XKJ(33)                                                          
      LOGICAL VD1,VD2                                                   
C                                                                       
      IF(T1(1,1).NE.0.0) XH=MIX                                         
      IF(T1(1,NN).NE.0.0) XK=MAX                                        
      NN1=NN-1                                                          
      DO 1 J=1,G                                                        
      IF(T1(J,1).NE.0.0) XHJ(J)=MIX-INT(L0(J)*0.5/HX1+1.E-4)*HX1        
      IF(T1(J,NN).NE.0.0) XKJ(J)=MAX+INT(L0(J)*0.5/HX1+1.E-4)*HX1       
      DO 1 I=2,NN1                                                      
      IF(T1(J,I).NE.0.0.AND.T1(J,I-1).EQ.0.) XHJ(J)=MIX+(I-1)*HX1-      
     * INT(L0(J)*0.5/HX1+1.E-4)*HX1                                     
      IF(T1(J,I).NE.0.0.AND.T1(J,I+1).EQ.0.) XKJ(J)=MIX+(I-1)*HX1+      
     * INT(L0(J)*0.5/HX1+1.E-4)*HX1                                     
      IF(T1(1,I).NE.0.0.AND.T1(1,I-1).EQ.0.) XH=MIX+(I-1)*HX1           
      IF(T1(1,I).NE.0.0.AND.T1(1,I+1).EQ.0.) XK=MIX+(I-1)*HX1           
    1 CONTINUE                                                          
      XHJ(1)=XH                                                         
      XKJ(1)=XK                                                         
      K=INT((XH-MIX)/HX1+1.E-4)+1                                       
      N=INT((XK-MIX)/HX1+1.E-4)+1                                       
      DO 2 J=1,G                                                        
      DO 2 I=K,N                                                        
    2 T1(J,I-K+1)=T1(J,I)                                               
      XHV=XH                                                            
      XKV=XK                                                            
      N=INT((XK-XH)/HX1+1.E-4)+1                                        
C                                                                       
      NH(1)=1                                                           
      NK(1)=N                                                           
      DO 3 J=2,G                                                        
      IF(XHJ(J).LT.XHJ(J-1)) XHJ(J)=XHJ(J-1)                            
      IF(XKJ(J).GT.XKJ(J-1)) XKJ(J)=XKJ(J-1)                            
      NH(J)=INT((XHJ(J)+L0(J)*0.5-XH+1.E-4)/HX1)+1                      
      NK(J)=INT((XKJ(J)-L0(J)*0.5-XH+1.E-4)/HX1)+1                      
    3 CONTINUE                                                          
C                                                                       
      JG=G                                                              
      DO 101 J=1,JG                                                     
      IF(J.GT.G) GOTO 101                                               
      IF(XHJ(J).GT.XKJ(J)) G=J-1                                        
  101 CONTINUE                                                          
C                                                                       
      WRITE(6,30)                                                       
   30 FORMAT(/2X,'*** �E�AT� XH,XK,NH(J),NK(J) ***'/)                   
      WRITE(6,70) XH,XK,(NH(J),NK(J),J=1,G)                             
   70 FORMAT(2X,'XH=',F6.1,2X,'XK=',F6.1/2(20I4))                        
      DO 4 I=1,NN                                                       
      VR(I)=0.0                                                         
      IND1(I)=0                                                         
      IND2(I)=0                                                         
    4 SVOLN(I)=0                                                        
      DO 5 J=1,G                                                        
      DO 5 I=1,NN                                                       
      VOLN1(J,I)=0                                                      
      VOLN2(J,I)=0                                                      
      Z(J,I)=1.E10                                                      
      ZM0(J,I)=0.0                                                      
    5 VP(J,I)=0.0                                                       
      NGG=G-1                                                           
      NK1=NK(1)                                                         
      NH1=NH(1)                                                         
      DO 15 I=NH1,NK1                                                   
      VP(1,I)=L0(1)/T1(1,I)
   15 IF(KOD1.EQ.1.AND.VPMAX(1).LT.VP(1,I)) VP(1,I)=VPMAX(1)            
C *** ��K� O�PE�E�EH�� VP(G-1,NN) ***                                   
      DO 200 J=2,NGG                                                    
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      DO 200 I=NHJ,NKJ                                                  
      IF(J.NE.2) GOTO44                                                 
      VR(I)=L0(2)/T1(2,I)                                               
      IF(VR(I).LT.VP(1,I))IND1(I)=1                                     
   44 IF(T1(J+1,I).NE.0.0)GOTO55                                        
      VP(J,I)=VP(J-1,I)                                                 
      GOTO56                                                            
   55 VP(J,I)=(L0(J+1)-L0(J-1))/(T1(J+1 ,I)-T1((J-1),I))                
      IF(KOD1.EQ.1.AND.VPMAX(J).LT.VP(J,I)) VP(J,I)=VPMAX(J)            
   56 IF(VP(J-1,I).GT.VP(J,I).AND.IND1(I).EQ.0) GOTO66                  
      GOTO67                                                            
   66 IND1(I)=J                                                         
      VP(J,I)=VP(J-1,I)                                                 
      GOTO67                                                            
   67 IF((IND1(I).NE.0.AND.IND2(I).EQ.0).AND.VP(J,I).NE.0.0) GOTO77     
      GOTO78                                                            
   77 IF(VP(J,I).GT.VP(J-1,I)) IND2(I)=J                                
   78 IF(IND1(I).NE.0.AND.IND2(I).NE.0) GOTO88                          
      GOTO91                                                            
   88 SVOLN(I)=SVOLN(I)+1                                               
      IF(SVOLN(I).LE.3) GOTO89                                          
      GOTO91                                                            
   89 NV=SVOLN(I)                                                       
      VOLN1(NV,I)=IND1(I)                                               
      VOLN2(NV,I)=IND2(I)                                               
      IND1(I)=0                                                         
      IND2(I)=0                                                         
   91 IF(VP(J,I).LT.VP(J-1,I))GOTO 92                                   
      GOTO200                                                           
   92 VP(J,I)=VP(J-1,I)                                                 
      IF(KOD1.EQ.1.AND.VPMAX(J).LT.VP(J,I)) GOTO93                      
      GOTO200                                                           
   93 VP(J,I)=VPMAX(J)                                                  
  200 CONTINUE                                                          
C *** KOHE� ��K�A O�PE�E�EH�� VP(G-1,NN) ***                            
      NHG=NH(G)                                                         
      NKG=NK(G)                                                         
      DO 6 I=NHG,NKG                                                    
      VP(G,I)=(L0(G)-L0(G-1))/(T1(G,I)-T1(G-1,I))                       
      IF(VP(G,I).LT.VP(G-1,I))VP(G,I)=VP(G-1,I)                         
      IF(KOD1.EQ.1.AND.VPMAX(G).LT.VP(G,I)) VP(G,I)=VPMAX(G)            
    6 CONTINUE                                                          
      DO 14 J=1,G                                                       
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      IF(VP(J,NHJ).EQ.0.)VP(J,NHJ)=VP(J,NHJ+1)                          
      IF(VP(J,NKJ).EQ.0.)VP(J,NKJ)=VP(J,NKJ-1)                          
   14 CONTINUE                                                          
C *** �E�AT� MACC�BA VP(G,NN) ***                                       
      WRITE(6,50)                                                       
   50 FORMAT(/2X,' * MACC�B KA����XC� CKOPOCTE� * VP(G,NN) *'/)         
      IF(KOD.EQ.0) goto 1007
      DO 1006 J=1,G
      WRITE(6,10) (VP(J,I),I=1,NN)                   
 1006 CONTINUE
   10 FORMAT(1X,10F6.2)                                                 
C                                                                       
C *** ��K� O�PE�E�EH�� Z(G,NN) ***                                      
 1007 DO 7 J=1,G                                                        
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      DO 7 I=NHJ,NKJ                                                    
      MU=VP(J,I)*T1(J,I)/L0(J)                                          
      IF(MU.LT.1.001 ) MU=1.001                                         
   99 Z(J,I)=L0(J)/(28.274*(MU-1)**2)*(((3*MU-2)**2+0.5)*               
     *ALOG(3*MU-2+SQRT((3*MU-2)**2-1))-1.5*(3*MU-2)*SQRT((3*MU-2)**2-1))
      IF(J.EQ.1)GOTO111                                                 
      IF(Z(J,I).GE.Z(J-1,I))GOTO111                                     
      MU=MU+0.001                                                       
      VP(J,I)=MU/T1(J,I)*L0(J)                                          
      GOTO99                                                            
  111 V0P(J,I)=VP(J,I)*(L0(J)**2-4*Z(J,I)**2)/(4*Z(J,I)**2+L0(J)**2)    
      BETA(J,I)=(VP(J,I)/V0P(J,I)-1.)/Z(J,I)                            
      ZM0(J,I)=Z(J,I)                                                   
      IF(J.EQ.1)GOTO112                                                 
      IF(J.NE.1)V0(J,I)=V0P(J,I)*(1+BETA(J,I)*(Z(J-1,I)+Z(J,I))*0.5)    
      GOTO 113                                                          
  112 V0(J,I)=V0P(J,I)*(BETA(J,I)*Z(1,I)*0.5+1)                         
  113 T0(J,I)=2/V0P(J,I)/BETA(J,I)*ALOG(0.5*L0(J)*BETA(J,I)             
     *+SQRT((L0(J)*BETA(J,I)*0.5)**2+1))                                
    7 CONTINUE                                                          
C *** KOHE� ��K�A O�PE�E�EH�� Z(G,NN) ***                               
C *** �E�AT� MACC�BA Z(G,NN) ***                                        
      WRITE(6,60)                                                       
   60 FORMAT(/2X,' * MACC�B �����H MAKC�M-�O �POH�KAH�� * Z(G,NN) *'/)  
      IF(KOD.EQ.0) GOTO 1008
      DO 1017 J=1,G
      WRITE(6,10) (Z(J,I),I=1,NN)                 
 1017 CONTINUE
C                                                                       
C *** B���C�EH�E AHOMA��� BPEMEH� ***                                   
 1008 DO 8 J=1,G                                                        
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      DO 8 I=NHJ,NKJ                                                    
    8 Q(J,I)=T1(J,I)-T0(J,I)                                            
C                                                                       
C *** B���C�EH�E KC�-AHOMA��� � CKOPOCT� ��� 1-�O �P-H� �����H ***      
      NH1=NH(1)                                                         
      NK1=NK(1)                                                         
      DO 9 I=NH1,NK1                                                    
      Q(1,I)=Q(1,I)/SQRT(L0(1)**2+5.333*Z(1,I)**2)                      
    9 V(1,I)=V0(1,I)-V0(1,I)**2*Q(1,I)/(1+V0(1,I)*Q(1,I))               
C                                                                       
C *** ��K� O�PE�E�EH�� KC�-AHOMA��� � CKOPOCTE� ��� 2-G �POBHE� ***     
      DO 300 J=2,G                                                      
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      DO 300 I=NHJ,NKJ                                                  
      A0=0.0                                                            
      DL1=0.0                                                           
      DL2=0.0                                                           
      DL01=0.0                                                          
      DL02=0.0                                                          
      I01=0                                                             
      I02=0                                                             
      J0=1                                                              
      SH1=SQRT(L0(J)**2+5.333*Z(J,I)**2)                                
      SB1=SH1                                                           
      X=XH +(I-1)*HX1                                                   
      XI=X-L0(J)*0.5                                                    
      XP=X+L0(J)*0.5                                                    
      AL0=ASIN(V0P(J,I)/VP(J,I))                                        
      JJ=J-1                                                            
C *** BH�TPEHH�� J1-��K� ***                                            
      DO12 J1=1,JJ                                                      
      VD1=.FALSE.                                                       
      VD2=.FALSE.                                                       
      X1=XI+DL1                                                         
      I1=INT((X1-XH)/HX1+1.E-4)+1                                       
      IF(X1.GE.(XH+(I1+0.5-1)*HX1-1.E-4)) I1=I1+1                       
      X2=XP-DL2                                                         
      I2=INT((X2-XH)/HX1+1.E-4)+1                                       
      IF(X2.GT.(XH+(I2+0.5-1)*HX1)) I2=I2+1                             
      IF(Z(J1,I1).GT.1.E04)I1=NH(J1)                                    
      IF(Z(J1,I2).GT.1.E04) I2=NK(J1)                                   
      Z1=Z(J1,I1)                                                       
      Z2=Z(J1,I2)                                                       
      IF(J1.EQ.1) GOTO 444                                              
      IF(Z1.LT.Z01.OR.(Z1.GT.Z(J,I).AND.(I-I1).EQ.1))Z1=Z01             
      IF(Z1.LT.Z01.OR.(Z1.GT.Z(J,I).AND.(I-I1).EQ.1)) VD1=.TRUE.        
      IF(Z1.GT.Z(J,I).AND.(I-I1).GT.1) GOTO222                          
      GOTO223                                                           
  222 X1=XH+(I1+1-1)*HX1                                                
      Z1=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2/SIN(AL0)**2-(X1-XI-1/         
     *BETA(J,I)/TAN(AL0))**2)                                           
  223 IF(Z2.LT.Z02.OR.(Z2.GT.Z(J,I).AND.(I2-I).EQ.1))Z2=Z02             
      IF(Z2.LT.Z02.OR.(Z2.GT.Z(J,I).AND.(I2-I).EQ.1))VD2=.TRUE.         
      IF(Z2.GT.Z(J,I).AND.(I2-I).GT.1)GOTO333                           
      GOTO444                                                           
  333 X2=XH+(I2-1+1)*HX1                                                
      Z2=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2 /SIN(AL0)**2-(XP-X2-1/BETA(   
     *J,I)/TAN(AL0))**2)                                                
  444 CONTINUE                                                          
      IF(J1.NE.1) GOTO 555                                              
      IF(Z1.LE.Z(J,I)) GOTO 666                                         
      X1=XH+( I1-1)*HX1+L0(J1)*0.5                                      
      Z1=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2/ SIN(AL0)**2-(X1-XI-1/        
     *BETA(J,I)/TAN(AL0))**2)                                           
  666 IF(Z2.LE.Z(J,I)) GOTO555                                          
      X2=XH+(I2-1)*HX1-L0(J1)*0.5                                       
      Z2=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2/SIN(AL0)**2-(XP-X2-1/         
     *BETA(J,I)/TAN(AL0))**2)                                           
  555 DL1=DL01                                                          
      IF(.NOT.VD1)DL1=1/(BETA(J,I)*TAN(AL0))-1/(BETA(J,I)*              
     *SIN(AL0))*SQRT(1-SIN(AL0)**2*(1+BETA(J,I)*Z1)**2)                 
      DL2=DL02                                                          
      IF(.NOT.VD2) DL2=1/(BETA(J,I)*TAN(AL0))-1/(BETA(J,I)              
     **SIN(AL0))*SQRT(1-SIN(AL0)**2*(1+BETA(J,I)*Z2)**2)                
      IF(J1.EQ.1) GOTO888                                               
      IF(XI+DL1.LE.(XH+(I1-1)*HX1+L0(J1)*0.5)) GOTO777                  
      X1=XH+(I1-1)*HX1+L0(J1)*0.5                                       
      Z1=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2/SIN(AL0)**2-(X1-XI-1/BETA(    
     *J,I)/TAN(AL0))**2)                                                
      DL1=X1-XI                                                         
  777 IF(XP-DL2.GE.(XH+(I2-1)*HX1-L0(J1)*0.5))GOTO888                   
      X2=XH+(I2-1)*HX1-L0(J1)*0.5                                       
      Z2=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2/SIN(AL0)**2-(XP-X2-1/BETA(    
     *J,I)/TAN(AL0))**2)                                                
      DL2=XP-X2                                                         
  888 SH2=SH1                                                           
      SB2=SB1                                                           
      SH1=SQRT((L0(J)-2*DL1)**2+5.333*(Z(J,I)-Z1)**2)                   
      SB1=SQRT((L0(J)-2*DL2)**2+5.333*(Z(J,I)-Z2)**2)                   
      DSH=0.5*(SH2-SH1)                                                 
      DSB=0.5*(SB2-SB1)                                                 
      IF(J1-1) 999,999,991                                              
  999 IF(I1.LT.NH(1))I1=NH(1)                                           
      IF(I2.GT.NK(1))I2=NK(1)                                           
      Q1=V(1,I1)-V0P(J,I)*(1+BETA(J,I)*Z(1,I1)*0.5)                     
      Q1=-Q1/(V0P(J,I)*(1+BETA(J,I)*Z(1,I1)*0.5))/V(1,I1)               
      Q2=V (1,I2)-V0P(J,I)*(1+BETA(J,I)*Z(1,I2)*0.5)                    
      Q2=-Q2/(V0P(J,I)*(1+BETA(J,I)*Z(1,I2)*0.5))/V(1,I2)               
      GOTO 992                                                          
  991 IF(I1.LT.NH(J1))I1=NH(J1)                                         
      IF(I2.GT.NK(J1))I2=NK(J1)                                         
      Q1=V(J1,I1)-V0P(J,I)*(1+BETA(J,I)*(Z(J1-1,I1)+Z(J1,I1))*0.5)      
      Q1=-Q1/(V0P(J,I)*(1+BETA(J,I)*(Z(J1-1,I1)+Z(J1,I1))*0.5))/        
     *V(J1,I1)                                                          
      Q2=V(J1,I2)-V0P(J,I)*(1+BETA(J,I)*(Z(J1-1,I2)+Z(J1,I2))*0.5)      
      Q2=-Q2/(V0P(J,I)*(1+BETA(J,I)*(Z(J1-1,I2)+Z(J1,I2))*0.5))/        
     *V(J1,I2)                                                          
  992 A0=A0+Q1*DSH+Q2*DSB                                               
      DL01=DL1                                                          
      DL02=DL2                                                          
      IF(VD1)  I1=I01                                                   
      IF(VD2)  I2=I02                                                   
      Z01=Z1                                                            
      Z02=Z2                                                            
      J0=J1                                                             
      I01=I1                                                            
   12 I02=I2                                                            
C *** KOHE� J1-��K�A ***                                                
      DS=(SH1+SB1)*0.5                                                  
      Q(J,I)=(Q(J,I)-A0)/DS                                             
  300 V(J,I)=V0(J,I)-V0(J,I)**2*Q(J,I)/(1+V0(J,I)*Q(J,I))               
C *** KOHE� ��K�A O�PE�E�EH�� KC�-AHOMA��� � CKOPOCTE� ***              
C                                                                       
C *** ��K� B���C�EH�� �����H �EHTPOB ��EEK ***                          
      DO 400 J=1,G                                                      
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      DO 400 I=NHJ,NKJ                                                  
      A0=Z(J,I)                                                         
      IF(J.NE.1)Z(J,I)=(ZR(I)+Z(J,I))*0.5                               
      IF(J.EQ.1)Z(J,I)=0.5*Z(J,I)                                       
      ZR(I)=A0                                                          
  400 CONTINUE                                                          
C                                                                       
C *** ��K� �E�AT� X,Z,V  �  PAC�ETA MACC�BA WR(NR) ***                  
      WRITE(6,80)                                                       
   80 FORMAT(/2X,'��CKPETH�� CKOPOCTHO� PA�PE� X,Z,V(X,Z)'/)            
      MIX=XH                                                            
      NN=NK(1)                                                          
      IC=1                                                              
      DO 13 J=1,G                                                       
      DO 18 I=1,NN                                                      
      WR(IC)=HX1*(I-1)+XH                                               
      WR(IC+1)=Z(J,I)                                                   
      WR(IC+2)=V(J,I)                                                   
      IF(Z(J,I).LT.1.E4)GOTO18                                          
      WR(IC+1)=0.0                                                      
      WR(IC+2)=0.0                                                      
   18 IC=IC+3                                                           
   13 CONTINUE                                                          
      DO 20 J=1,G                                                       
      IR1=(J-1)*NN*3+1                                                  
      IR2=J*NN*3                                                        
      WRITE(6,21) (WR(I),I=IR1,IR2)                                     
   20 CONTINUE                                                          
   21 FORMAT(15F8.2)                                                    
      NR=G*NN*3                                                         
C                                                                       
C     *** O�PE�E�EH�E �PAH��� O��ACT� V(X,Z) �O �����HE ***             
      DO 40 I=1,NN                                                      
      ZMAX(I)=0.0                                                       
      DO 40 J=2,G                                                       
      IF(Z(J,I).LT.1.E+4) ZMAX(I)=Z(J,I)                                
      IF(Z(J,I).GE.1.E+4.AND.Z(J-1,I).LT.1.E+4) ZMAX(I)=Z(J-1,I)        
   40 CONTINUE                                                          
C                                                                       
C                                                                       
      RETURN                                                            
      END                                                               
C --------------------------------------------------------------------- 
C                       SYMPLEX                                         
C     ==========================================================        
C     =     �O��PO�PAMMA 2-X-MEPHO� ��HE�HO� �HTEP�O�����      =        
C     =            C�M��EKC METO�OM                            =        
C     ==========================================================        
C                                                                       
C                     BXO�H�E �AHH�E                                    
C     *************************************************************     
C     * XH - REAL, �EBA� X-�PAH��A O��ACT�                        *     
C     * ZH - REAL, BEPXH�� Z-�PAH��A O��ACT�                      *     
C     * HX1 -REAL, �A� �A�AH�� �CXO�HO�O PA�PE�A �O OC� X         *     
C     * NN - INTEGER, KO���ECTBO TO�EK �O OC� X                   *     
C     * G - INTEGER, KO���ECTBO OTC�ETOB Z � V �O BEPT�KA��       *     
C     * Z(G,NN) - REAL, MACC�B �����H �CXO�HO�O PA�PE�A V(X,Z)    *     
C     * V(G,NN) - REAL, MACC�B CKOPOCTE� �CXO�HO�O PA�PE�A        *     
C     * ZMAX(NN) - REAL, MACC�B �����H H��HE� �PAH��� O��ACT�     *     
C     * IZMAX(NN)- INTEGER, MACC�B �H�EKCOB �����H H��HE� �PAH��� *     
C     * XS,ZS - REAL, X,Z-KOOP��HAT� TEK��E� TO�K� �HTEP�O�����   *     
C     *************************************************************     
C                                                                       
C                B�XO�H�E �AHH�E                                        
C     *************************************************************     
C     * XS,ZS - REAL, X-Z KOP��HAT� TEK��E� TO�K� �HTEP�O�����    *     
C     * VS -REAL, �HTEP�O����OHHOE �HA�EH�E CKOPOCT� B �TO� TO�KE *     
C     *************************************************************     
C                                                                       
      SUBROUTINE SYMPL(NN,G,V,Z,XH,ZH,HX1,ZMAX,IZMAX,XS,ZS,VS)          
      DIMENSION V(100,200),Z(100,200),ZMAX(150),IZMAX(150)              
      REAL XH,ZH,HX1,XS,ZS,VS                                           
      INTEGER G,NN,IZMAX                                                
C                                                                       
C     *** O�PE�E�EH�E X-�O�O�EH�E TO�K� �HTEP�O����� ***                
      NN1=NN-1                                                          
      DO 1 I=1,NN1                                                      
      IF(XS.GE.XH+HX1*(I-1).AND.XS.LT.XH+HX1*I) IX=I                    
    1 CONTINUE                                                          
      I=IX                                                              
C     *** O�PE�E�EH�E Z-�O�O�EH�� TO�K� �HTEP�O����� ***                
C     *** OTHOC�TE��HO H��HE� �PAH��� O��ACT� ZMAX   ***                
      ZMD=ZMAX(I)+(ZMAX(I+1)-ZMAX(I))/HX1*(XS-XH-(I-1)*HX1)             
C     *** EC�� TO�KA HAXO��TC� BHE O��ACT�,          ***                
C     *** �HTEP�O����� HE �PO��BO��TC�               ***                
      IF(ZS.GE.ZMD) GOTO 9                                              
C     *** ��ET BO�MO�HOCT� HAXO��EH�� TO�K� B��E BEPXHE�  ***           
C     *** �PAH��� O��ACT� �A�AH�� CKOPOCT�                ***           
      ZXB=Z(1,I)+(Z(1,I+1)-Z(1,I))/HX1*(XS-XH-(I-1)*HX1)                
      IF(ZS.LT.ZXB) VS=V(1,I)+(V(1,I+1)-V(1,I))/HX1*                    
     *                      (XS-XH-(I-1)*HX1)                           
C     *** �OCKO��K� CKOPOCT� O�PE�E�EHA �XO��M HA KOHE�  ***            
      IF(ZS.LT.ZXB) GOTO 9                                              
C                                                                       
      JLU=1                                                             
      JRU=1                                                             
      IL=0                                                              
      IR=0                                                              
C     ***  �H�EKC� ��� PA�OT� HA �OKOB�X �PAH��AX O��ACT� ***           
      KLU=IZMAX(I)                                                      
      KRD=IZMAX(I+1)                                                    
C                                                                       
C     *** O�PE�E�EH�E Z-�O�O�EH�� TO�K� BH�TP� O��ACT� ***              
C     ### OKPECTHOCT� BEPXHE� �PAH��� ###                               
      IF(ZS.LT.Z(1,I).AND.ZS.GE.Z(1,I+1)) GOTO 11                       
      GOTO 12                                                           
   11 JLU=1                                                             
      IL=1                                                              
   12 IF(ZS.GE.Z(1,I).AND.ZS.LT.Z(1,I+1)) GOTO 13                       
      GOTO 14                                                           
   13 JRD=1                                                             
      IR=1                                                              
   14 DO 5 J=2,G                                                        
C                                                                       
      IF(ZS.GE.Z(J-1,I).AND.ZS.LT.Z(J,I)) GOTO 2                        
      GOTO 3                                                            
C                                                                       
    2 JLU=J-1                                                           
      JLD=J                                                             
      IL=1                                                              
C                                                                       
    3 IF(ZS.GE.Z(J-1,I+1).AND.ZS.LT.Z(J,I+1)) GOTO 4                    
      GOTO 5                                                            
C                                                                       
    4 JRU=J-1                                                           
      JRD=J                                                             
      IR=1                                                              
C                                                                       
    5 CONTINUE                                                          
C                                                                       
      IF(IL.EQ.0) JLU=KLU                                               
      IF(IR.EQ.0) JRD=KRD                                               
C                                                                       
C     *** �A�AH�E KOOP��HAT TO�EK C�M��EKCA ***                         
C                                                                       
      X1=XH+HX1*(I-1)                                                   
      Z1=Z(JLU,I)                                                       
      V1=V(JLU,I)                                                       
C     X2,Z2,V2 - O�PE�E���TC� B �AB�C�MOCT� OT �O�O�EH�� TO�K�          
      X3=XH+HX1*I                                                       
      Z3=Z(JRD,I+1)                                                     
      V3=V(JRD,I+1)                                                     
C                                                                       
C     VS= H��HO O�PE�E��T�                                              
C                                                                       
C     *** O�PE�E�EH�E �O�O�EH�� TO�K� OTHOC�TE��HO ��A�OHA�� 4-��-KA ** 
      ZX=Z(JLU,I)+(Z(JRD,I+1)-Z(JLU,I))*(XS-XH-(I-1)*HX1)/HX1           
C                                                                       
      IF(ZS.LT.ZX) GOTO 6                                               
      GOTO 7                                                            
C                                                                       
    6 X2=XH+HX1*I                                                       
      Z2=Z(JRU,I+1)                                                     
      V2=V(JRU,I+1)                                                     
      GOTO 8                                                            
C                                                                       
C     *** �HA�E T.E. �P� ZS.GE.ZX                                       
    7 IF(ZS.LT.ZMD.AND.IL.EQ.0) GOTO 6                                  
C     (*** �KCTPA�O����� H��E TPE��O��H�KA ***)                         
C                                                                       
      X2=XH+HX1*(I-1)                                                   
      Z2=Z(JLD,I)                                                       
      V2=V(JLD,I)                                                       
C                                                                       
    8 VS=                                                               
     *(-(V1*(Z2-Z3)-Z1*(V2-V3)+(V2*Z3-V3*Z2))*XS                        
     * -(X1*(V2-V3)-V1*(X2-X3)+(X2*V3-X3*V2))*ZS                        
     * +(X1*(V2*Z3-V3*Z2)-V1*(X2*Z3-X3*Z2)+Z1*(X2*V3-X3*V2)))/          
     *  (Z1*(X2-X3)-X1*(Z2-Z3)+(Z2*X3-Z3*X2))                           
C                                                                       
    9 CONTINUE                                                          
C                                                                       
      RETURN                                                            
      END                                                               
C     ----------------------------------------------------------------- 
      SUBROUTINE FILTR(M,N,MN,VS,HX,HZ,IZMAX,DX1,DZ1,DX2,DZ2,ZF2,VF)    
C     ================================================================  
C     =  �O��PO�PAMMA ����TPA��� �OCPE�CTBOM OCPE�HEH�� B OKHE DX,DZ =  
C     =                      15.03.88.                               =  
C     ================================================================  
C                     �APAMETP� BXO�A                                   
C     ****************************************************************  
C     * VS(MN) - REAL, MACC�B �CXO�H�X �HA�EH�� CKOPOCT�, C�OPM�PO-  *  
C     *          BAHH�� B O�HOMEPHOM B��E (�OCTPO�HO)                *  
C     * N - INTEGER, ���HA CTPOK� COOTBETCTB���E�O 2-MEPHO�O MACC�BA *  
C     * M - INTEGER, KO���ECTBO CTPOK  -/-/-/-/-       VS(M,N)       *  
C     * MN = M*N, INTEGER                                            *  
C     * HX - REAL, �A� ��CKPET��A��� �HA�EH�� CKOPOCT� �O OC� X      *  
C     * HZ - REAL, �A� ��CKPET��A��� �HA�EH�� CKOPOCT� �O OC� Z      *  
C     * IZMAX(N) - INTEGER, MACC�B �H�EKCOB �����H H��HE� �PAH���    *  
C     * DX1 \                      O��ACT� �A�AH�� CKOPOCT�          *  
C     * DZ1 /                                                        *  
C     * DX2 \      PA�MEP� OKOH OCPE�H���E�O C��A��BAH��             *  
C     * DZ2 /                                                        *  
C     * ZF2 - �POBEH�, PA��E������ O��ACT� PA����HO�O C��A��BAH��    *  
C     ****************************************************************  
C                     �APAMETP� B�XO�A                                  
C     ****************************************************************  
C     * VF(MN) - REAL, MACC�B C��A�EHH�X �HA�EH�� CKOPOCT�,          *  
C     *          C�OPM�POBAHH�� B O�HOMEPHOM B��E (�OCTPO�HO)        *  
C     * MN -                                                         *  
C     * M  -                                                         *  
C     * N  -          CM. B��E                                       *  
C     * HX,HZ -                                                      *  
C     * IZMAX(N) -                                                   *  
C     ****************************************************************  
C                                                                       
C                                                                       
      DIMENSION VS(MN),VF(MN),IZMAX(N)                                  
      REAL HX,HZ,DX1,DZ1,DX2,DZ2,ZF2                                    
C     INTEGER M,N                                                       
C                                                                       
C     *** ��CTKA MACC�BA VF(MN) ***                                     
      DO 1 I=1,MN                                                       
      VF(I)=0.0                                                         
    1 CONTINUE                                                          
C                                                                       
C     *** �A�AH�E OKHA ����TPA��� ***                                   
      NF1=INT(DX1/HX+1.E-5)+1                                           
      MF1=INT(DZ1/HZ+1.E-5)+1                                           
      NF2=INT(DX2/HX+1.E-5)+1                                           
      MF2=INT(DZ2/HZ+1.E-5)+1                                           
      JF2=INT(ZF2/HZ+1.E-5)+1                                           
      NF=NF1                                                            
      MF=MF1                                                            
C                                                                       
C     *** O�PE�E�EH�E HA�A��H�X IH,JH � KOHE�H�X IK,JK �H�EKCOB ***     
C     *** O��ACT� PE����TATA ����TPA��� (�OKA HE �C�O�����TC�)  ***     
C     IH=NF/2                                                           
C     IK=N-NF/2                                                         
C     JH=MF/2                                                           
C     JK=M-MF/2                                                         
C                                                                       
C     *** �X �AMEHA ***                                                 
      IH=1                                                              
      IK=N                                                              
      JH=1                                                              
      JK=M                                                              
C                                                                       
C     *** �EHTPA��H�� ��OK ����TPA ***                                  
C                                                                       
      DO 4 J=JH,JK                                                      
      DO 4 I=IH,IK                                                      
C                                                                       
C     *** ���E�AEM ����TPA��� �A �PAH��E� O��ACT� ***                   
      IF(J.GT.IZMAX(I)) GOTO 4                                          
C                                                                       
C     *** �BE���EH�E OKHA OCPE�HEH�� �P� J > JF2  ***                   
      IF(J.LE.JF2) GOTO 2                                               
      NF=NF2                                                            
      MF=MF2                                                            
C                                                                       
C     *** C�MM�POBAH�E B OKHE ***                                       
    2 S=0.0                                                             
      ISCH=0                                                            
      DO 3 JJ=1,MF                                                      
      DO 3 II=1,NF                                                      
C                                                                       
C     *** �EPEC�ET �H�EKCOB OKHA B �H�EKC� O��ACT� ***                  
      J1=J-MF/2+JJ-1                                                    
      I1=I-NF/2+II-1                                                    
C                                                                       
      JI=(J1-1)*N+I1                                                    
C                                                                       
C     *** KOHTPO�� B�XO�A �A �PAH��� O��ACT� ***                        
      IF(J1.LT.1.OR.J1.GT.M) GOTO 3                                     
      IF(I1.LT.1.OR.I1.GT.N) GOTO 3                                     
C                                                                       
C     *** H��EB�E ��EMEHT� OKHA HE ���T�BA�TC� ***                      
      IF(VS(JI).EQ.0.0) GOTO 3                                          
      S=S+VS(JI)                                                        
      ISCH=ISCH+1                                                       
    3 CONTINUE                                                          
C                                                                       
      IF(ISCH.NE.0) VF((J-1)*N+I)=S/ISCH                                
C                                                                       
    4 CONTINUE                                                          
C                                                                       
      RETURN                                                            
      END                                                               
C     --------------------------------------------------------------    
C              PERICL(M,N,XH,ZH,HX,HZ,MINV,MAXV,
C                           G,NN,V,Z,HX1,ZMAX,IZMAX)
C                                                                       
C     ==============================================================    
C     =  �O��PO�PAMMA �OPM�POBAH�� ������������ MACC�BA            =    
C     =  �������� �O 640 �������� (1280 �A�T)                      =    
C     =  � ������ ����� ������� HA ���� � ��������� ����           =    
C     ==============================================================    
C                                                                       
C     �/� O�PA�AETC� K SYMPL(NN,G,V,Z,MIX,MIZ,HX1,ZMAX,IZMAX,XS,ZS,VS)  
C     �PE��O�A�AETC�, �TO �APAMETP� SYMPL O��CAH� � O�PE�E�EH� B        
C     �O�OBHO� �PO�PAMME                                                
C                                                                       
C              BXO�H�E �APAMETP�                                        
C     ***************************************************************** 
C     * M - INTEGER, ��C�O CTPOK MACC�BA                                
C     * N - INTEGER, ��C�O O�PE�E�EHH�X CTO���OB MACC�BA                
C     * XH - REAL, HA�A�O OTC�ETOB �O OC� X                             
C     * ZH - REAL, HA�A�O OTC�ETOB �O OC� Z                             
C     * HX - REAL, �A� ��CKPET��A��� �O OC� X                           
C     * HZ - REAL, �A� ��CKPET��A��� �O OC� Z                           
C     ***************************************************************** 
C                                                                       
C                 B�XO�H�E �APAMETP�                                    
C     ***************************************************************** 
C     * IVGRAF(640) - INTEGER*2  ��POK� MACC�BA, ���������������
C     *              �A��C�BAEM�E HA ����, ������� �� 1280 ����         
C     * �INV                                                            
C     * MAXV                                                            
C     ***************************************************************** 
C                                                                       
      SUBROUTINE PERICL (M,N,XH,ZH,HX,HZ,MINV,MAXV,            
     *                   G,NN,V,Z,HX1,ZMAX,IZMAX)                       
C                                                                       
      DIMENSION V(100,200),Z(100,200),ZMAX(NN),IZMAX(NN)                
      INTEGER M,N,G,IZMAX                                               
      REAL XH,ZH,HX,HZ                                                  
      INTEGER*2 IVGRAF(640)                                              
      INTEGER MINV,MAXV                                                 
C
      NN2=NN/2                                                          
      MINV=INT(V(1,NN2)*100+1.E-5)                                                     
      MAXV=MINV                                                         
C                                                                       
C     *** OCHOBHO� J-��K� �EPE�OPA CTPOK CETK� ***                      
C                                                                       
      DO 30 J=1,M                                                       
      DO 10 I=1,640                                                     
   10 IVGRAF(I)=0                                                      
C                                                                       
      ZS=ZH+(J-1)*HZ                                                    
C     *** I-��K� PAC�ETA �HA�EH�� B CTPOKE ***                          
      DO 20 I=1,N                                                       
      XS=XH+(I-1)*HX                                                    
      VS=0.0                                                            
C                                                                       
      CALL SYMPL(NN,G,V,Z,XH,ZH,HX1,ZMAX,IZMAX,XS,ZS,VS)                
C     *** �OPM�POBAH�E ������� ����� MACC�BA  ***                       
C                                                                       
      IVGRAF(I)=INT(VS*100+1.E-5)                                                       
C                                                                       
C     *** O�PE�E�EH�E MIN-�O � MAX-�O �HA�EH�� MACC�BA  ***             
      IF(IVGRAF(I).NE.0.AND.IVGRAF(I).LT.MINV) MINV=IVGRAF(I)            
      IF(IVGRAF(I).GT.MAXV) MAXV=IVGRAF(I)                                
C                                                                       
   20 CONTINUE                                                          
C     *** KOHE� I-��K�A �OPM�POBAH�� CTPOK� ***                         
C
C     ================================================================  
C       
C     *** �OCTPO�HA�  �A��C� HA �EHT� MACC�BA VGRAF(640) - M CTPOK ***  
C
      WRITE(10) (IVGRAF(I),I=1,N)
C
C     ================================================================= 
   30 CONTINUE                                                          
C
C++   WRITE(6,31) MINV,MAXV                                             
C++31 FORMAT(/2X,'MINV = ',I5,5X,'MAXV = ',I5)                      
C                                                                       
      RETURN                                                            
      END                                                               
