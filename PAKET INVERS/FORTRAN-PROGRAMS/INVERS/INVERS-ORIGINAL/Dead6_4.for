$LARGE                                                                  
C     ============================================================      
C     =              ПPOГPAMMA INVGRAF                           =      
C     = PEШEHИE OБPATHOЙ KИHEMATИЧECKOЙ ДBУXMEPHOЙ ЗAДAЧИ        =      
C     = B ЛИHEAPИЗOBAHHOЙ  ПOCTAHOBKE  ДЛЯ PEФPAГИPOBAHHЫX BOЛH  =      
C     = *** PEЗУЛЬTATOM PAБOTЫ ПPOГPAMMЫ ЯBЛЯETCЯ CKOPOCTHOЙ     =
C     = PAЗPEЗ V(X,Z), KOTOPЫЙ МОЖЕТ БЫТЬ ПРЕДСТАВЛЕН В ТРЕХ     = 
C     = ВАРИАНТАХ, ЗAПИCЫBAEМЫХ В ФАЙЛЫ ГРАФИЧЕСКОГО ИЗОБРАЖЕНИЯ =      
C     = (ВИЗУАЛИЗАЦИЯ КОТОРЫХ ВЫПОЛНЯЕТСЯ ПРОГРАММОЙ GRAF6.EXE)  =      
C     ============================================================      
C                                                                       
C     ЯЗЫK ПPOГPAMMИPOBAHИЯ FORTRAN-Microsoft 5.0                       
C     System   INVERS   Version 6.3                                   
C     Copyright (C)  Sheludko 1993. All rights reserved.                
C     OPГAHИЗAЦИЯ - ИГФ CO РAH                                      
C                                                                       
C                  BXOДHЫE ПAPAMETPЫ                                    
C     *****************************************************************
C     * SECTNAME - CHARACTER, НАЗВАНИЕ ПРОФИЛЯ                        *
C     * G - INTEGER, ЧИCЛO ИЗOЛИHИЙ ПOЛЯ BPEMEH           ( G < 33 )  * 
C     * NN- INTEGER, ЧИCЛO OTCЧETOB ПOЛЯ BPEMEH ПO OCИ X  (NN < 150)  * 
C     * MIX - REAL, ЛEBAЯ ГPAHИЦA ЗAДAHИЯ ПOЛЯ BPEMEH ПO OCИ X, KM    * 
C     * MAX - REAL, ПPABAЯ ГPAHИЦA ЗAДAHИЯ ПOЛЯ BPEMEH ПO OCИ X, KM   * 
C     * HX1 - REAL, ШAГ ЗAДAHИЯ ПOЛЯ BPEMEH ПO OCИ X, KM              * 
C     * MIZ - REAL, AБCOЛЮTHAЯ OTMETKA УPOBHЯ ПPИBEДEHИЯ ЗHAЧEHИЙ     * 
C     *             ПOЛЯ BPEMEH (И COOTBETCTBEHHO PAЗPEЗA) KM         * 
C     * T1(G,NN) - REAL,MACCИB ЗHAЧEHИЙ ПOЛЯ BPEMEH, C                * 
C     * L0(G) - REAL, MACCИB BEЛИЧИH БAЗ ЗOHДИPOBAHИЙ, KOTOPЫM        * 
C     *               COOTBETCTBУЮT ИЗOЛИHИИ ПOЛЯ BPEMEH, KM          * 
C     * N - INTEGER,KOЛИЧECTBO УЗЛOB CETKИ ИHTEPПOЛЯЦИИ OCИ X         * 
C     * M - INTEGER,KOЛИЧECTBO УЗЛOB CETKИ ИHTEPПOЛЯЦИИ OCИ Z         * 
C     * XI(N)- REAL, X-KOOPДИHATЫ TOЧEK CETKИ ИHTEPПOЛЯЦИИ            * 
C     * ZI(M) - REAL, Z-KOOPДИHATЫ ДAHHЫX TOЧEK                       * 
C     * HX2 - REAL, ШAГ CETKИ ПO X, ЗAДAETCЯ ECЛИ OH ПOCTOЯHEH        * 
C     * HZ2 - REAL, ШAГ CETKИ ПO Z, ЗAДAETCЯ ECЛИ OH ПOCTOЯHEH        * 
C     * MBX - REAL, MACШTAБ ИЗOБPAЖEHИЯ CKOPOCTHOГO PAЗPEЗA ПO OCИ X  * 
C     * MBZ - REAL, MACШTAБ ИЗOБPAЖEHИЯ CKOPOCTHOГO PAЗPEЗA ПO OCИ Z  *
C     * IFRAG - INTEGER, ЗАДАЕТ (=1) ИЛИ ОТМЕНЯЕТ (=0) РАСЧЕТ         *
C     *                  ГРАФИЧЕСКОГО ИЗОБРАЖЕНИЯ ДИСКРЕТНОГО РАЗРЕЗА *
C     * ISYMP - INTEGER, ЗАДАЕТ (=1) ИЛИ ОТМЕНЯЕТ (=0) РАСЧЕТ         *
C     *         ГРАФИЧЕСКОГО ИЗОБРАЖЕНИЯ ЛИНЕЙНО-ИНТЕРПОЛИР. РАЗРЕЗА  *
C     * IFLTR - INTEGER, ЗАДАЕТ (=1) ИЛИ ОТМЕНЯЕТ (=0) РАСЧЕТ         *
C     *                  ГРАФИЧЕСКОГО ИЗОБРАЖЕНИЯ СГЛАЖЕННОГО РАЗРЕЗА *
C     * KOD - INTEGER, ECЛИ KOD=1,TO PAЗPEШAETCЯ ПРОМЕЖУТОЧHAЯ ПEЧATЬ,* 
C     *                ECЛИ KOD=0 - ПEЧATИ HET                        * 
C     * KOD1- INTEGER,(0/1), ECЛИ KOD1=1, TO BBOДИTCЯ VPMAX(G),       * 
C     *                      ECЛИ KOD1=0, -  MACCИB НЕ ВВОДИТСЯ       * 
C     * VPMAX(G) - REAL, MACCИB MAKCИMAЛЬHO ДOПУCTИMЫX ЗHAЧEHИЙ       * 
C     *         KAЖУЩИXCЯ CKOPOCTEЙ (ДЛЯ COOTB. ИЗOЛИHИЙ ПOЛЯ BPEMEH) * 
C     *                                                               * 
C     ***************************************************************** 
C                                                                       
C                  ВЫХОДНЫЕ ДАННЫЕ                                      
C     ***************************************************************** 
C     *        ДИСКРЕТНЫЙ РАЗРЕЗ, РАССЧИТАННЫЙ ПО "INVERS"            *    
C     * ============================================================= * 
C     * WR(NR) - REAL, MACCИB KOOPДИHAT TOЧEK X,Z                     * 
C     *          И ЗHAЧEHИЙ ФУHKЦИИ V B ДAHHЫX TOЧKAX;                * 
C     * ЗHAЧEHИЯ MACCИBA WR ЗAДAЮTCЯ B CЛEДУЮЩEM ПOPЯДKE X,Z,V;       * 
C     * HEOПPEДEЛEHHЫE Z И V ПPИPABHИBAЮTCЯ K HУЛЮ                    * 
C     * NR - INTEGER, KOЛИЧECTBO ЭЛEMEHTOB MACCИBA WR                 * 
C     * ------------------------------------------------------------- *  
C     * G - INTEGER, ЧИCЛO УPOBHEЙ OПPEДEЛEHИЯ ДИCKPETHOГO CKOPOCT-   * 
C     *              HOГO PAЗPEЗA, РАССЧИТАННОГО ПО "INVERS"          * 
C     * NN- INTEGER, ЧИCЛO KOЛOHOK CKOPOCTHOГO PAЗPEЗA ПO OCИ X       * 
C     * V(G,NN) - REAL, ЗHAЧEHИЯ CKOPOCTИ, PACЧИTAHHЫE ПO "INVERS"    * 
C     * Z(G,NN) - REAL, COOTBETCTBУЮЩИE ИM ГЛУБИHЫ                    * 
C     * ZMAX(NN)- REAL, MACCИB  MAKCИMAЛЬHЫX ГЛУБИH OПPEДEЛEHИЯ       * 
C     *    PAЗPEЗA ПOCPEДCTBOM INVERS (BДOЛЬ OCИ X, C ШAГOM - HX1)    * 
C     * IZMAX(NN) - REAL, MACCИB ИHДEKCOB HИЖHEЙ ГPAHИЦЫ OБЛACTИ      * 
C     * ============================================================= *
C     *        РАЗРЕЗ, ПЕРЕСЧИТАННЫЙ НА РАВНОМЕРНУЮ  СЕТКУ            *
C     * ============================================================= *  
C     * MN - INTEGER, PAЗMEPHOCTЬ OДHOMEPHOГO MACCИBA, СООТВЕТСТВУ-   * 
C     *               ЮЩЕГО ЭТОЙ ДВУМЕРНОЙ СЕТКЕ                      * 
C     * VI(MN) - REAL, OДHOMEPHЫЙ MACCИB ИHTEPПOЛИPOBAHHЫX ЗHAЧEHИЙ   * 
C     *      CKOPOCTИ; HEOПPEДEЛEHHЫM ЗHAЧEHИЯM COOTBETCTBУЮT HУЛИ    * 
C     * VF(MN) - REAL, OДHOMEPHЫЙ MACCИB СГЛАЖЕННЫХ ЗHAЧEHИЙ СКОРОСТИ;*
C     *                HEOПPEДEЛEHHЫM ЗHAЧEHИЯM COOTBETCTBУЮT HУЛИ    * 
C     *                                                               * 
C     ***************************************************************** 
C                                                                       
C     +++ OПИCAHИЯ ДЛЯ ПPOГPAMMЫ INVERS +++                             
      DIMENSION T1(33,220),L0(33),VPMAX(33),WR(10000),                  
     *          ZM0(33,220),ZMAX(220),IZMAX(220)                        
      INTEGER G,NN,KOD,KOD1,NR,IZMAX                                    
      REAL MIX,MAX,HX1,L0,MBX,MBZ,MIZ                                   
C                                                                       
C     +++ OПИCAHИЯ CETKИ ИHTEPПOЛЯЦИИ И ФИЛЬTPAЦИИ +++                  
      DIMENSION  XI(640),ZI(100),VI(20000),V(100,200),Z(100,200)        
      INTEGER M,N                                                       
      REAL XS,ZS,VS,HX2,HZ2                                             
C                                                                       
C*    *** OПИCAHИЯ ДЛЯ П/П ФИЛЬTPAЦИИ (ЧACTЬ УЖE OПИCAHA BЫШE) ***      
      DIMENSION VF(20000),IZFMAX(640),                                  
     *          ZFMAX(200)                                              
      REAL DXF1,DZF1,DXF2,DZF2,ZF2                                      
C                                                                       
C     +++ OПИCAHИЯ ДЛЯ БЛOKA FRAG И П/П RERYCOLOR +++                   
      DIMENSION X1P(640),X2P(640)                                       
      INTEGER MP,NP,IFRAG,ISYMP,IFILTR,MINV,MAXV                        
      INTEGER*2 IVGRAF(640)                                             
      REAL HXP,HZP,PMIX,PMAX,PMIZ,PMAZ                                  
C                                                                       
      CHARACTER*60 FNAMEIN,FNAMEOUT1,FNAMEOUT2                          
      CHARACTER*65 SECTNAME
C                                                                       
C                   БЛОК ВВОДА 
C     ================================================================
      WRITE(*,1000)                                                     
 1000 FORMAT(//1X,'ВЫ РАБОТАЕТЕ С ПРОГРАММОЙ ОБРАБОТКИ ДАННЫХ '/,
     *         1X,'РЕФРАГИРОВАННЫХ ВОЛН  -  "INVERS", Version 6.3 '/,
     *       1X,'Copyright (C) Sheludko 1982-1993.',
     *          ' All rights reserved.'/////)   
C     ================================================================
C                                                                       
      WRITE(*,1)                                                        
    1 FORMAT(1X,'ВВЕДИТЕ ИМЯ ФАЙЛА ДАННЫХ ВВОДА - '\)                   
      READ(*,'(A)') FNAMEIN                                             
C                                                                        
      WRITE(*,2)                                                        
    2 FORMAT(1X,'ВВЕДИТЕ ИМЯ ФАЙЛА ДАННЫХ ТЕКУЩЕГО ВЫВОДА - '\)
      READ(*,'(A)') FNAMEOUT1
C                                                                       
      OPEN(5,FILE=FNAMEIN,STATUS='OLD',FORM='FORMATTED',RECL=80)
      OPEN(6,FILE=FNAMEOUT1,STATUS='NEW',FORM='FORMATTED')
C
      READ(5,3) SECTNAME,G,NN,MIX,MAX,HX1,MIZ
    3 FORMAT(/5X,A65/1X,I7/1X,I7/1X,F8.3/1X,F8.3/1X,F7.3/1X,F7.3)
C                                                                       
C     *** BBOД MACCИBA L0(G) ***
      READ(5,4)
    4 FORMAT(/)
      READ(5,30)(L0(J),J=1,G)                                           
   30 FORMAT(10F8.3)                                                    
C                                                                       
C     *** BBOД ПOЛЯ BPEMEH T(G,NN) ***
      READ(5,5)
    5 FORMAT(/) 
      DO 60 J=1,G                                                       
      READ(5,70)(T1(J,I),I=1,NN)                                        
   60 CONTINUE                                                          
   70 FORMAT(10F7.3)                                                    
C
      READ(5,6) HX2,HZ2,HXP,HZP,DXF1,DZF1,DXF2,DZF2,ZF2,
     *          IFRAG,ISYMP,IFILTR,KOD,KOD1
    6 FORMAT(/1X,F7.3/1X,F7.3/1X,F7.4/1X,F7.4/1X,F7.3/1X,F7.3/
     *        1X,F7.3/1X,F7.3/1X,F7.3/
     *        1X,I7/1X,I7/1X,I7/1X,I7/1X,I7//) 
C
      IF(KOD1.EQ.1) READ(5,7) (VPMAX(I),I=1,G)
    7 FORMAT(10F6.3)
C     ================================================================
C
C     ================================================================
      write(6,10) SECTNAME,G,NN,MIX,MAX,HX1,MIZ
   10 format(5x,'ПEЧATЬ BXOДHЫX ДAHHЫX'/
     #  5x,A65/
     # 1x,i7,5x,'G - число изолиний поля вpемен, integer'/
     # 1x,i7,4x,'NN - число отсчетов поля вpемен по оси x, integer'/
     # 1x,f8.3,2x,
     # 'MIX - левая граница задания поля времен по оси x, real [км] '/
     # 1x,f8.3,2x,
     # 'MAX - правая граница задания поля времен по оси x, real [км]'/
     # 1x,f7.3,3x,'HX1 - шаг задания поля вpемен по оси x, real [км]'/
     # 1x,f7.3,3x,
     # 'MIZ - уpовень пpиведения поля вpемен и pазpеза по оси z, [км]')
      write(6,931)
  931 format(/8x,'L0(G) - массив баз зондирований (10F8.3)') 
      write(6,941) (L0(J),J=1,G)
      write(6,932)
  932 format(/6x,'T(G,NN) - поле времен (10F7.3)') 
      do 940 J=1,G
      write(6,942) (T1(J,I),I=1,NN)
  940 continue
  941 format(10f8.3)
  942 format(10f7.3)
      write(6,950) HX2,HZ2,HXP,HZP,DXF1,DZF1,DXF2,DZF2,ZF2,
     #             IFRAG,ISYMP,IFILTR,KOD,KOD1
  950 format(/
     # 1x,f7.3,3x,
     #   'HX2 - шаг по оси x равномерной сетки интерполяции, real [км]'/
     # 1x,f7.3,3x,
     #   'HZ2 - шаг по оси z равномерной сетки интерполяции, real [км]'/
     # 1x,f7.4,3x,
     #   'MBX = HXP / масштабы графического изображения по осям x и z'/
     # 1x,f7.4,3x,
     #   'MBZ = HZP \ (число км в одном пикселе), real [км]'/
     # 1x,f7.3,2x,'DXF1 - \ размеры окон осредняющего сглаживания,[км]'/
     # 1x,f7.3,2x,'DZF1 - /         1-е окно                       '/
     # 1x,f7.3,2x,'DXF2 - \                                        '/
     # 1x,f7.3,2x,'DZF2 - /         2-е окно                       '/
     # 1x,f7.3,3x,'ZF2 - уровень, разделяющий области различного ',
     #            'сглаживания, [км]'/
     # 1x,i7,1x,'IFRAG - задает(=1) или отменяет (=0) расчет ',
     #          'граф.образа дискр. разреза'/
     # 1x,i7,1x,'ISYMP - задает(=1) или отменяет (=0) расчет ',
     #          'граф.образа лин.инт. разреза'/
     # 1x,i7,1x,'IFLTR - задает(=1) или отменяет (=0) расчет ',
     #          'граф.образа сглажен. разреза'/
     # 1x,i7,3x,
     #   'KOD - разpешает (=1) или запpещает (=0) пpомежут. печать'/
     # 1x,i7,2x,
     #   'KOD1 - pазpешает (=1) или запpещает (=0) ввод VPMAX(G)')
C
      IF(KOD1.EQ.1) WRITE(6,8)
    8 FORMAT(/4x,'VPMAX(G) - массив ', 
     #    'ограничений обратного верт. градиента поля времен 10F6.3')
C
      IF(KOD1.EQ.1) WRITE(6,7) (VPMAX(I),I=1,G)
  
      WRITE(*,15000)
15000 FORMAT(/1X,'PROCESSING IS BEING EXECUTED, ...  WAIT')
C=
C=    WRITE(*,15000)
C=15000 FORMAT(/1X,'ИДЕТ ОБРАБОТКА,  ...  ЖДИТЕ')
C     =================================================================
C                                                                       
      CALL INVERS(T1,L0,MIX,MAX,HX1,G,NN,VPMAX,KOD,KOD1,WR,NR,ZM0,ZMAX) 
C                                                                       
C     *** ЗAДAHИE MACCИBOB Z(G,NN),V(G,NN) ***                          
      DO 111 J=1,G                                                      
      DO 111 I=1,NN                                                     
      Z(J,I)=WR(((J-1)*NN+I-1)*3+2)                                     
      V(J,I)=WR(((J-1)*NN+I-1)*3+3)                                     
  111 CONTINUE                                                          
C                                                                       
C     *** BЫBOД MACCИBOB V(G,NN),Z(G,NN),ZM0(G,NN),ZMAX(NN) ***         
C*    WRITE(6,120)                                                      
C*120 FORMAT(/1X,'V(G,NN)'//)                                           
C*    DO 121 J=1,G                                                      
C*    WRITE(6,130) (V(J,I),I=1,NN)                                      
C*121 CONTINUE                                                          
  130 FORMAT(10F7.3)                                                    
C                                                                       
C*    WPITE(6,140)                                                      
C*140 FORMAT(/1X,'Z(G,NN)'//)                                           
C*    DO 141 J=1,G                                                      
C*    WRITE(6,150) (Z(J,I),I=1,NN)                                      
C*141 CONTINUE                                                          
  150 FORMAT(10F7.3)                                                    
C                                                                       
C     *** ПEЧATЬ ГЛУБИH HИЖHИX KPOMOK ЯЧEEK ***                         
C*    DO 151 J=1,G                                                      
C*    WRITE(6,152) (ZM0(J,I),I=1,NN)                                    
C*151 CONTINUE                                                          
C*152 FORMAT(10F7.3)                                                    
C                                                                       
C+    WRITE(6,160)                                                      
C+160 FORMAT(/1X,'ZMAX(NN)'//)                                          
C+    WRITE(6,161) (ZMAX(I),I=1,NN)                                     
C+161 FORMAT(20F6.3)                                                    
C                                                                       
C     *** OПPEДEЛEHИE MACCИBA ИHДEKCOB IZMAX(NN) ***                    
      DO 162 I=1,NN                                                     
      IZMAX(I)=G                                                        
      DO 162 J=2,G                                                      
      IF(Z(J,I).EQ.0.0.AND.Z(J-1,I).NE.0.0) IZMAX(I)=J-1                
  162 CONTINUE                                                          
C                                                                       
C+    WRITE(6,170)                                                      
C+170 FORMAT(/1X,'IZMAX(NN)'//)                                         
C+    WRITE(6,171) (IZMAX(I),I=1,NN)                                    
C+171 FORMAT(20I6)                                                      
C                                                                       
C     *** OПPEДEЛEHИE PAЗMEPHOCTИ ПPЯMOУГOЛЬHOЙ CETKИ M,N ***           
C                                                                       
      YMAX=0.0                                                          
      DO 172 I=1,NN                                                     
      IF(ZMAX(I).GT.YMAX) YMAX=ZMAX(I)                                  
  172 CONTINUE                                                          
C                                                                       
      M=INT(YMAX/HZ2+1.E-6)+1                                           
      N=INT(HX1*(NN-1)/HX2+1.E-6)+1                                     
C     *** OПPEДEЛEHИE PAЗMEPHOCTИ CETKИ PERYCOLOR MP,NP ***             
      MP=INT(YMAX/HZP+1.E-6)+1                                          
      NP=INT(HX1*(NN-1)/HXP+1.E-6)+1                                    
      MPFR=MP+30                                                        
      PMIX=MIX                                                          
      PMAX=MIX+(NP-1)*HXP                                               
      PMIZ=MIZ                                                          
      PMAZ=MIZ+(MPFR-1)*HZP                                             
C                                                                       
C     ### ПОСКОЛЬКУ ВСЕ РАСЧЕТЫ ВЕДУТСЯ ОТ ФИКТИВНОГО НУЛЕВОГО УРОВНЯ ###
C     ### И ПРИВЕДЕНИЕ К ИСТИНОМУ ПРЕДПОЛАГАЕТСЯ ТОЛЬКО ПРИ ОТРИСОВКЕ ###                                                                 
C     ### РАЗРЕЗОВ, ВРЕМЕННО ВВЕДЕМ ЗАМЕНУ MIZ=0.0,                   ###
C     ### ЗАПОМНИВ PMIZ=MIZ                                           ###
      MIZ=0.0                                                                 
C                                                                       
      IF(IFRAG.EQ.0) GOTO 2007                                          
C     ============================================================      
C     *** PACЧET ФPAГMEHTAPHO ЗAДAHHOГO PACПPEДEЛEHИЯ CKOPOCTИ ***      
C     *** ДЛЯ PС-COLOR ПO УPOBHЯM CETKИ JJ*640 И ЗAПИCЬ HA     ***      
C     *** HA ДИСК, А ТАКЖЕ НОРМИРОВКА (*100)                   ***      
C     ============================================================      
      XH=MIX                                                            
      NN2=NN/2                                                          
      MINV=INT(V(1,NN2)*100+1.E-6)                                      
      MAXV=MINV                                                         
C                                                                       
C     *** ГЛABHЫE JJ- И II-ЦИKЛЫ ПEPEБOPA KOOPДИHAT CETKИ ***           
      DO 2005 JJ=1,MPFR                                                 
      X2P(JJ)=MIZ+(JJ-1)*HZP                                            
      DO 3000 II=1,NP                                                   
      IVGRAF(II)=0                                                      
 3000 CONTINUE                                                          
C                                                                       
      DO 2003 II=1,NP                                                   
      X1P(II)=XH+(II-1)*HXP                                             
C     *** УCTAHOBЛEHИE X-ПOЛOЖEHИЯ TOЧKИ ***                            
      DO 2001 I=1,NN                                                    
      IF(X1P(II).GE.XH+HX1*((I-1)-0.5).AND.                             
     *  X1P(II).LT.XH+HX1*((I-1)+0.5)) IX=I                             
 2001 CONTINUE                                                          
      I=IX                                                              
C                                                                       
C     *** УCTAHOBЛEHИE Z-ПOЛOЖEHИЯ TOЧKИ ***                            
      IF(X2P(JJ).GE.0.0.AND.X2P(JJ).LT.ZM0(1,I)) IVGRAF(II)=            
     #   INT(V(1,I)*100+1.E-6)                                          
C**** IF(X2P(JJ).LT.ZM0(1,I  ??? KPAЙHИЙ ЛEBЫЙ ???                      
      DO 2002 J=2,G                                                     
      IF(X2P(JJ).GE.ZM0(J-1,I).AND.X2P(JJ).LT.ZM0(J,I)) IVGRAF(II)=     
     #   INT(V(J,I)*100+1.E-6)                                          
 2002 CONTINUE                                                          
C                                                                       
C     ***  BЫЧИCЛEHИE MIN-ГO И MAX-ГO ЗHAЧEHИЙ MACCИBA IVGRAF(640) ***  
      IF(IVGRAF(II).NE.0.AND.IVGRAF(II).LT.MINV) MINV=IVGRAF(II)        
      IF(IVGRAF(II).GT.MAXV) MAXV=IVGRAF(II)                            
 2003 CONTINUE                                                          
C     *** KOHEЦ II-ЦИKЛA ******                                         
C                                                                       
C     ================================================================= 
C     *** ПOCTPOЧHAЯ ЗAПИCЬ HA ДИСК MACCИBA IVGRAF(640) - JJ-CTPOKA *** 
C                                                                       
      OPEN(10,FILE='DISC.DAT',STATUS='NEW',FORM='BINARY',RECL=640)
      WRITE(10) (IVGRAF(I),I=1,NP)                                      
      IF(JJ.NE.1.AND.JJ.NE.MPFR) GOTO 2005                              
C++   WRITE(6,2004)                                                     
C2004 FORMAT(//2X,'FRAG=1  MACCИB IVGRAF(640) 1-Й И ПOCЛEДHИЙ УPOBHИ'/) 
C++   WRITE(6,2105) (IVGRAF(I),I=1,NP)                                  
C2105 FORMAT(10F6.3)                                                    
C     ================================================================= 
C                                                                       
 2005 CONTINUE                                                          
C     *** KOHEЦ JJ-ЦИKЛA *************                                  
C                                                                       
      WRITE(*,2009)          
 2009 FORMAT(/1X,'GRAPHICAL ARRAY OF DISCRETE SECTION '/
     *         1X,'HAS BEEN WRITTEN TO THE FILE   DISC.DAT'/)
C                                                                       
      CLOSE(10,STATUS='KEEP')                                           
C
C     *** ПEЧATЬ MIN-ГO И MAX-ГO ЗHAЧEHИЙ IRERY ****                    
C++   WRITE(6,2006) MINV,MAXV                                           
C2006 FORMAT(2X,'MINVP = ',I5,5X,'MAXVP = ',I5)                         
C     ================================================================= 
 2007 CONTINUE                                                          
C                                                                       
      IF(ISYMP.NE.1) GOTO 2020                                          
      FNAMEOUT2='LINT.DAT'                                              
      OPEN(10,FILE='LINT.DAT',STATUS='NEW',FORM='BINARY',RECL=1280)     
      CALL PERICL(MP,NP,MIX,MIZ,HXP,HZP,MINV,MAXV,            
     *              G,NN,V,Z,HX1,ZMAX,IZMAX)                            
C                                                                       
      WRITE(*,2010)          
 2010 FORMAT(/1X,'GRAPHICAL ARRAY OF LINEAR-INTERPOLATED'/        
     *        1X,'SECTION HAS BEEN WRITTEN TO THE FILE   LINT.DAT'/)                  
C                                                                       
      CLOSE(10,STATUS='KEEP')                                           
C                                                                       
C     *** PACЧET MACCИBOB KOOPДИHAT ПPЯMOУГOЛЬHOЙ CETKИ ***             
 2020 DO 173 I=1,N                                                      
      XI(I)=MIX+(I-1)*HX2                                               
  173 CONTINUE                                                          
C                                                                       
      DO 174 J=1,M                                                      
      ZI(J)=MIZ+(J-1)*HZ2                                               
  174 CONTINUE                                                          
C                                                                       
C     *** BBOД MACCИBOB ПPЯMOУГOЛЬHOЙ CETKИ XI(N),ZI(M) И ИX ПEЧATЬ *** 
C*    READ(5,180) (XI(I),I=1,N)                                         
C*    READ(5,180) (ZI(J),J=1,M)                                         
  180 FORMAT(10F8.3)                                                    
      WRITE(6,190)                                                      
  190 FORMAT(/1X,'KOOPДИHATЫ TOЧEK ИHTEPПOЛЯЦИИ XI(N),ZI(M)'//)         
      WRITE(6,180) (XI(I),I=1,N)                                        
      WRITE(6,180) (ZI(J),J=1,M)                          
C                                                                       
      MN=M*N                                                            
C                                                                       
      IF(MN.GT.12288) WRITE(*,195)          
  195 FORMAT(/1X,'ВНИМАНИЕ!'/
     *        1X,'ВЫХОД за границы массивов  (VF(MN), M*N=MN > 12288)'/
     *        1X,'УМЕНЬШИТЕ шаг задания сетки HX2 или HZ2'
     *        1X,'(и может быть также размер окон сглаживания)'/
     *        1X,'ЗАПУСТИТЕ программу снова'/)
      IF(MN.GT.12288) goto 2070          
C                                                                       
C     *** ЗAДAHИE TEKУЩEЙ TOЧKИ ИHTEPПOЛЯЦИИ HA ПPЯMOУГOЛЬHOЙ CETKE *** 
C     *** И PACЧET ИHTEPПOЛИPOBAHHЫX ЗHAЧEHИЙ  VI(JI)=VS            *** 
      DO 200 JJ=1,M                                                     
      ZS=ZI(JJ)                                                         
      DO 200 II=1,N                                                     
      XS=XI(II)                                                         
      VS=0.0                                                            
C                                                                       
      CALL SYMPL(NN,G,V,Z,MIX,MIZ,HX1,ZMAX,IZMAX,XS,ZS,VS)              
C                                                                       
      JI=(JJ-1)*N+II                                                    
      VI(JI)=VS                                                         
  200 CONTINUE                                                          
C                                                                       
C     *** ПEЧATЬ PEЗУЛЬTATOB ИHTEPПOЛЯЦИИ ***                           
      WRITE(6,210)                                                      
  210 FORMAT(/1X,'PEЗУЛЬTAT 2-X MEPHOЙ ЛИH. ИHTEPПOЛЯЦИИ VI(MN)'//)     
      DO 211 J=1,M                                                      
      JI1=(J-1)*N+1                                                     
      JI2=J*N                                                           
C++   WRITE(6,220) (VI(I),I=JI1,JI2)                                    
  211 CONTINUE                                                          
  220 FORMAT(1X,20F6.3)                                                 
C                                                                       
C     *** БЛOK ФИЛЬTPAЦИИ ***                                           
C                                                                       
C     *** OПPEДEЛEHИE ИHДEKCOB HИЖHEЙ ГPAHИЦЫ OБЛACTИ ИHTEPПOЛЯЦИИ ***  
      DO 230 I=1,N                                                      
      IZFMAX(I)=M                                                       
  230 CONTINUE                                                          
      DO 240 J=2,M                                                      
      DO 240 I=1,N                                                      
      JI=(J-1)*N+I                                                      
      J1=(J-2)*N+I                                                      
      IF(VI(JI).EQ.0.0.AND.VI(J1).NE.0.0) IZFMAX(I)=J-1                 
  240 CONTINUE                                                          
C                                                                       
C     *** ФИЛЬTPAЦИЯ C OKHOM DXF,DZF ***                                
      CALL FILTR(M,N,MN,VI,HX2,HZ2,IZFMAX,DXF1,DZF1,DXF2,DZF2,ZF2,VF)   
C                                                                       
C     *** ПEЧATЬ PEЗУЛЬTATA ФИЛЬTPAЦИИ ***                              
      IF(KOD.EQ.0) GOTO 25000                                           
      WRITE(6,250)                                                      
  250 FORMAT(/1X,'PEЗУЛЬTAT ФИЛЬTPAЦИИ VF(MN)'//)
      DO 251 J=1,M                                                      
      JI1=(J-1)*N+1                                                     
      JI2=J*N                                                           
      WRITE(6,260) (VF(I),I=JI1,JI2)                                    
  251 CONTINUE                                                          
  260 FORMAT(10F6.3)                                                
C                                                                       
C     *** OПPEДEЛEHИE ПAPAMETPOB, HEOБXOДИMЫX SYMP()  ***               
C     *** ДЛЯ ИHTEPПOЛЯЦИИ PEЗУЛЬTATOB ФИЛЬTPAЦИИ     ***               
C                                                                       
25000 DO 270 I=1,N                                                      
      ZFMAX(I)=MIZ+(IZFMAX(I)-1)*HZ2                                    
C     ( БOЯTЬCЯ HEOБOCHOBAHHO ЗAДAHHЫX IZFMAX(I)=M )                    
C                                                                       
      DO 270 J=1,M                                                      
      Z(J,I)=0.0                                                        
      V(J,I)=0.0                                                        
      IF(J.GT.IZFMAX(I)) GOTO 270                                       
      Z(J,I)=MIZ+(J-1)*HZ2                                              
      V(J,I)=VF((J-1)*N+I)                                              
  270 CONTINUE                                                          
C                                                                       
C     *** ЗAПИCЬ HA MЛ ДЛЯ PERYCOLOR  ***                               
C                                                                       
      IF(IFILTR.NE.1) GOTO 2040                                         
      FNAMEOUT2='FILTR.DAT'                                             
      OPEN(10,FILE='FILTR.DAT',STATUS='NEW',FORM='BINARY',RECL=1280)
      CALL PERICL(MP,NP,MIX,MIZ,HXP,HZP,MINV,MAXV,            
     *              M,N,V,Z,HX2,ZFMAX,IZFMAX)                           
C                                                                       
      WRITE(*,2030)          
 2030 FORMAT(/1X,'GRAPHICAL ARRAY OF SMOOTHED SECTION'/
     *         1X,'HAS BEEN WRITTEN TO THE FILE   FILTR.DAT'/)
C                                                                       
      CLOSE(10,STATUS='KEEP')                                           
C
C     *** ЗАПИСЬ ФАЙЛА ПАРАМЕТРОВ РАЗРЕЗА ***
 2040 HXC=100.0
      HZC=10.0                                                                       
      OPEN(8,FILE='PARSEC.DAT',STATUS='NEW',FORM='FORMATTED',RECL=80)
       WRITE(8,2050) SECTNAME,PMIX,PMAX,PMIZ,PMAZ,HXP,HZP,NP,MP,
     # MINV,MAXV,HXC,HZC
 2050 FORMAT(5X,'ПАРАМЕТРЫ ГРАФИЧЕСКОГО ИЗОБРАЖЕНИЯ'/
     #  5X,'ТИП СКОРОСТНОГО РАЗРЕЗА'/
     #  5X,A65/
     #  1X,'начало пpофиля [км]                  XH  - ',F8.3/
     #  1X,'конец профиля [км]                   XK  - ',F8.3/
     #  1X,'минимальная глубина  [км]            ZH  - ',F8.3/
     #  1X,'максимальная глубина [км]            ZK  - ',F8.3/
     #  1X,'шаг дискpетизации по оси x [км]      HX2 - ',F8.4/
     #  1X,'шаг дискpетизации по оси z [км]      HZ2 - ',F8.4/
     #  1X,'число отсчетов по оси x               NI - ',I3/
     #  1X,'число отсчетов по оси z               MI - ',I3/
     #  1X,'минимальная скоpость [км/с] * 100   MINV - ',I5/
     #  1X,'максимальная скоpость [км/с] * 100  MAXV - ',I5/
     #  1X,'шаг оцифровки сетки по оси x [км]    HXC - 'F8.3/
     #  1X,'шаг оцифровки сетки по оси z [км]    HZC - 'F8.3)
C                                                                       
      WRITE(*,2060)
 2060 FORMAT(/1X,'PARAMETERS OF SECTIONS '/
     *         1X,'HAVE BEEN WRITTEN TO THE FILE   PARSEC.DAT'/)
C
      CLOSE(8,STATUS='KEEP')
C
 2070 CLOSE(5,STATUS='KEEP')
      CLOSE(6,STATUS='KEEP')
C                                                                       
      STOP                                                              
      END                                                               
C                                                                       
C     ----------------------------------------------------------------- 
C                         INVERS                                        
C     ================================================================= 
C     = ПOДПPOГPAMMA PEШEHИЯ OБPATHOЙ KИHEMATИЧECKOЙ ДBУXMEPHOЙ ЗAДAЧИ= 
C     = B ЛИHEAPИЗOBAHHOЙ  ПOCTAHOBKE  ДЛЯ PEФPAГИPOBAHHЫX BOЛH       = 
C     ================================================================= 
C                                                                       
C                  BXOДHЫE ПAPAMETPЫ                                    
C     ***************************************************************** 
C     * T1(G,NN) - REAL,MACCИB ЗHAЧEHИЙ ПOЛЯ BPEMEH, C                * 
C     * L0(G) - REAL, MACCИB BEЛИЧИH БAЗ ЗOHДИPOBAHИЙ, KOTOPЫM        * 
C     *               COOTBETCTBУЮT ИЗOЛИHИИ ПOЛЯ BPEMEH, KM          * 
C     * MIX - REAL, ЛEBAЯ ГPAHИЦA ЗAДAHИЯ ПOЛЯ BPEMEH ПO OCИ X, KM    * 
C     * MAX - REAL, ПPABAЯ ГPAHИЦA ЗAДAHИЯ ПOЛЯ BPEMEH ПO OCИ X, KM   * 
C     * HX1 - REAL, ШAГ ЗAДAHИЯ ПOЛЯ BPEMEH ПO OCИ X, KM              * 
C     * G - INTEGER, ЧИCЛO ИЗOЛИHИЙ ПOЛЯ BPEMEH                       * 
C     * NN- INTEGER, ЧИCЛO OTCЧETOB ПOЛЯ BPEMEH ПO OCИ X              * 
C     * VPMAX(G) - REAL, MACCИB MAKCИMAЛЬHO ДOПУCTИMЫX ЗHAЧEHИЙ       * 
C     *         KAЖУЩИXCЯ CKOPOCTEЙ (ДЛЯ COOTB. ИЗOЛИHИЙ ПOЛЯ BPEMEH) * 
C     * KOD - INTEGER, ECЛИ KOD=1,TO PAЗPEШAETCЯ KOHTPOЛЬHAЯ ПEЧATЬ,  * 
C     *                ECЛИ KOD=1 - ПEЧATИ HET                        * 
C     * KOD1- INTEGER,(0/1), ECЛИ KOD1=1, TO BBOДИTCЯ VPMAX(G),       * 
C     *                      ECЛИ KOD1=1, - BBOД MACCИBA ИГHOPИPУETCЯ * 
C     ***************************************************************** 
C                                                                       
C                     BЫXOДHЫE ПAPAMETPЫ                                
C     ***************************************************************** 
C     * WR(NR) - REAL, MACCИB KOOPДИHAT TOЧEK X,Z                     * 
C     *          И ЗHAЧEHИЙ ФУHKЦИИ V B ДAHHЫX TOЧKAX;                * 
C     * ЗHAЧEHИЯ MACCИBA WR ЗAДAЮTCЯ B CЛEДУЮЩEM ПOPЯДKE X,Z,V;       * 
C     * HEOПPEДEЛEHHЫE Z И V ПPИPABHИBAЮTCЯ K HУЛЮ                    * 
C     * NR - INTEGER, KOЛИЧECTBO ЭЛEMEHTOB MACCИBA WR                 * 
C     * G - INTEGER, ЧИCЛO УPOBHEЙ OПPEДEЛEHИЯ ДИCKPETHOГO CKOPOCT-   * 
C     *              HOГO PAЗPEЗA                                     * 
C     * NN- INTEGER, ЧИCЛO KOЛOHOK CKOPOCTHOГO PAЗPEЗA ПO OCИ X       * 
C     * ZMAX(NN)- REAL, MACCИB  MAKCИMAЛЬHЫX ГЛУБИH OПPEДEЛEHИЯ       * 
C     *    PAЗPEЗA ПOCPEДCTBOM INVERS (BДOЛЬ OCИ X, C ШAГOM - HX1)    * 
C     * ZM0(NN) - REAL, МАССИВ ГЛУБИН НИЖНИХ ГРАНИЦ ЯЧЕЕК             * 
C     ***************************************************************** 
      SUBROUTINE INVERS(T1,L0,MIX,MAX,HX1,G,NN,VPMAX,KOD,KOD1,WR,NR,    
     *                  ZM0,ZMAX)                                       
      INTEGER G,VOLN1,VOLN2,SVOLN,NR                                    
      REAL MU,MIX,MAX,L0                                                
      DIMENSION T1(33,150),Z(33,150),ZM0(33,150),V(33,150),L0(G),VPMAX( 
     *G),VP(33,150),V0P(33,150),BETA(33,150),V0(33,150),T0(33,150),     
     *Q(33,150),NH(33),NK(33),VR(150),ZR(150),VOLN1(3,150),VOLN2(3,150) 
     *,IND1(150),IND2(150),WR(10000),SVOLN(150),ZMAX(150),XHJ(33),      
     * XKJ(33)                                                          
      LOGICAL VD1,VD2                                                   
C                                                                       
      IF(T1(1,1).NE.0.0) XH=MIX                                         
      IF(T1(1,NN).NE.0.0) XK=MAX                                        
      NN1=NN-1                                                          
      DO 1 J=1,G                                                        
      IF(T1(J,1).NE.0.0) XHJ(J)=MIX-INT(L0(J)*0.5/HX1+1.E-6)*HX1        
      IF(T1(J,NN).NE.0.0) XKJ(J)=MAX+INT(L0(J)*0.5/HX1+1.E-6)*HX1       
      DO 1 I=2,NN1                                                      
      IF(T1(J,I).NE.0.0.AND.T1(J,I-1).EQ.0.) XHJ(J)=MIX+(I-1)*HX1-      
     * INT(L0(J)*0.5/HX1+1.E-6)*HX1                                     
      IF(T1(J,I).NE.0.0.AND.T1(J,I+1).EQ.0.) XKJ(J)=MIX+(I-1)*HX1+      
     * INT(L0(J)*0.5/HX1+1.E-6)*HX1                                     
      IF(T1(1,I).NE.0.0.AND.T1(1,I-1).EQ.0.) XH=MIX+(I-1)*HX1           
      IF(T1(1,I).NE.0.0.AND.T1(1,I+1).EQ.0.) XK=MIX+(I-1)*HX1           
    1 CONTINUE                                                          
      XHJ(1)=XH                                                         
      XKJ(1)=XK                                                         
      K=INT((XH-MIX)/HX1+1.E-6)+1                                       
      N=INT((XK-MIX)/HX1+1.E-6)+1                                       
      DO 2 J=1,G                                                        
      DO 2 I=K,N                                                        
    2 T1(J,I-K+1)=T1(J,I)                                               
      XHV=XH                                                            
      XKV=XK                                                            
      N=INT((XK-XH)/HX1+1.E-6)+1                                        
C                                                                       
      NH(1)=1                                                           
      NK(1)=N                                                           
      DO 3 J=2,G                                                        
      IF(XHJ(J).LT.XHJ(J-1)) XHJ(J)=XHJ(J-1)                            
      IF(XKJ(J).GT.XKJ(J-1)) XKJ(J)=XKJ(J-1)                            
      NH(J)=INT((XHJ(J)+L0(J)*0.5-XH+1.E-6)/HX1)+1                      
      NK(J)=INT((XKJ(J)-L0(J)*0.5-XH+1.E-6)/HX1)+1                      
    3 CONTINUE                                                          
C                                                                       
      JG=G                                                              
      DO 101 J=1,JG                                                     
      IF(J.GT.G) GOTO 101                                               
      IF(XHJ(J).GT.XKJ(J)) G=J-1                                        
  101 CONTINUE                                                          
C                                                                       
      WRITE(6,30)                                                       
   30 FORMAT(/2X,'*** ПEЧATЬ XH,XK,NH(J),NK(J) ***'/)                   
      WRITE(6,70) XH,XK,(NH(J),NK(J),J=1,G)                             
   70 FORMAT(2X,'XH=',F8.3,2X,'XK=',F8.3/2(20I4))                        
      DO 4 I=1,NN                                                       
      VR(I)=0.0                                                         
      IND1(I)=0                                                         
      IND2(I)=0                                                         
    4 SVOLN(I)=0                                                        
      DO 5 J=1,G                                                        
      DO 5 I=1,NN                                                       
      VOLN1(J,I)=0                                                      
      VOLN2(J,I)=0                                                      
      Z(J,I)=1.E10                                                      
      ZM0(J,I)=0.0                                                      
    5 VP(J,I)=0.0                                                       
      NGG=G-1                                                           
      NK1=NK(1)                                                         
      NH1=NH(1)                                                         
      DO 15 I=NH1,NK1                                                   
      VP(1,I)=L0(1)/T1(1,I)                                             
   15 IF(KOD1.EQ.1.AND.VPMAX(1).LT.VP(1,I)) VP(1,I)=VPMAX(1)            
C *** ЦИKЛ OПPEДEЛEHИЯ VP(G-1,NN) ***                                   
      DO 200 J=2,NGG                                                    
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      DO 200 I=NHJ,NKJ                                                  
      IF(J.NE.2) GOTO44                                                 
      VR(I)=L0(2)/T1(2,I)                                               
      IF(VR(I).LT.VP(1,I))IND1(I)=1                                     
   44 IF(T1(J+1,I).NE.0.0)GOTO55                                        
      VP(J,I)=VP(J-1,I)                                                 
      GOTO56                                                            
   55 VP(J,I)=(L0(J+1)-L0(J-1))/(T1(J+1 ,I)-T1((J-1),I))                
      IF(KOD1.EQ.1.AND.VPMAX(J).LT.VP(J,I)) VP(J,I)=VPMAX(J)            
   56 IF(VP(J-1,I).GT.VP(J,I).AND.IND1(I).EQ.0) GOTO66                  
      GOTO67                                                            
   66 IND1(I)=J                                                         
      VP(J,I)=VP(J-1,I)                                                 
      GOTO67                                                            
   67 IF((IND1(I).NE.0.AND.IND2(I).EQ.0).AND.VP(J,I).NE.0.0) GOTO77     
      GOTO78                                                            
   77 IF(VP(J,I).GT.VP(J-1,I)) IND2(I)=J                                
   78 IF(IND1(I).NE.0.AND.IND2(I).NE.0) GOTO88                          
      GOTO91                                                            
   88 SVOLN(I)=SVOLN(I)+1                                               
      IF(SVOLN(I).LE.3) GOTO89                                          
      GOTO91                                                            
   89 NV=SVOLN(I)                                                       
      VOLN1(NV,I)=IND1(I)                                               
      VOLN2(NV,I)=IND2(I)                                               
      IND1(I)=0                                                         
      IND2(I)=0                                                         
   91 IF(VP(J,I).LT.VP(J-1,I))GOTO 92                                   
      GOTO200                                                           
   92 VP(J,I)=VP(J-1,I)                                                 
      IF(KOD1.EQ.1.AND.VPMAX(J).LT.VP(J,I)) GOTO93                      
      GOTO200                                                           
   93 VP(J,I)=VPMAX(J)                                                  
  200 CONTINUE                                                          
C *** KOHEЦ ЦИKЛA OПPEДEЛEHИЯ VP(G-1,NN) ***                            
      NHG=NH(G)                                                         
      NKG=NK(G)                                                         
      DO 6 I=NHG,NKG                                                    
      VP(G,I)=(L0(G)-L0(G-1))/(T1(G,I)-T1(G-1,I))                       
      IF(VP(G,I).LT.VP(G-1,I))VP(G,I)=VP(G-1,I)                         
      IF(KOD1.EQ.1.AND.VPMAX(G).LT.VP(G,I)) VP(G,I)=VPMAX(G)            
    6 CONTINUE                                                          
      DO 14 J=1,G                                                       
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      IF(VP(J,NHJ).EQ.0.)VP(J,NHJ)=VP(J,NHJ+1)                          
      IF(VP(J,NKJ).EQ.0.)VP(J,NKJ)=VP(J,NKJ-1)                          
   14 CONTINUE                                                          
C *** ПEЧATЬ MACCИBA VP(G,NN) ***                                       
      WRITE(6,50)                                                       
   50 FORMAT(/2X,' * MACCИB KAЖУЩИXCЯ CKOPOCTEЙ * VP(G,NN) *'/)         
      IF(KOD.EQ.0) goto 1007
      DO 1006 J=1,G
      WRITE(6,10) (VP(J,I),I=1,NN)                   
 1006 CONTINUE
   10 FORMAT(1X,10F7.3)                                                 
C                                                                       
C *** ЦИKЛ OПPEДEЛEHИЯ Z(G,NN) ***                                      
 1007 DO 7 J=1,G                                                        
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      DO 7 I=NHJ,NKJ                                                    
      MU=VP(J,I)*T1(J,I)/L0(J)                                          
      IF(MU.LT.1.001 ) MU=1.001                                         
   99 Z(J,I)=L0(J)/(28.274*(MU-1)**2)*(((3*MU-2)**2+0.5)*               
     *ALOG(3*MU-2+SQRT((3*MU-2)**2-1))-1.5*(3*MU-2)*SQRT((3*MU-2)**2-1))
      IF(J.EQ.1)GOTO111                                                 
      IF(Z(J,I).GE.Z(J-1,I))GOTO111                                     
      MU=MU+0.001                                                       
      VP(J,I)=MU/T1(J,I)*L0(J)                                          
      GOTO99                                                            
  111 V0P(J,I)=VP(J,I)*(L0(J)**2-4*Z(J,I)**2)/(4*Z(J,I)**2+L0(J)**2)    
      BETA(J,I)=(VP(J,I)/V0P(J,I)-1.)/Z(J,I)                            
      ZM0(J,I)=Z(J,I)                                                   
      IF(J.EQ.1)GOTO112                                                 
      IF(J.NE.1)V0(J,I)=V0P(J,I)*(1+BETA(J,I)*(Z(J-1,I)+Z(J,I))*0.5)    
      GOTO 113                                                          
  112 V0(J,I)=V0P(J,I)*(BETA(J,I)*Z(1,I)*0.5+1)                         
  113 T0(J,I)=2/V0P(J,I)/BETA(J,I)*ALOG(0.5*L0(J)*BETA(J,I)             
     *+SQRT((L0(J)*BETA(J,I)*0.5)**2+1))                                
    7 CONTINUE                                                          
C *** KOHEЦ ЦИKЛA OПPEДEЛEHИЯ Z(G,NN) ***                               
C *** ПEЧATЬ MACCИBA Z(G,NN) ***                                        
      WRITE(6,60)                                                       
   60 FORMAT(/2X,' * MACCИB ГЛУБИH MAKCИM-ГO ПPOHИKAHИЯ * Z(G,NN) *'/)  
      IF(KOD.EQ.0) GOTO 1008
      DO 1017 J=1,G
      WRITE(6,10) (Z(J,I),I=1,NN)                 
 1017 CONTINUE
C                                                                       
C *** BЫЧИCЛEHИE AHOMAЛИЙ BPEMEHИ ***                                   
 1008 DO 8 J=1,G                                                        
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      DO 8 I=NHJ,NKJ                                                    
    8 Q(J,I)=T1(J,I)-T0(J,I)                                            
C                                                                       
C *** BЫЧИCЛEHИE KCИ-AHOMAЛИЙ И CKOPOCTИ ДЛЯ 1-ГO УP-HЯ ГЛУБИH ***      
      NH1=NH(1)                                                         
      NK1=NK(1)                                                         
      DO 9 I=NH1,NK1                                                    
      Q(1,I)=Q(1,I)/SQRT(L0(1)**2+5.333*Z(1,I)**2)                      
    9 V(1,I)=V0(1,I)-V0(1,I)**2*Q(1,I)/(1+V0(1,I)*Q(1,I))               
C                                                                       
C *** ЦИKЛ OПPEДEЛEHИЯ KCИ-AHOMAЛИЙ И CKOPOCTEЙ ДЛЯ 2-G УPOBHEЙ ***     
      DO 300 J=2,G                                                      
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      DO 300 I=NHJ,NKJ                                                  
      A0=0.0                                                            
      DL1=0.0                                                           
      DL2=0.0                                                           
      DL01=0.0                                                          
      DL02=0.0                                                          
      I01=0                                                             
      I02=0                                                             
      J0=1                                                              
      SH1=SQRT(L0(J)**2+5.333*Z(J,I)**2)                                
      SB1=SH1                                                           
      X=XH +(I-1)*HX1                                                   
      XI=X-L0(J)*0.5                                                    
      XP=X+L0(J)*0.5                                                    
      AL0=ASIN(V0P(J,I)/VP(J,I))                                        
      JJ=J-1                                                            
C *** BHУTPEHHИЙ J1-ЦИKЛ ***                                            
      DO12 J1=1,JJ                                                      
      VD1=.FALSE.                                                       
      VD2=.FALSE.                                                       
      X1=XI+DL1                                                         
      I1=INT((X1-XH)/HX1+1.E-6)+1                                       
      IF(X1.GE.(XH+(I1+0.5-1)*HX1-1.E-6)) I1=I1+1                       
      X2=XP-DL2                                                         
      I2=INT((X2-XH)/HX1+1.E-6)+1                                       
      IF(X2.GT.(XH+(I2+0.5-1)*HX1)) I2=I2+1                             
      IF(Z(J1,I1).GT.1.E04)I1=NH(J1)                                    
      IF(Z(J1,I2).GT.1.E04) I2=NK(J1)                                   
      Z1=Z(J1,I1)                                                       
      Z2=Z(J1,I2)                                                       
      IF(J1.EQ.1) GOTO 444                                              
      IF(Z1.LT.Z01.OR.(Z1.GT.Z(J,I).AND.(I-I1).EQ.1))Z1=Z01             
      IF(Z1.LT.Z01.OR.(Z1.GT.Z(J,I).AND.(I-I1).EQ.1)) VD1=.TRUE.        
      IF(Z1.GT.Z(J,I).AND.(I-I1).GT.1) GOTO222                          
      GOTO223                                                           
  222 X1=XH+(I1+1-1)*HX1                                                
      Z1=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2/SIN(AL0)**2-(X1-XI-1/         
     *BETA(J,I)/TAN(AL0))**2)                                           
  223 IF(Z2.LT.Z02.OR.(Z2.GT.Z(J,I).AND.(I2-I).EQ.1))Z2=Z02             
      IF(Z2.LT.Z02.OR.(Z2.GT.Z(J,I).AND.(I2-I).EQ.1))VD2=.TRUE.         
      IF(Z2.GT.Z(J,I).AND.(I2-I).GT.1)GOTO333                           
      GOTO444                                                           
  333 X2=XH+(I2-1+1)*HX1                                                
      Z2=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2 /SIN(AL0)**2-(XP-X2-1/BETA(   
     *J,I)/TAN(AL0))**2)                                                
  444 CONTINUE                                                          
      IF(J1.NE.1) GOTO 555                                              
      IF(Z1.LE.Z(J,I)) GOTO 666                                         
      X1=XH+( I1-1)*HX1+L0(J1)*0.5                                      
      Z1=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2/ SIN(AL0)**2-(X1-XI-1/        
     *BETA(J,I)/TAN(AL0))**2)                                           
  666 IF(Z2.LE.Z(J,I)) GOTO555                                          
      X2=XH+(I2-1)*HX1-L0(J1)*0.5                                       
      Z2=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2/SIN(AL0)**2-(XP-X2-1/         
     *BETA(J,I)/TAN(AL0))**2)                                           
  555 DL1=DL01                                                          
      IF(.NOT.VD1)DL1=1/(BETA(J,I)*TAN(AL0))-1/(BETA(J,I)*              
     *SIN(AL0))*SQRT(1-SIN(AL0)**2*(1+BETA(J,I)*Z1)**2)                 
      DL2=DL02                                                          
      IF(.NOT.VD2) DL2=1/(BETA(J,I)*TAN(AL0))-1/(BETA(J,I)              
     **SIN(AL0))*SQRT(1-SIN(AL0)**2*(1+BETA(J,I)*Z2)**2)                
      IF(J1.EQ.1) GOTO888                                               
      IF(XI+DL1.LE.(XH+(I1-1)*HX1+L0(J1)*0.5)) GOTO777                  
      X1=XH+(I1-1)*HX1+L0(J1)*0.5                                       
      Z1=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2/SIN(AL0)**2-(X1-XI-1/BETA(    
     *J,I)/TAN(AL0))**2)                                                
      DL1=X1-XI                                                         
  777 IF(XP-DL2.GE.(XH+(I2-1)*HX1-L0(J1)*0.5))GOTO888                   
      X2=XH+(I2-1)*HX1-L0(J1)*0.5                                       
      Z2=-1/BETA(J,I)+SQRT(1/BETA(J,I)**2/SIN(AL0)**2-(XP-X2-1/BETA(    
     *J,I)/TAN(AL0))**2)                                                
      DL2=XP-X2                                                         
  888 SH2=SH1                                                           
      SB2=SB1                                                           
      SH1=SQRT((L0(J)-2*DL1)**2+5.333*(Z(J,I)-Z1)**2)                   
      SB1=SQRT((L0(J)-2*DL2)**2+5.333*(Z(J,I)-Z2)**2)                   
      DSH=0.5*(SH2-SH1)                                                 
      DSB=0.5*(SB2-SB1)                                                 
      IF(J1-1) 999,999,991                                              
  999 IF(I1.LT.NH(1))I1=NH(1)                                           
      IF(I2.GT.NK(1))I2=NK(1)                                           
      Q1=V(1,I1)-V0P(J,I)*(1+BETA(J,I)*Z(1,I1)*0.5)                     
      Q1=-Q1/(V0P(J,I)*(1+BETA(J,I)*Z(1,I1)*0.5))/V(1,I1)               
      Q2=V (1,I2)-V0P(J,I)*(1+BETA(J,I)*Z(1,I2)*0.5)                    
      Q2=-Q2/(V0P(J,I)*(1+BETA(J,I)*Z(1,I2)*0.5))/V(1,I2)               
      GOTO 992                                                          
  991 IF(I1.LT.NH(J1))I1=NH(J1)                                         
      IF(I2.GT.NK(J1))I2=NK(J1)                                         
      Q1=V(J1,I1)-V0P(J,I)*(1+BETA(J,I)*(Z(J1-1,I1)+Z(J1,I1))*0.5)      
      Q1=-Q1/(V0P(J,I)*(1+BETA(J,I)*(Z(J1-1,I1)+Z(J1,I1))*0.5))/        
     *V(J1,I1)                                                          
      Q2=V(J1,I2)-V0P(J,I)*(1+BETA(J,I)*(Z(J1-1,I2)+Z(J1,I2))*0.5)      
      Q2=-Q2/(V0P(J,I)*(1+BETA(J,I)*(Z(J1-1,I2)+Z(J1,I2))*0.5))/        
     *V(J1,I2)                                                          
  992 A0=A0+Q1*DSH+Q2*DSB                                               
      DL01=DL1                                                          
      DL02=DL2                                                          
      IF(VD1)  I1=I01                                                   
      IF(VD2)  I2=I02                                                   
      Z01=Z1                                                            
      Z02=Z2                                                            
      J0=J1                                                             
      I01=I1                                                            
   12 I02=I2                                                            
C *** KOHEЦ J1-ЦИKЛA ***                                                
      DS=(SH1+SB1)*0.5                                                  
      Q(J,I)=(Q(J,I)-A0)/DS                                             
  300 V(J,I)=V0(J,I)-V0(J,I)**2*Q(J,I)/(1+V0(J,I)*Q(J,I))               
C *** KOHEЦ ЦИKЛA OПPEДEЛEHИЯ KCИ-AHOMAЛИЙ И CKOPOCTEЙ ***              
C                                                                       
C *** ЦИKЛ BЫЧИCЛEHИЯ ГЛУБИH ЦEHTPOB ЯЧEEK ***                          
      DO 400 J=1,G                                                      
      NHJ=NH(J)                                                         
      NKJ=NK(J)                                                         
      DO 400 I=NHJ,NKJ                                                  
      A0=Z(J,I)                                                         
      IF(J.NE.1)Z(J,I)=(ZR(I)+Z(J,I))*0.5                               
      IF(J.EQ.1)Z(J,I)=0.5*Z(J,I)                                       
      ZR(I)=A0                                                          
  400 CONTINUE                                                          
C                                                                       
C *** ЦИKЛ ПEЧATИ X,Z,V  И  PACЧETA MACCИBA WR(NR) ***                  
      WRITE(6,80)                                                       
   80 FORMAT(/2X,'ДИCKPETHЫЙ CKOPOCTHOЙ PAЗPEЗ X,Z,V(X,Z)'/)            
      MIX=XH                                                            
      NN=NK(1)                                                          
      IC=1                                                              
      DO 13 J=1,G                                                       
      DO 18 I=1,NN                                                      
      WR(IC)=HX1*(I-1)+XH                                               
      WR(IC+1)=Z(J,I)                                                   
      WR(IC+2)=V(J,I)                                                   
      IF(Z(J,I).LT.1.E4)GOTO18                                          
      WR(IC+1)=0.0                                                      
      WR(IC+2)=0.0                                                      
   18 IC=IC+3                                                           
   13 CONTINUE                                                          
      DO 20 J=1,G                                                       
      IR1=(J-1)*NN*3+1                                                  
      IR2=J*NN*3                                                        
      WRITE(6,21) (WR(I),I=IR1,IR2)                                     
   20 CONTINUE                                                          
   21 FORMAT(9F9.3)                                                    
      NR=G*NN*3                                                         
C                                                                       
C     *** OПPEДEЛEHИE ГPAHИЦЫ OБЛACTИ V(X,Z) ПO ГЛУБИHE ***             
      DO 40 I=1,NN                                                      
      ZMAX(I)=0.0                                                       
      DO 40 J=2,G                                                       
      IF(Z(J,I).LT.1.E+4) ZMAX(I)=Z(J,I)                                
      IF(Z(J,I).GE.1.E+4.AND.Z(J-1,I).LT.1.E+4) ZMAX(I)=Z(J-1,I)        
   40 CONTINUE                                                          
C                                                                       
C                                                                       
      RETURN                                                            
      END                                                               
C --------------------------------------------------------------------- 
C                       SYMPLEX                                         
C     ==========================================================        
C     =     ПOДПPOГPAMMA 2-X-MEPHOЙ ЛИHEЙHOЙ ИHTEPПOЛЯЦИИ      =        
C     =            CИMПЛEKC METOДOM                            =        
C     ==========================================================        
C                                                                       
C                     BXOДHЫE ДAHHЫE                                    
C     *************************************************************     
C     * XH - REAL, ЛEBAЯ X-ГPAHИЦA OБЛACTИ                        *     
C     * ZH - REAL, BEPXHЯЯ Z-ГPAHИЦA OБЛACTИ                      *     
C     * HX1 -REAL, ШAГ ЗAДAHИЯ ИCXOДHOГO PAЗPEЗA ПO OCИ X         *     
C     * NN - INTEGER, KOЛИЧECTBO TOЧEK ПO OCИ X                   *     
C     * G - INTEGER, KOЛИЧECTBO OTCЧETOB Z И V ПO BEPTИKAЛИ       *     
C     * Z(G,NN) - REAL, MACCИB ГЛУБИH ИCXOДHOГO PAЗPEЗA V(X,Z)    *     
C     * V(G,NN) - REAL, MACCИB CKOPOCTEЙ ИCXOДHOГO PAЗPEЗA        *     
C     * ZMAX(NN) - REAL, MACCИB ГЛУБИH HИЖHEЙ ГPAHИЦЫ OБЛACTИ     *     
C     * IZMAX(NN)- INTEGER, MACCИB ИHДEKCOB ГЛУБИH HИЖHEЙ ГPAHИЦЫ *     
C     * XS,ZS - REAL, X,Z-KOOPДИHATЫ TEKУЩEЙ TOЧKИ ИHTEPПOЛЯЦИИ   *     
C     *************************************************************     
C                                                                       
C                BЫXOДHЫE ДAHHЫE                                        
C     *************************************************************     
C     * XS,ZS - REAL, X-Z KOPДИHATЫ TEKУЩEЙ TOЧKИ ИHTEPПOЛЯЦИИ    *     
C     * VS -REAL, ИHTEPПOЛЯЦИOHHOE ЗHAЧEHИE CKOPOCTИ B ЭTOЙ TOЧKE *     
C     *************************************************************     
C                                                                       
      SUBROUTINE SYMPL(NN,G,V,Z,XH,ZH,HX1,ZMAX,IZMAX,XS,ZS,VS)          
      DIMENSION V(100,200),Z(100,200),ZMAX(150),IZMAX(150)              
      REAL XH,ZH,HX1,XS,ZS,VS                                           
      INTEGER G,NN,IZMAX                                                
C                                                                       
C     *** OПPEДEЛEHИE X-ПOЛOЖEHИE TOЧKИ ИHTEPПOЛЯЦИИ ***                
      NN1=NN-1                                                          
      DO 1 I=1,NN1                                                      
      IF(XS.GE.XH+HX1*(I-1).AND.XS.LT.XH+HX1*I) IX=I                    
    1 CONTINUE                                                          
      I=IX                                                              
C     *** OПPEДEЛEHИE Z-ПOЛOЖEHИЯ TOЧKИ ИHTEPПOЛЯЦИИ ***                
C     *** OTHOCИTEЛЬHO HИЖHEЙ ГPAHИЦЫ OБЛACTИ ZMAX   ***                
      ZMD=ZMAX(I)+(ZMAX(I+1)-ZMAX(I))/HX1*(XS-XH-(I-1)*HX1)             
C     *** ECЛИ TOЧKA HAXOДИTCЯ BHE OБЛACTИ,          ***                
C     *** ИHTEPПOЛЯЦИЯ HE ПPOИЗBOДИTCЯ               ***                
      IF(ZS.GE.ZMD) GOTO 9                                              
C     *** УЧET BOЗMOЖHOCTИ HAXOЖДEHИЯ TOЧKИ BЫШE BEPXHEЙ  ***           
C     *** ГPAHИЦЫ OБЛACTИ ЗAДAHИЯ CKOPOCTИ                ***           
      ZXB=Z(1,I)+(Z(1,I+1)-Z(1,I))/HX1*(XS-XH-(I-1)*HX1)                
      IF(ZS.LT.ZXB) VS=V(1,I)+(V(1,I+1)-V(1,I))/HX1*                    
     *                      (XS-XH-(I-1)*HX1)                           
C     *** ПOCKOЛЬKУ CKOPOCTЬ OПPEДEЛEHA УXOДИM HA KOHEЦ  ***            
      IF(ZS.LT.ZXB) GOTO 9                                              
C                                                                       
      JLU=1                                                             
      JRU=1                                                             
      IL=0                                                              
      IR=0                                                              
C     ***  ИHДEKCЫ ДЛЯ PAБOTЫ HA БOKOBЫX ГPAHИЦAX OБЛACTИ ***           
      KLU=IZMAX(I)                                                      
      KRD=IZMAX(I+1)                                                    
C                                                                       
C     *** OПPEДEЛEHИE Z-ПOЛOЖEHИЯ TOЧKИ BHУTPИ OБЛACTИ ***              
C     ### OKPECTHOCTЬ BEPXHEЙ ГPAHИЦЫ ###                               
      IF(ZS.LT.Z(1,I).AND.ZS.GE.Z(1,I+1)) GOTO 11                       
      GOTO 12                                                           
   11 JLU=1                                                             
      IL=1                                                              
   12 IF(ZS.GE.Z(1,I).AND.ZS.LT.Z(1,I+1)) GOTO 13                       
      GOTO 14                                                           
   13 JRD=1                                                             
      IR=1                                                              
   14 DO 5 J=2,G                                                        
C                                                                       
      IF(ZS.GE.Z(J-1,I).AND.ZS.LT.Z(J,I)) GOTO 2                        
      GOTO 3                                                            
C                                                                       
    2 JLU=J-1                                                           
      JLD=J                                                             
      IL=1                                                              
C                                                                       
    3 IF(ZS.GE.Z(J-1,I+1).AND.ZS.LT.Z(J,I+1)) GOTO 4                    
      GOTO 5                                                            
C                                                                       
    4 JRU=J-1                                                           
      JRD=J                                                             
      IR=1                                                              
C                                                                       
    5 CONTINUE                                                          
C                                                                       
      IF(IL.EQ.0) JLU=KLU                                               
      IF(IR.EQ.0) JRD=KRD                                               
C                                                                       
C     *** ЗAДAHИE KOOPДИHAT TOЧEK CИMПЛEKCA ***                         
C                                                                       
      X1=XH+HX1*(I-1)                                                   
      Z1=Z(JLU,I)                                                       
      V1=V(JLU,I)                                                       
C     X2,Z2,V2 - OПPEДEЛЯЮTCЯ B ЗABИCИMOCTИ OT ПOЛOЖEHИЯ TOЧKИ          
      X3=XH+HX1*I                                                       
      Z3=Z(JRD,I+1)                                                     
      V3=V(JRD,I+1)                                                     
C                                                                       
C     VS= HУЖHO OПPEДEЛИTЬ                                              
C                                                                       
C     *** OПPEДEЛEHИE ПOЛOЖEHИЯ TOЧKИ OTHOCИTEЛЬHO ДИAГOHAЛИ 4-УГ-KA ** 
      ZX=Z(JLU,I)+(Z(JRD,I+1)-Z(JLU,I))*(XS-XH-(I-1)*HX1)/HX1           
C                                                                       
      IF(ZS.LT.ZX) GOTO 6                                               
      GOTO 7                                                            
C                                                                       
    6 X2=XH+HX1*I                                                       
      Z2=Z(JRU,I+1)                                                     
      V2=V(JRU,I+1)                                                     
      GOTO 8                                                            
C                                                                       
C     *** ИHAЧE T.E. ПPИ ZS.GE.ZX                                       
    7 IF(ZS.LT.ZMD.AND.IL.EQ.0) GOTO 6                                  
C     (*** ЭKCTPAПOЛЯЦИЯ HИЖE TPEУГOЛЬHИKA ***)                         
C                                                                       
      X2=XH+HX1*(I-1)                                                   
      Z2=Z(JLD,I)                                                       
      V2=V(JLD,I)                                                       
C                                                                       
    8 VS=                                                               
     *(-(V1*(Z2-Z3)-Z1*(V2-V3)+(V2*Z3-V3*Z2))*XS                        
     * -(X1*(V2-V3)-V1*(X2-X3)+(X2*V3-X3*V2))*ZS                        
     * +(X1*(V2*Z3-V3*Z2)-V1*(X2*Z3-X3*Z2)+Z1*(X2*V3-X3*V2)))/          
     *  (Z1*(X2-X3)-X1*(Z2-Z3)+(Z2*X3-Z3*X2))                           
C                                                                       
    9 CONTINUE                                                          
C                                                                       
      RETURN                                                            
      END                                                               
C     ----------------------------------------------------------------- 
      SUBROUTINE FILTR(M,N,MN,VS,HX,HZ,IZMAX,DX1,DZ1,DX2,DZ2,ZF2,VF)    
C     ================================================================  
C     =  ПOДПPOГPAMMA ФИЛЬTPAЦИИ ПOCPEДCTBOM OCPEДHEHИЯ B OKHE DX,DZ =  
C     =                      15.03.88.                               =  
C     ================================================================  
C                     ПAPAMETPЫ BXOДA                                   
C     ****************************************************************  
C     * VS(MN) - REAL, MACCИB ИCXOДHЫX ЗHAЧEHИЙ CKOPOCTИ, CФOPMИPO-  *  
C     *          BAHHЫЙ B OДHOMEPHOM BИДE (ПOCTPOЧHO)                *  
C     * N - INTEGER, ДЛИHA CTPOKИ COOTBETCTBУЮЩEГO 2-MEPHOГO MACCИBA *  
C     * M - INTEGER, KOЛИЧECTBO CTPOK  -/-/-/-/-       VS(M,N)       *  
C     * MN = M*N, INTEGER                                            *  
C     * HX - REAL, ШAГ ДИCKPETИЗAЦИИ ЗHAЧEHИЙ CKOPOCTИ ПO OCИ X      *  
C     * HZ - REAL, ШAГ ДИCKPETИЗAЦИИ ЗHAЧEHИЙ CKOPOCTИ ПO OCИ Z      *  
C     * IZMAX(N) - INTEGER, MACCИB ИHДEKCOB ГЛУБИH HИЖHEЙ ГPAHИЦЫ    *  
C     * DX1 \                      OБЛACTИ ЗAДAHИЯ CKOPOCTИ          *  
C     * DZ1 /                                                        *  
C     * DX2 \      PAЗMEPЫ OKOH OCPEДHЯЮЩEГO CГЛAЖИBAHИЯ             *  
C     * DZ2 /                                                        *  
C     * ZF2 - УPOBEHЬ, PAЗДEЛЯЮЩИЙ OБЛACTИ PAЗЛИЧHOГO CГЛAЖИBAHИЯ    *  
C     ****************************************************************  
C                     ПAPAMETPЫ BЫXOДA                                  
C     ****************************************************************  
C     * VF(MN) - REAL, MACCИB CГЛAЖEHHЫX ЗHAЧEHИЙ CKOPOCTИ,          *  
C     *          CФOPMИPOBAHHЫЙ B OДHOMEPHOM BИДE (ПOCTPOЧHO)        *  
C     * MN -                                                         *  
C     * M  -                                                         *  
C     * N  -          CM. BЫШE                                       *  
C     * HX,HZ -                                                      *  
C     * IZMAX(N) -                                                   *  
C     ****************************************************************  
C                                                                       
C                                                                       
      DIMENSION VS(MN),VF(MN),IZMAX(N)                                  
      REAL HX,HZ,DX1,DZ1,DX2,DZ2,ZF2                                    
C     INTEGER M,N                                                       
C                                                                       
C     *** ЧИCTKA MACCИBA VF(MN) ***                                     
      DO 1 I=1,MN                                                       
      VF(I)=0.0                                                         
    1 CONTINUE                                                          
C                                                                       
C     *** ЗAДAHИE OKHA ФИЛЬTPAЦИИ ***                                   
      NF1=INT(DX1/HX+1.E-6)+1                                           
      MF1=INT(DZ1/HZ+1.E-6)+1                                           
      NF2=INT(DX2/HX+1.E-6)+1                                           
      MF2=INT(DZ2/HZ+1.E-6)+1                                           
      JF2=INT(ZF2/HZ+1.E-6)+1                                           
      NF=NF1                                                            
      MF=MF1                                                            
C                                                                       
C     *** OПPEДEЛEHИE HAЧAЛЬHЫX IH,JH И KOHEЧHЫX IK,JK ИHДEKCOB ***     
C     *** OБЛACTИ PEЗУЛЬTATA ФИЛЬTPAЦИИ (ПOKA HE ИCПOЛЬЗУЮTCЯ)  ***     
C     IH=NF/2                                                           
C     IK=N-NF/2                                                         
C     JH=MF/2                                                           
C     JK=M-MF/2                                                         
C                                                                       
C     *** ИX ЗAMEHA ***                                                 
      IH=1                                                              
      IK=N                                                              
      JH=1                                                              
      JK=M                                                              
C                                                                       
C     *** ЦEHTPAЛЬHЫЙ БЛOK ФИЛЬTPA ***                                  
C                                                                       
      DO 4 J=JH,JK                                                      
      DO 4 I=IH,IK                                                      
C                                                                       
C     *** ИЗБEГAEM ФИЛЬTPAЦИИ ЗA ГPAHИЦEЙ OБЛACTИ ***                   
      IF(J.GT.IZMAX(I)) GOTO 4                                          
C                                                                       
C     *** УBEЛИЧEHИE OKHA OCPEДHEHИЯ ПPИ J > JF2  ***                   
      IF(J.LE.JF2) GOTO 2                                               
      NF=NF2                                                            
      MF=MF2                                                            
C                                                                       
C     *** CУMMИPOBAHИE B OKHE ***                                       
    2 S=0.0                                                             
      ISCH=0                                                            
      DO 3 JJ=1,MF                                                      
      DO 3 II=1,NF                                                      
C                                                                       
C     *** ПEPECЧET ИHДEKCOB OKHA B ИHДEKCЫ OБЛACTИ ***                  
      J1=J-MF/2+JJ-1                                                    
      I1=I-NF/2+II-1                                                    
C                                                                       
      JI=(J1-1)*N+I1                                                    
C                                                                       
C     *** KOHTPOЛЬ BЫXOДA ЗA ГPAHИЦУ OБЛACTИ ***                        
      IF(J1.LT.1.OR.J1.GT.M) GOTO 3                                     
      IF(I1.LT.1.OR.I1.GT.N) GOTO 3                                     
C                                                                       
C     *** HУЛEBЫE ЭЛEMEHTЫ OKHA HE УЧИTЫBAЮTCЯ ***                      
      IF(VS(JI).EQ.0.0) GOTO 3                                          
      S=S+VS(JI)                                                        
      ISCH=ISCH+1                                                       
    3 CONTINUE                                                          
C                                                                       
      IF(ISCH.NE.0) VF((J-1)*N+I)=S/ISCH                                
C                                                                       
    4 CONTINUE                                                          
C                                                                       
      RETURN                                                            
      END                                                               
C     --------------------------------------------------------------    
C              PERICL(M,N,XH,ZH,HX,HZ,MINV,MAXV,
C                           G,NN,V,Z,HX1,ZMAX,IZMAX)
C                                                                       
C     ==============================================================    
C     =  ПOДПPOГPAMMA ФOPMИPOBAHИЯ ГРАФИЧЕСКОГО MACCИBA            =    
C     =  СТРОКАМИ ПO 640 ЗНАЧЕНИЙ (1280 БAЙT)                      =    
C     =  И ЗАПИСЬ ЭТОГО МАССИВА HA ДИСК В УКАЗАННЫЙ ФАЙЛ           =    
C     ==============================================================    
C                                                                       
C     П/П OБPAЩAETCЯ K SYMPL(NN,G,V,Z,MIX,MIZ,HX1,ZMAX,IZMAX,XS,ZS,VS)  
C     ПPEДПOЛAГAETCЯ, ЧTO ПAPAMETPЫ SYMPL OПИCAHЫ И OПPEДEЛEHЫ B        
C     ГOЛOBHOЙ ПPOГPAMME                                                
C                                                                       
C              BXOДHЫE ПAPAMETPЫ                                        
C     ***************************************************************** 
C     * M - INTEGER, ЧИCЛO CTPOK MACCИBA                                
C     * N - INTEGER, ЧИCЛO OПPEДEЛEHHЫX CTOЛБЦOB MACCИBA                
C     * XH - REAL, HAЧAЛO OTCЧETOB ПO OCИ X                             
C     * ZH - REAL, HAЧAЛO OTCЧETOB ПO OCИ Z                             
C     * HX - REAL, ШAГ ДИCKPETИЗAЦИИ ПO OCИ X                           
C     * HZ - REAL, ШAГ ДИCKPETИЗAЦИИ ПO OCИ Z                           
C     ***************************************************************** 
C                                                                       
C                 BЫXOДHЫE ПAPAMETPЫ                                    
C     ***************************************************************** 
C     * IVGRAF(640) - INTEGER*2  СТPOKИ MACCИBA, ПОСЛЕДОВАТЕЛЬНО
C     *              ЗAПИCЫBAEMЫE HA ДИСК, БЛОКАМИ ПО 1280 БАЙТ         
C     * МINV                           е                                 
C     * MAXV                                                            
C     ***************************************************************** 
C                                                                       
      SUBROUTINE PERICL (M,N,XH,ZH,HX,HZ,MINV,MAXV,            
     *                   G,NN,V,Z,HX1,ZMAX,IZMAX)                       
C                                                                       
      DIMENSION V(100,200),Z(100,200),ZMAX(NN),IZMAX(NN)                
      INTEGER M,N,G,IZMAX                                               
      REAL XH,ZH,HX,HZ                                                  
      INTEGER*2 IVGRAF(640)                                              
      INTEGER MINV,MAXV                                                 
C
      NN2=NN/2                                                          
      MINV=INT(V(1,NN2)*100+1.E-5)                                                     
      MAXV=MINV                                                         
C                                                                       
C     *** OCHOBHOЙ J-ЦИKЛ ПEPEБOPA CTPOK CETKИ ***                      
C                                                                       
      DO 30 J=1,M                                                       
      DO 10 I=1,640                                                     
   10 IVGRAF(I)=0                                                      
C                                                                       
      ZS=ZH+(J-1)*HZ                                                    
C     *** I-ЦИKЛ PACЧETA ЗHAЧEHИЙ B CTPOKE ***                          
      DO 20 I=1,N                                                       
      XS=XH+(I-1)*HX                                                    
      VS=0.0                                                            
C                                                                       
      CALL SYMPL(NN,G,V,Z,XH,ZH,HX1,ZMAX,IZMAX,XS,ZS,VS)                
C     *** ФOPMИPOBAHИE ТЕКУЩЕЙ ТОЧКИ MACCИBA  ***                       
C                                                                       
      IVGRAF(I)=INT(VS*100+1.E-5)                                                       
C                                                                       
C     *** OПPEДEЛEHИE MIN-ГO И MAX-ГO ЗHAЧEHИЙ MACCИBA  ***             
      IF(IVGRAF(I).NE.0.AND.IVGRAF(I).LT.MINV) MINV=IVGRAF(I)            
      IF(IVGRAF(I).GT.MAXV) MAXV=IVGRAF(I)                                
C                                                                       
   20 CONTINUE                                                          
C     *** KOHEЦ I-ЦИKЛA ФOPMИPOBAHИЯ CTPOKИ ***                         
C
C     ================================================================  
C       
C     *** ПOCTPOЧHAЯ  ЗAПИCЬ HA ЛEHTУ MACCИBA VGRAF(640) - M CTPOK ***  
C
      WRITE(10) (IVGRAF(I),I=1,N)
C
C     ================================================================= 
   30 CONTINUE                                                          
C
C++   WRITE(6,31) MINV,MAXV                                             
C++31 FORMAT(/2X,'MINV = ',I5,5X,'MAXV = ',I5)                      
C                                                                       
      RETURN                                                            
      END                                                               
