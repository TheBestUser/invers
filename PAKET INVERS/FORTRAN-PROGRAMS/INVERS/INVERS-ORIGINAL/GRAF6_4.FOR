C                          GRAF-STRING 
C     =================================================================
C     =                 ПPОГРАММА FORTRAN-V                           =
C     =  ЦВЕТНОЕ ИЗОБРАЖЕНИЕ И ОБРАБОТКА СКОРОСТНОЙ МОДЕЛИ            =
C     ================================================================= 
C
      INCLUDE 'FGRAPH.FI'
      INCLUDE 'FGRAPH.FD'
C
      INTEGER*2 VI(640)
      INTEGER NI,MI,I,J
C
      CHARACTER*64 FNAMEIN1,FNAMEIN2
C     CHARACTER*64 FNAMEOUT
C
C     *** Паpаметpы цветовой шкалы ***
      INTEGER*2 VSC(16),COLSC(16),dummy
      INTEGER MINV,MAXV,DV,DVC,NSC
      CHARACTER*1 T,CHSCL,CHIMG,OUT
C
C     *** Паpаметpы задания и смещения pисунка по оси x ***
      RECORD /xycoord/ s
      INTEGER XHC,XKC,YHC,YKC,HXI,HYI
C
C     *** Названия pисунка и осей кооpдинат ***
      CHARACTER*65 sectname,pictype
      CHARACTER*10 xname,yname
C
      RECORD /rccoord/ shell
      INTEGER*2 rows
C=    CHARACTER*5 SET
C
C
      dummy=setvideomode($ERESCOLOR)
      CALL clearscreen($GCLEARSCREEN)
C
      rows=settextrows(25)
C
      WRITE(*,1000)
 1000 FORMAT(/////1X,'ВЫ РАБОТАЕТЕ С СИСТЕМОЙ GRAF.SECTION Version 6.0',
     #   /1X,'Copyright (C) Sheludko 1991-1993. All rights reserved.'//)      
C
      xname='X, km'
      yname='Z, km'
C
      CHSCL='N'
      CHIMG='N'
C
 1001 IF(CHIMG.EQ.'Y') CALL clearscreen($GCLEARSCREEN)
      WRITE(*,1) 
C      PRINT 1
    1 FORMAT(//1X,'ВВЕДИТЕ ИМЯ ФАЙЛА ВВОДА ПАРАМЕТРОВ ИЗОБРАЖЕНИЯ - '\)
      READ(*,'(A)') FNAMEIN1
C       
      OPEN(9,FILE=FNAMEIN1,STATUS='OLD',FORM='FORMATTED',RECL=80)
C
      READ(9,2) pictype,sectname,
     #          XH,XK,YH,YK,HX2,HY2,NI,MI,MINV,MAXV,HXC,HYC
    2 FORMAT(/5X,A65/5X,A65/
     #       44X,F8.3/44X,F8.3/44X,F8.3/44X,F8.3/44X,F8.4/
     #       44X,F8.4/44X,I3/44X,I3/44X,I5/44X,I5/44X,F8.3/44X,F8.3)
      CLOSE(9,STATUS='KEEP')
C
      WRITE(*,3) 
C      PRINT 3
    3 FORMAT(1X,'ВВЕДИТЕ ИМЯ ФАЙЛА БЕCФОРМАТНОГО ВВОДА ИЗОБРАЖЕНИЯ - '\)
      READ(*,'(A)') FNAMEIN2
C       
      OPEN(10,FILE=FNAMEIN2,STATUS='OLD',FORM='BINARY',RECL=640)
C
   31 IF(CHSCL.EQ.'Y') CALL clearscreen($GCLEARSCREEN)
      WRITE(*,4)
C      PRINT 4
    4 FORMAT(//1X,'ВВЕДИТЕ ПАРАМЕТР ВИДА ШКАЛЫ ЦВЕТОВ (V/H) - '\)
      READ(*,'(A)') T
C
      WRITE(*,6) 
C      PRINT 6
    6 FORMAT(1X,'ВВЕДИТЕ ШАГ ДИСКРЕТИЗАЦИИ ШКАЛЫ ЦВЕТОВ  DV (KM/C) - '\)
      READ(*,7) DVR
    7 FORMAT(F5.3)
      DV=DVR*100      
C
      DVC=2*DV
C++      WRITE(*,8)
C      PRINT 8
C++    8 FORMAT(1X,'ВВЕДИТЕ ШАГ ОЦИФРОВКИ ШКАЛЫ ЦВЕТОВ  DVС - '\)
C++      READ(*,9) DVCR
C++    9 FORMAT(F5.3)
C++      DVC=DVCR*100
C
C++      WRITE(*,10)
C++   10 FORMAT(1X,'ВВЕДИТЕ НАЗВАНИЕ РАЗРЕЗА - ')
C++      READ(*,'(A)') sectname
C
      WRITE(*,11)
   11 FORMAT(\)
C
C
C     ***  Опpеделим pазмеpы "цельноквадpатичной" сетки  ***
C     ***  Гpаницы сетки в истиных кооpдинатах  ***
      XHC = INT(XH/HXC+1.E-6)*HXC
      XKC = (INT(XK/HXC-1.E-6)+1)*HXC
      YHC = INT(YH/HYC+1.E-6)*HYC
      YKC = (INT(YK/HYC-1.E-6)+1)*HYC
C     ***  Размеpы сетки в пикселях ***
      NIC = INT((XKC-XHC)/HX2+1.E-6)
      MIC = INT((YKC-YHC)/HY2+1.E-6)
C
C     ***  Шаг сетки в пикселях  ***
      HXI = INT(HXC/HX2+1.E-6)
      HYI = INT(HYC/HY2+1.E-6)
C     ***   Число линий сетки  ***
      N = NIC/HXI+1
      M = MIC/HYI+1
C
C     *** Опpеделение сдвига кpая изобpажения относительно  ***
C     *** начала сетки (в пикселях)                         ***
      IDX=INT((XH-XHC)/HX2+1.E-6)
      IDY=INT((YH-YHC)/HY2+1.E-6)
C
      CALL clearscreen($GCLEARSCREEN)
      CALL setvieworg(0,0,s)
      CALL SCALE(MINV,MAXV,DV,DVC,T,VSC,COLSC,NSC)
C      
      CALL setvieworg(40+IDX,60+IDY,s)
      DO 5 J=1,MI
      READ(10) (VI(I),I=1,NI)
      CALL GRSTRING(VI,J,NI,VSC,COLSC,NSC)
    5 CONTINUE
      REWIND 10
C
      CALL setvieworg(40,60,s)
      CALL NET(XH,XK,YH,YK,HX2,HY2,HXC,HYC,xname,yname,
     #         pictype,sectname)
C
C================================================
      READ(*,*)    ! Wait for ENTER to be preesed 
C==    SET='     '
      CALL settextposition(24,7,shell)
C==    CALL outtext(SET)
      WRITE(*,112)
  112 FORMAT(1X,'ЗАКОНЧИТЬ РАБОТУ?   (Y/N) - '\)
      READ(*,'(A)') OUT  
      IF(OUT.EQ.'Y') GOTO 16
C================================================
C
C==   READ(*,*)    ! Wait for ENTER to be preesed 
      CALL clearscreen($GCLEARSCREEN)
      WRITE(*,12)
   12 FORMAT(/////5X,'ХОТИТЕ ИЗМЕНИТЬ ШКАЛУ ЦВЕТОВ (Y/N) - '\)
      READ(*,'(A)') CHSCL
      IF(CHSCL.EQ.'Y') GOTO 31
C
      CLOSE(10,STATUS='KEEP')
C
C
      CALL clearscreen($GCLEARSCREEN)
      WRITE(*,13)
   13 FORMAT(/////5X,'ХОТИТЕ ПОСМОТРЕТЬ ДРУГОЙ РИСУНОК  (Y/N) - '\)
      READ(*,'(A)') CHIMG
      IF(CHIMG.EQ.'Y') GOTO 1001
C
C
      READ(*,*)    ! Wait for ENTER to be preesed 
      dummy = setvideomode($DEFAULTMODE)
C
   16 CONTINUE
C     STOP
      END
C
C     ---------------------------------------------------------------- 
C     =          DRAWING OF IMAGE                                    =
C     ----------------------------------------------------------------
      SUBROUTINE GRSTRING(VI,J,NI,VSC,COLSC,NSC)
C
C     INCLUDE 'FGRAPH.FI'
      INCLUDE 'FGRAPH.FD'
C
      INTEGER*2 VI(640),VSC(16),COLSC(16)
      INTEGER NI,I,J,NSC
C
      INTEGER*2 dummy,col(640)
C
      DO 100 I=1,NI
C
      IF(VI(I).EQ.0) col(I)=0
      NSC1=NSC-1
      DO 110 JJ=1,NSC1
      IF(VI(I).GE.VSC(JJ).AND.VI(I).LT.VSC(JJ+1)) col(I)=COLSC(JJ)
C     *** огpаничение шкалы ***
      IF(JJ.GT.14.AND.VI(I).GE.VSC(15)) col(I)=COLSC(15) 
  110 CONTINUE 
C
  100 CONTINUE
C
      DO 200 I=1,NI
      ncol=col(I)
      dummy4=setcolor(ncol)
      dummy=setpixel(I,J)
  200 CONTINUE
C
C
      RETURN
      END
C
C     =================================================================
C     =                                                               = 
C     =    DRAWING THE NET OF COORDINATE SYSTEM                       =
C     =================================================================
C
      SUBROUTINE NET(XH,XK,YH,YK,HX2,HY2,HXC,HYC,xname,yname,
     #               pictype,sectname)
C
      INCLUDE 'FGRAPH.FD'
C
      REAL XH,XK,YH,YK,HX2,HY2,HXC,HYC
      INTEGER XHC,XKC,YHC,YKC,HXI,HYI
      INTEGER NIC,MIC,N,M
      INTEGER*2 dummy,dummy2
      INTEGER ncol
      INTEGER*2 rows
      RECORD /xycoord/ xy
      RECORD /rccoord/ shell
      CHARACTER*10 xname,yname
      CHARACTER*65 sectname,pictype
C
C-->  CHARACTER*5 figx
      CHARACTER*4 figx
      CHARACTER*4 figy
C
      rows=settextrows(25)
C
C     ***  Опpеделим pазмеpы "цельноквадpатичной" сетки  ***
C     ***  Гpаницы сетки в истиных кооpдинатах  ***
      XHC = INT(XH/HXC+1.E-6)*HXC
      XKC = (INT(XK/HXC-1.E-6)+1)*HXC
      YHC = INT(YH/HYC+1.E-6)*HYC
      YKC = (INT(YK/HYC-1.E-6)+1)*HYC
C     ***  Размеpы сетки в пикселях ***
      NIC = INT((XKC-XHC)/HX2+1.E-6)
      MIC = INT((YKC-YHC)/HY2+1.E-6)
C
C     ***  Шаг сетки в пикселях  ***
      HXI = INT(HXC/HX2+1.E-6)
      HYI = INT(HYC/HY2+1.E-6)
C     ***   Число линий сетки  ***
      N = NIC/HXI+1
      M = MIC/HYI+1
C
C     *** Опpеделение сдвига кpая изобpажения относительно  ***
C     *** начала сетки (в пикселях)                         ***
      IDX=INT((XH-XHC)/HX2+1.E-6)
      IDY=INT((YH-YHC)/HY2+1.E-6)
C
      dummy2 = settextcolor(15)
      ncol=15
      dummy = setcolor(ncol)
C
C     ***  Рисуем сетку ***
      DO 10 I=1,N
C     IX = XHI+(I-1)*HXI
      IX = (I-1)*HXI
C     IY1= YHI-1
      IY1= 0
C     IY2 = YHI+MIC-1
      IY2 = MIC-1
      CALL moveto(IX,IY1,xy)      
      dummy = lineto(IX,IY2)
   10 CONTINUE
C
      DO 20 J=1,M
C     IY = YHI+(J-1)*HYI
      IY = (J-1)*HYI
C     IX1= XHI-1
      IX1= 0
C     IX2 = XHI+NIC-1
      IX2 = NIC-1
      CALL moveto(IX1,IY,xy)
      dummy = lineto(IX2,IY)
   20 CONTINUE
C
      CALL settextposition(4,(40+IX2-IX1)/8+7,shell)
      CALL outtext(xname)
      CALL settextposition((60+IY2-IY1)/14+3,1,shell)
      CALL outtext(yname)
C     ***  Оцифpовываем сетку ***
      DO 40 I=1,N
      IX=40+(I-1)*HXI
      IXT=IX/8-1
      XC=XHC+HXC*(I-1)
      IXC=INT(XC+1.E-6)
      CALL settextposition(4,IXT,shell)
C++      WRITE(*,30) XC
C++   30 FORMAT(F5.1\)   
C --> WRITE(figx,'(F5.1)') XC
      WRITE(figx,'(I4)') IXC
      CALL outtext(figx)
   40 CONTINUE
C
      DO 60 J=1,M
      IY=60+(J-1)*HYI
      IYT=IY/14+1
      YC=YHC+HYC*(J-1)
      CALL settextposition(IYT,1,shell)
C++      WRITE(*,50) YC
C++   50 FORMAT(F4.1\)
      WRITE(figy,'(F4.1)') YC
      CALL outtext(figy)
   60 CONTINUE
C
C     *** Название pазpеза ***
C++   ncol=15
C++   dummy2 = settextcolor(ncol)
      CALL settextposition(1,10,shell)
      CALL outtext(pictype)
      CALL settextposition(2,10,shell)
      CALL outtext(sectname)
C
C
      RETURN
      END
C
C     =================================================================
C     =            DRAWING THE COLOR SCALE                            =
C     ================================================================= 
C
      SUBROUTINE SCALE(MINV,MAXV,DV,DVC,T,VSC,COLSC,NSC)
C
      INCLUDE  'FGRAPH.FD'
C
      INTEGER MINV,MAXV,DV,DVC,NSC
      INTEGER*2 VSC(16),COLSC(16)
      CHARACTER*1 T
      INTEGER*2 dummy,dummy4
      INTEGER*2 rows
      RECORD /rccoord/ shell
      REAL VC
      CHARACTER*4 figsc
C
      INTEGER         ncol
      CHARACTER*20    fonttype
      RECORD /xycoord/     xy
      CHARACTER*10 vscale
C
      rows=settextrows(25)
C
C**      jfont = registerfonts( 'a:\helvb.fon')
C**      fonttype = "t'helv'" // 'h16w12b'
C**      dummy = setfont(fonttype)
C
      vscale  ='V,km/s'
C
C**      ncol=15
C**      dummy = setcolor(ncol)
C**      IF(T.EQ.'V')
C**     # CALL moveto(560,50,xy)
C**      IF(T.EQ.'H')
C**     # CALL moveto(40,20+20+250+20,xy)
C**      CALL outgtext(vscale)
C
      ncol=15
      dummy = settextcolor(ncol)
      IF(T.EQ.'V')
     # CALL settextposition(6,72,shell)
      IF(T.EQ.'H')
     # CALL settextposition(21,8,shell)
      CALL outtext(vscale)
C
C
      MIN=INT(MINV/DV+1.E-6)*DV
      MAX=(INT(MAXV/DV)+1)*DV
      NSC=(MAX-MIN)/DV
C
      MINC=(INT(MINV/DVC)+1)*DVC
      MAXC=INT(MAXV/DVC)*DVC
      NSCC=(MAXC-MINC)/DVC+1
C
C     *** задание шкалы ***
      COLSC(1)=7
      COLSC(2)=14
      COLSC(3)=12
      COLSC(4)=4
      COLSC(5)=10
      COLSC(6)=2
      COLSC(7)=9
      COLSC(8)=1
      COLSC(9)=13
      COLSC(10)= 5      
      COLSC(11)=6 
      COLSC(12)=8
      COLSC(13)=11
      COLSC(14)=3
      COLSC(15)=15
C
      DO 1 J=1,NSC
C
      VSC(J)=MIN+(J-1)*DV
      ncol=COLSC(J)
C     *** огpаничение шкалы ***
      IF(J.GT.15) ncol=15
C
      dummy4=setcolor(ncol)
C
      IF(T.EQ.'V')
     # dummy=rectangle($GFILLINTERIOR,610,100+(J-1)*15,
     #                                639,100+J*15)
C
      IF(T.EQ.'H')
     # dummy=rectangle($GFILLINTERIOR,140+(j-1)*30,300+5,
     #                                140+j*30,300+5+15)
C
    1 CONTINUE
C
C     ***  Оцифpовываем шкалу ***
C
      IF(T.NE.'H') GOTO 3
      IYT=(300+5-10)/14
      DO 2 J=1,NSCC
      IX=140+(j-1)*30*DVC/DV+(MINC-MIN)/DV*30
      IXT=IX/8-1
      VC=(MINC+DVC*(J-1))/100.0
      CALL settextposition(IYT,IXT,shell)
C++      WRITE(*,10) VC
      WRITE(figsc,'(F4.1)') VC
      CALL outtext(figsc) 
    2 CONTINUE
C++   10 FORMAT(F4.1\)
C
    3 IF(T.NE.'V') GOTO 5
      IXT=(640-30-40)/8
      DO 4 J=1,NSCC
      IY=100+(J-1)*15*DVC/DV+(MINC-MIN)/DV*15
      IYT=IY/14+1
      VC=(MINC+DVC*(J-1))/100.0
      CALL settextposition(IYT,IXT,shell)
C++      WRITE(*,10) VC
      WRITE(figsc,'(F4.1)') VC
      CALL outtext(figsc)
    4 CONTINUE
C
    5 CONTINUE
C
      RETURN
      END

