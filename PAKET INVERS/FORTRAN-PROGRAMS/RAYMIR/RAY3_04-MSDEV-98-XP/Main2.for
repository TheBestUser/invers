$LARGE
C			  
C		                 24.03.92.	  
C	==============================================================    
C	=	         �PO�PAMMA *RAYMIR*                             =    
C	= PE�EH�E �P�MO� K�HEMAT��ECKO� �A�A�� ��� �B�MEPHO-         =    
C	= HEO�HOPO�HO� �PA��EHTHO� CPE��, �O�CT��AEMO� OTPA�A��E�    =    
C	= �PAH��E�.PACC��T�BA�TC� BPEM� � TPAEKTOP�� PAC�POCTPAHEH�� =    
C	= PE�PA��POBAHHO� � OTPA�EHHO� BO�H.                         =    
C	= TPAEKTOP�� ���E� ��O�PA�A�TC� B �PA���ECKOM B��E.          =    
C	=	                                                        =    
C	==============================================================    
C			  
C	���K �PO�PAMM�POBAH��   FORTRAN-Microsoft Version 5.00
C	�C�O����ETC� �����OTEKA   GRAPHICS
C	OP�AH��A��� - ��� CO AH CCCP, �A�.59
C	COPYRIGHT (C), 1989, BY I.F.SHELUDKO, IGG SB USSR AS	  
C			  
C		              BXO�H�E �AHH�E	  
C	***************************************************************** 
C	*	              PAC�PE�E�EH�E CKOPOCT�                       * 
C	* ------------------------------------------------------------- * 
C	*	  MO�ET ��T� �A�AHO PA����H�M� C�OCO�AM�	               * 
C	*	  � ������ ������ ��������� (Version 2.0)                  * 
C	*    BBO��TC� �A�AHHOE HA �P�MO��O��HO� CETKE PAC�PE�E�EH�E     * 
C	*    CKOPOCT�, HE TPE����EE �O�O�H�TE��HO� O�PA�OTK� (�EPEC�ETA * 
C	*    HA �P���� CETK�, C��A��BAH��), �C�O����EMOE HE�OCPE�CTBEH- * 
C	*    HO ��� TPACC�POBAH�� ���E�	                               * 
C	*    VF2(M2,N2) - REAL, MACC�B �HA�EH�� CKOPOCT�, (KM/C)	       * 
C	*    N2,M2 - INTEGER, ��C�O ���OB CETK� �O OC� X � Z COOTBET-   * 
C	*	                CTBEHHO (N2<200, M2<100)	               * 
C	*	                                                           * 
C	*	## KOOP��HAT� ���OB PAC�PE�E�EH�� CKOPOCT� MO��T ��T�      * 
C	*	�A�AH� B C�E����EM B��E:	                               * 
C	*  A) XH,XK - REAL, �EBA� � �PABA� �PAH��� �PO���� �O OC� X,/KM/* 
C	*	MIZ    - REAL, �POBEH� �P�BE�EH�� �AHH�X �O OC� Z, /KM/	   * 
C	*	HX2,HZ2 - REAL, �A� CETK� �A�AH�� CKOPOCT� �O OC� X � Z,   * 
C	*	                COOTBETCTBEHHO, /KM/	                   * 
C	*  �) C���A� �.(����� ���� ������ �������������)	               * 
C	*	XI(N2) - REAL, KOOP��HAT� ���OB CETK� �O OC� X, /KM/       * 
C	*	ZI(M2) - REAL, KOOP��HAT� ���OB CETK� �O OC� Z, /KM/	   * 
C	*		                                                       * 
C	*  => HEO�PE�E�EHH�E �HA�EH�� CKOPOCT� �A�A�TC� H���M�          * 
C	* ------------------------------------------------------------- * 
C	*	                                                       	   * 
C	* IVR - INTEGER, PA�PE�AET (=1) ��� OTMEH�ET (=0) BBO� HRL(NR)  * 
C	* HRL(NR) - REAL, MACC�B B�COT PE��E�A ��H�� HA����EH��, /KM/   * 
C	* NR  - INTEGER, ��C�O OTC�ETOB MACC�BA HRL(), NR<300	       * 
C	* HXR - REAL, �A� �A�AH�� PE��E�A �O OC� X (HA��HA� C XH), /KM/ * 
C	*		                                                       * 
C	* IVB - INTEGER, (0 ��� 1) �P� IVB=1 BBO��TC� ZRB()	           * 
C	* ZRB(NRB) - REAL, MACC�B �����H OTPA�A��E� �PAH���, /KM/	   * 
C	* NRB - INTEGER, ��C�O OTC�ETOB ZRB( ), NRB<300	               * 
C	* HXB - REAL, �A� �A�AH�� OTPA�A��E� �PAH��� �O OC� X, /KM/	   * 
C	*		                                                       * 
C	* SXZ(3,NS) - REAL,MACC�B HOMEPOB � X-Z-KOOP��HAT �CTO�H�KOB,/KM/
C	* NS  - INTEGER, ��C�O �CTO�H�KOB, NS<5	                       * 
C	* ALAR(NAL) - REAL, MACC�B ���OB BXO�A TPACC�P�EM�X ���E�, /PA�/* 
C	*	        (�O �MO��AH�� �A�A�TC� ���� B ��A�A�OHE 0-40 �PA-  * 
C	*	        ��COB C �A�OM ��� �PA��C� <2*1.57/90 PA�>)	       * 
C	* NAL - INTEGER, ��C�O TPACC�P�EM�X ���E�, NAL<300	           * 
C	* IVAL - INTEGER, (0 ��� 1) �P� IVAL=1 BBO��TC� MACC�B ALAR(NAL)* 
C	* DS  - REAL, �A� TPACC�POBAH�� ���A  (DS > LAMBDA), KM	       * 
C	*		                                                       * 
C	*		                                                       * 
C	***************************************************************** 
C			  
C		             CEPB�CH�E �APAMETP� (BBO��TC�)	  
C		***************************************************************** 
C		*	                                                            * 
C		* IGRAF - INTEGER, �APAMETP, �A�A���� (=1) ��� OTMEH����� (=0)  * 
C		*         �PA���ECKOE ��O�PA�EH�E TPAEKTOP�� ���E� � ���������� * 
C		*         �P� IGRAF=1 �A�A�TC�:	                                * 
C		* MBX - REAL, MAC�TA� �PA���ECKO�O ��O�PA�EH�� �O OC� X	        * 
C		*       (KO���ECTBO E��H�� ��MEPEH�� /��/ � 1-� �������)	    * 
C		* MBZ - REAL, MAC�TA� �PA���ECKO�O ��O�PA�EH�� �O OC� Z	        * 
C		* MBT - REAL, ������� ������������ ����������� �� ��� T	        * 
C		* HXC -  REAL,   �A� ��������� OCE� KOOP��HAT �PA���ECKO�O	    * 
C		* HZC -  REAL,   ��O�PA�EH�� �O OC� X,Z  /��/	                *
C		* HTC -  REAL,  �  T, ��������������, /C/	                    *
C		* VRED-  REAL,   �������� �������� ��� ����������, (��/�)	    * 
C		***************************************************************** 
C			  
C		                PA�O��E �APAMETP�                               
C		***************************************************************** 
C		*	                                                            * 
C		***************************************************************** 
C			  
C		                B�XO�H�E �APAMETP�	  
C		***************************************************************** 
C		* SXZ(3,NS) - REAL, HOMEPA � X-Z-KOOP��HAT� �CTO�H�KOB	        * 
C		* NS  - INTEGER, KO���ECTBO �CTO�H�KOB	                        * 
C		* ALAR(NAL) - REAL, MACC�B ���OB BXO�A TPACC�P�EM�X ���E�, /PA�/* 
C		* NAL - INTEGER, KO�-BO TPACC�P�EM�X ���E� ��� KA��O�O �CT-KA	* 
C		* T(NAL)  - REAL, MACC�B BPEMEH PAC�POCTPAHEH�� TPAC-X ���E�	* 
C		* L(NAL)  - REAL, MACC�B �A� �OH��POBAH�� COOTBET. HA����EH��	* 
C		*                 TPACC�P�EM�X ���E�	                        * 
C		* X(NAL)  - REAL, MACC�B X-KOOP��HAT TO�EK B�XO�A HA �OBEPXHOCT�* 
C		* XC(NAL) - REAL, MACC�B �EHTPOB �A� �OH��POBAH��	            * 
C		* TR(NAL) - REAL, MACC�B �������������� BPEMEH	                * 
C		* GODS(3,NAL,NS) REAL, ������, ���������� T,L,X ��� ����	    * 
C		*                      ������� ������	                        * 
C		* ###       ���������� ���� ����� (NAL*NS) ���������������	    *
C		*           ������������ �� ���� �� ��������� ���� "RAYPATH"	* 
C		* RXZ(2,512) - REAL, MACC�B TEK���X X-Z-KOOP��HAT TPA�������	* 
C		*       1024         ��� KA��O�O ������������� ���A	            * 
C =>  * ? MO�ET ��T� ������ RXZ(2,NST), ��E	                            * 
C		*   NST - INTEGER, ��C�O �A�OB �P� PAC�ETE TPAEKTOP�� ���A (ISH)* 
C		* ###       ��� ������ ������������ (6-��) ������ ������	    *
C		*           ������������ � ����������� � ������� ���� "OUT"	    *
C		***************************************************************** 
C
	INCLUDE 'FGRAPH.FI'
	INCLUDE 'FGRAPH.FD'
C			  
C	 +++ O��CAH�� �APAMETPOB BBO�A � B�BO�A +++	  
	DIMENSION VF2(100,200),XI(200),ZI(100),       
	#          HRL(300),ZRB(300),                            
	#    SXZ(3,5),ALAR(2300),T(2300),X(2300),XC(2300),TR(2300),
     #    RXZ(2,1024),           
	#    GODS(3,2300,5)	                   
	REAL XH,XK,MIZ,HX2,HZ2,HXR,HXB,DS,L(2300)                       
	INTEGER N2,M2,IVR,NR,IVB,NRB,NS,IVAL,NAL                   
C			  
C		+++ O��CAH�� CEPB�CH�X � PA�O��X �APAMETPOB +++	  
	DIMENSION  NST(5,2300)                                   
	REAL MBX,MBZ,MBT
	INTEGER IGRAF                                 
	REAL TT(2300),ZT	  
C			  
	REAL XS,ZS,AL0,ZM,X1,Z1,T1,L1	     
	INTEGER ISH,JLEFT,JRIGHT	              
	LOGICAL KAV	  
	CHARACTER*60 FNAMEIN
      CHARACTER*60 FNAMEOUT1,FNAMEOUT2
	CHARACTER*1 RAY,GOD,CHRED
C				                
C	 +++ ��������� ����� +++
	REAL HXC,HZC,HTC,VRED
      INTEGER XHC,XKC,ZHC,ZKC,THC,TKC,NCOL
	INTEGER*2 DUMMY,DUMMY4
      CHARACTER*65 SECTNAME,PICTYPE
      CHARACTER*10 XNAME,ZNAME,TNAME
	RECORD /xycoord/ s
      RECORD /xycoord/ xz
C				                                                                  
	EXTERNAL STEP                     
	EXTERNAL LINT
	EXTERNAL RUNGE                     
      EXTERNAL SYMPL
      EXTERNAL FILTR                     
CCCC     EXTERNAL NET                     
C				                
	WRITE(*,1000)
 1000 FORMAT(/////,1X,'������������� ����� ��������������� ����'/
	#              1X,'��������� "RAYMIR"   Version 2.0'/
	#              1X,'Copyright (C) Sheludko 1992'//)
C				                                                                  
	WRITE(*,1)
    1 FORMAT(1X,'������� ��� ����� ������ ����� - '\)
	READ(*,'(A)') FNAMEIN
C
      OPEN(5,FILE=FNAMEIN,STATUS='OLD',FORM='FORMATTED',RECL=80)
C
	READ(5,2000) sectname,XH,XK,MIZ,HX2,HZ2,N2,M2,IVR,HXR,NR,
	#             IVB,HXB,NRB,NS,DS,IVAL,NAL,
	#          IGRAF,MBX,MBZ,MBT,HXC,HZC,HTC,VRED
 2000 FORMAT(/5X,A65/1X,F6.2/1X,F6.2/1X,F6.2/1X,F6.2/1X,F6.2/
	#       1X,I6/1X,I6/1X,I6/1X,F6.2/1X,I6/
	#       1X,I6/1X,F6.2/1X,I6/1X,I6/1X,F6.3/1X,I6/1X,I6/
	#       1X,I6/,1X,F6.3/1X,F6.3/1X,F6.3/
	#       1X,F6.2/1X,F6.2/1X,F6.3/1X,F6.3)
CCCC  	    WRITE(6,2000) sectname,XH,XK,MIZ,HX2,HZ2,N2,M2,IVR,HXR,NR,
CCCC	#             IVB,HXB,NRB,NS,DS,IVAL,NAL,
CCCC	#          IGRAF,MBX,MBZ,MBT,HXC,HZC,HTC,VRED
C	
	 WRITE(*,2)
    2 FORMAT(1X,'������� ��� ����� �������� ������ - '\)
	READ(*,'(A)') FNAMEOUT1
C
	OPEN(6,FILE=FNAMEOUT1,STATUS='NEW',FORM='FORMATTED',RECL=80)
C
C	*** BBO� PAC�PE�E�EH�� CKOPOCT� VF2(M2,N2),                   *** 
C	*** �A�AHHO�O HA �P�MO��O��HO� CETKE � �PE�HA�HA�EHHO�O HE�O- *** 
C	*** CPE�CTBEHHO ��� TPACC�POBAH�� ���E�                       *** 
	write(6,*) vred
	WRITE(6,360) M2,N2 			                                                                  
	DO 320 J=1,M2                                                     
	READ(5,330) (VF2(J,I),I=1,N2)                                     
  	       WRITE(6,330) (VF2(J,I),I=1,N2)  
  320 CONTINUE                                                          
  330 FORMAT (10F6.3)                                                   
C+++	ZM=MIZ+(M2-1)*HZ2                                              
CCCC	         			                                                                  
	READ(5,340) (ZI(J),J=1,M2)                                        
  340 FORMAT(10F6.2)                                                    
	ZM=ZI(M2)                                                         
	        WRITE(6,340) (ZI(J),J=1,M2)    
      READ(5,350) (XI(I),I=1,N2)                                        
  350 FORMAT(10F6.2)                                                    
	        WRITE (6,350) (XI(I),I=1,N2)		                                                                  
	WRITE(6,360) M2,N2                                                
  360 FORMAT(//1X,'PAC�PE�E�EH�E CKOPOCT� VF2(M2,N2)',5X,'M2 =',I5,     
	#           5X,'N2 =',I5/)                                         
	DO 370 J=1,M2                                                     
	WRITE(6,380) (VF2(J,I),I=1,N2)                                    
  370 CONTINUE                                                          
  380 FORMAT(10F6.3)                                                    
	WRITE(6,390)                                                      
  390 FORMAT(/1X,'XI(N2)'/)                                             
	WRITE(6,400) (XI(I),I=1,N2)                                       
  400 FORMAT (1X,10F6.2)                                                
	WRITE(6,410)                                                      
  410 FORMAT(/1X,'ZI(M2)'/)                                             
	WRITE(6,420) (ZI(J),J=1,M2)                                       
  420 FORMAT(1X,10F6.2)                                                 
C	 *** BBO� CKOPOCTHO�O PAC�PE�E�EH�� �AKOH�EH ***        
C				                                                                
C	 *** BBO� PE��E�A ��H�� HA����EH�� ***                             
  570 IF(IVR.NE.1) GOTO 112                                             
       READ(5,110) (HRL(I),I=1,NR)                                      
  110  FORMAT(10F6.3)                                                   
       WRITE(6,111)                                                     
  111  FORMAT(/1X,'PE��E� ��H�� HA����EH�� HRL(NR)'/)                   
	 WRITE(6,110) (HRL(I),I=1,NR)                                     
C	
C	 *** BBO� HOMEPOB � X-Z-KOOP��HAT �CTO�H�KOB ***                   
  112 READ(5,120) ((SXZ(I,J),I=1,3),J=1,NS)                             
  120 FORMAT(5(F3.0,F7.2,F6.2))                                         
	WRITE(6,121)                                                      
  121 FORMAT(/1X,'HOMEPA � X-Z KOOP��HAT� �CTO�H�KOB SXZ(3,NS)'/)       
	WRITE(6,120) ((SXZ(I,J),I=1,3),J=1,NS)                            
C				                                                                
C	*** BBO� ���OB BXO�A TPACC�P�EM�X ���E� ***                       
	IF(IVAL.EQ.1) GOTO 131                                            
	JRIGHT=NAL/2                                                            
	DAL=1.570796/90.0                                                 
C+++	  DAL=2*DAL
CCCC	DAL=akugol*DAL
	      DAL=0.5*DAL
                                                             
	DO 130 J=1,NAL                                                    
C#########################################################################
C###	dalnat=300.
	ALAR(J)=J*DAL +dalnat*DAL                                                    
	IF(J.GT.JRIGHT) ALAR(J)=3.141592-(dalnat*DAL)-(J-JRIGHT)*DAL                           
C#########################################################################
  130 CONTINUE                                                          
C				                                                                
  131 IF(IVAL.EQ.0) GOTO 141                                            
	READ(5,140) (ALAR(J),J=1,NAL)                                     
  140 FORMAT(10F7.4)                                                    
  141 WRITE(6,142)                                                      
  142 FORMAT(/1X,'���� BXO�A TPACC�P�EM�X ���E� ALAR(NAL)'/)            
      WRITE(6,140) (ALAR(J),J=1,NAL)                                    
C				                                                                
	JLEFT=NAL                                                                       
	IF(ALAR(1).GT.1.570796) JLEFT=1                                                                       
	DO 143 J=2,NAL                                                                       
  143 IF(ALAR(J-1).LT.1.570796.AND.ALAR(J).GT.1.570796) JLEFT=J                                                                       
C				                                                                
C	 *** BBO� PE��E�A OTPA�A��E� �PAH��� ***                           
	IF(IVB.NE.1) GOTO 300                                             
	  READ(5,150) (ZRB(J),J=1,NRB)                                    
150	  FORMAT(10F6.2)                                                  
	  WRITE(6,151)                                                    
151	  FORMAT(/1X,'PE��E� OTPA�A��E� �PAH��� ZRB(NRB)'/)               
	  WRITE(6,150) (ZRB(J),J=1,NRB)
C                                       
C	 +++ PAC�ET TPAEKTOP�� ���E� +++                                   
C	 ================================================================= 
C				                                                                
  300 FNAMEOUT2='RAYPATH' 
	OPEN(20,FILE=FNAMEOUT2,STATUS='NEW',FORM='BINARY',RECL=1024)
C
C	 *** �A�AH�E �CTO�H�KOB ***                                        
	DO 700 K=1,NS                                                     
	SP=SXZ(1,K)                                                       
	XS=SXZ(2,K)                                                       
	ZS=SXZ(3,K)                                                       
C			                                                                  
C	 *** �A�AH�E ���OB BXO�A ���E� ***                                 
	DO 600 J=1,NAL                                                    
	AL0=ALAR(J)                                                       
	T(J)=0.0                                                          
	X(J)=0.0                                                          
	L(J)=0.0                                                          
	XC(J)=0.0                                                         
	TR(J)=0.0                                                          
C**	NST(K,J)=0                                                        
C			                                                                  
	GODS(1,J,K)=0.0                                                                       
	GODS(2,J,K)=0.0                                                                       
	GODS(3,J,K)=0.0                                                                       
C			                                                                  
	DO 580 I=1,1024     !!!NEW                                                    
	RXZ(1,I)=0.0                                                      
	RXZ(2,I)=0.0                                                      
  580 CONTINUE                                                          
C
C	 *** PAC�ET BPEMEH� �PO�E�A � TPAEKTOP�� ���A ***                  
	CALL STEP(XS,ZS,AL0,DS,VF2,N2,M2,HX2,HZ2,XH,XK,MIZ,ZM,            
	#    XI,ZI,IVB,HXB,NRB,ZRB,X1,Z1,T1,RXZ,ISH,KAV)                   
C**	NST(K,J)=ISH                                                      
	IF(.NOT.KAV) GOTO 581                                             
	T(J)=T1                                                           
	X(J)=X1                                                           
	L1=ABS(X1-XS)                                                   
	L(J)=L1                                                               
	XC(J)=(XS+X1)*0.5                                                 
	TR(J)=T1-L1/VRED                                                           
C+++  ������������� ������ ������ ��� ������ "������������ ��������"                                                                       
C+++  (����� ���� ����������� ��� �������� �������� ����������)
C+++  ZT=0.1*L(J)/(2*5.0)                                               
C+++  TT(J)=2/0.1*ALOG(ZT+SQRT(ZT**2+1))                                
C			                                                                  
	GODS(1,J,K)=T1                                                                     
	GODS(2,J,K)=L1                                                                     
	GODS(3,J,K)=X1
		 write(6,631) ALAR(J), T(J),X(J),L(J),XC(J),TR(J)          
C			                                                                  
C			                            NST1=NST(K,J)=ISH                     
C			*** �A��C� TPAEKTOP�� ���A RXZ(2,ISH) HA �EHT� ***                
  581 WRITE(20) RXZ                                                     
  600 CONTINUE                                                          
C	 *** KOHE� ��K�A ���OB ***                                         
C			                                                                  
C	 *** �E�AT� PE����TATOB TPACC�POBAH�� ��� KA��O�O �CTO�H�KA ***    
		WRITE(6,610) SP                                                   
  610 FORMAT(//1X,'��HKT B�P�BA ',2X,F5.1/)                             
C			                                                                  
	WRITE(6,620)                                                      
  620 FORMAT(/8X,'AL',10X,'T',11X,'X',11X,'L',10X,'XC',10X,'TRED'//)            
C+++ #                                                ,9X,'T REAL'//)   
C####     	DO 630 J=1,NAL                                                    
C####	        WRITE(6,631) ALAR(J),T(J),X(J),L(J),XC(J),TR(J)                         
C+++ #                                         ,TT(J)                     
C***  �O�KOPP. FORMAT=> C�E�AT� J=2,NAL � �E�ATAT� E�E (J+1)-E          
  630 CONTINUE                                                          
  631 FORMAT(2(5X,F7.4,5X,F7.3,5X,F7.3,5X,F7.3,5X,F7.3,5X,F7.3))                
C+++ #                                                ,5X,F7.3))        
  700 CONTINUE                                                          
C	 *** KOHE� ��K�A �CTO�H�KOB ***                                    
C
	WRITE(*,1700) FNAMEOUT2
 1700 FORMAT(//1X,'���������� ����� �������� � ���� - ',A8/)      
C			                                                                  
C	 ================================================================= 
C	 *** �PA���ECKOE ��O�PA�EH�E TPAEKTOP�� ���E�  ***                 
C	 ================================================================= 
C			                                                                  
	IF(IGRAF.EQ.0) GOTO 850                                           
C
CCCC	DUMMY = setvideomode($ERESCOLOR)
CCCC	CALL clearscreen($GCLEARSCREEN)
C			                                                                  
C###	WRITE(*,3000)
C#### 3000 FORMAT(1X,'������ ���������� ���� ?   (Y/N) - ')
C####	READ(*,'(A)') RAY
C####	IF(RAY.EQ.'N') GOTO 750
C
CCCC	CALL clearscreen($GCLEARSCREEN)
CCCC	XNAME='X,KM'
CCCC	ZNAME='Z,KM'
CCCC	PICTYPE='������������� �����'      
C			                                                                  
C	 *** PAC�ET XH � XK ***                                            
CCCC	XH=XI(1)                                                          
CCCC	XK=XI(N2)                                                         
CCCC			                                                                  
CCCC	 *** PAC�ET PA�MEPOB PA�O�E�O �O�� ***                             
CCCC	PX=(XK-XH)*MBX                                                    
CCCC	PZ=(ZM-MIZ)*MBZ                                                   
C++		IF(PX.GT.600) WRITE(*,701)
C+701 FORMAT(1X,'�������������� ������ ������ 600 ��������')
C+		IF(PZ.GT.300) WRITE(*,702)
C+702 FORMAT(1X,'������������ ������ ������ 300 ��������')
C			                                                                  
C	 *** ����������� �������� "��������������������" ����� ***
CCCC	XHC=INT(XH/HXC+1.E-5)*HXC
CCCC	XKC=(INT(XK/HXC-1.E-5)+1)*HXC
CCCC	ZHC=INT(MIZ/HZC+1.E-5)*HZC
CCCC	ZKC=(INT(ZM/HZC-1.E-5)+1)*HZC
C   
C	 *** ����������� �������� � ���������� ����� ***
CCCC	CALL setvieworg(40,20,s)
CCCC	CALL NET(XH,XK,MIZ,ZM,MBX,MBZ,HXC,HZC,XNAME,ZNAME,SECTNAME,
CCCC	#         PICTYPE)                                                                   
C
C	 *** ��������� ���������� ������� (��� IVB=1)  
CCCC	IF(IVB.NE.1) GOTO 704
CCCC		NCOL=12
CCCC	DUMMY4=SETCOLOR(NCOL)
C
C	 *** �O�BO� �EPA K ������� ***                        
CCCC	IX=INT((XH-XHC)/MBX+1.E-5)                                  
CCCC	IZ=INT((ZRB(1)-ZHC)/MBZ+1.E-5)
CCCC	CALL MOVETO(IX,IZ,xz)
C			                                                                  
CCCC	DO 703 I=2,NRB                                                    
CCCC	IX=INT((XH+(I-1)*HXB-XHC)/MBX+1.E-5)                                  
CCCC	IZ=INT((ZRB(I)-ZHC)/MBZ+1.E-5)
CCCC	DUMMY=LINETO(IX,IZ)
CCCC  703 CONTINUE                                                          
C			                                                                  
C	 *** CO�CTBEHHO TPACC�POBAH�E ���E� ***                            
C
C###################################################################
C                  go to 850
C###################################################################				 			                                                                  
CCCC  704 REWIND 20
C
CCCC	DO 740 K=1,NS                                                     
CCCC	IF(K.EQ.1) DUMMY4=SETCOLOR(12)
CCCC	IF(K.EQ.2) DUMMY4=SETCOLOR(10)
CCCC	IF(K.EQ.3) DUMMY4=SETCOLOR(14)
CCCC	IF(K.EQ.4) DUMMY4=SETCOLOR(13)
CCCC	IF(K.EQ.5) DUMMY4=SETCOLOR(5)
C			                                                                  
CCCC	DO 740 J=1,NAL                                                    
C     *** �TEH�E KOOP��HAT O�EPE�HO�O ���A (MACC�B RXZ(512,2)) ***      
CCCC	READ(20) RXZ
CCCC			                                                                  
CCCC	 *** �O�BO� �EPA K O�EPE�HOM� �CTO�H�K� ***                        
CCCC	IX=INT((RXZ(1,1)-XHC)/MBX+1.E-5)                                  
CCCC	IZ=INT((RXZ(2,1)-ZHC)/MBZ+1.E-5)
CCCC	CALL MOVETO(IX,IZ,xz)
C			                                                                  
CCCC	DO 720 I=1,511                                                    
C**	DO 720 I=1,NST1                                                   
C			                                                                  
CCCC	IF(RXZ(1,I).EQ.0.0.AND.RXZ(1,I+1).EQ.0.0                     
CCCC	#    .AND.RXZ(2,I).EQ.0.0.AND.RXZ(2,I+1).EQ.0.0) GOTO 720           
C			                                                                  
CCCC	IX=INT((RXZ(1,I)-XHC)/MBX+1.E-5)                                  
CCCC	IZ=INT((RXZ(2,I)-ZHC)/MBZ+1.E-5)
C
CCCC	DUMMY=LINETO(IX,IZ)
C
CCCC  720 CONTINUE                                                          
C	 *** KOHE� ��K�A O�EPE�HO�O ���A ***                               
C			                                                                  
CCCC  740 CONTINUE                                                          
C	 *** KOHE� ��K�OB �EPE�OPA ���E� � �CTO�H�KOB ***                  
C			                                                                  
CCCC	READ(*,*)
CCCC	CALL clearscreen($GCLEARSCREEN)
C
CCCC  750 CONTINUE 
C	
CCCC	CLOSE(20,STATUS='DELETE')
C
C		========================================
C		=     ����������� ����� ����������     =
C		========================================
C			                                                                  
CCCC	WRITE(*,4000)
CCCC 4000 FORMAT(1X,'������ ���������� ��������� ?   (Y/N) - '\) 
CCCC	READ(*,'(A)') GOD
CCCC	IF(GOD.EQ.'N') GOTO 845
C
CCCC	CALL clearscreen($GCLEARSCREEN)
CCCC	TNAME='T-L/VR, c'
CCCC	PICTYPE='���������'
C			 
C	 *** PAC�ET TMIN � TMAX ***                                            
CCCC  760 TMIN=GODS(1,1,1)-GODS(2,1,1)/VRED
CCCC	TMAX=TMIN
CCCC	DO 800 K=1,NS
CCCC	DO 800 J=1,NAL
CCCC	TRJ=GODS(1,J,K)-GODS(2,J,K)/VRED
CCCC	IF(TRJ.GT.TMAX) TMAX=TRJ
CCCC	IF(TRJ.LT.TMIN) TMIN=TRJ
CCCC  800 CONTINUE
C
C	 *** PAC�ET PA�MEPOB PA�O�E�O �O�� ***                             
CCCC	PX=(XK-XH)*MBX                                                    
CCCC	PT=(TMAX-TMIN)*MBT                                                   
C+	IF(PX.GT.600) WRITE(*,801)
C+801 FORMAT(1X,'�������������� ������ ������ 600 ��������')
C+	IF(PT.GT.300) WRITE(*,802)
C+802 FORMAT(1X,'������������ ������ ������ 300 ��������')
C				                                                                
C	 *** ����������� �������� "��������������������" ����� ***
CCCC	XHC=INT(XH/HXC+1.E-5)*HXC
CCCC	XKC=(INT(XK/HXC-1.E-5)+1)*HXC
CCCC	THC=INT(TMIN/HTC+1.E-5)*HTC
CCCC	TKC=(INT(TMAX/HTC-1.E-5)+1)*HTC
C
C	 *** ����������� �������� � ���������� ����� ***
CCCC	CALL setvieworg(40,20,s)
CCCC	CALL NET(XH,XK,TMIN,TMAX,MBX,MBT,HXC,HTC,XNAME,TNAME,SECTNAME,
CCCC	#         PICTYPE)
C
C	 *** CO�CTBEHHO ��������� ���������� ***
C				                                                                
CCCC	DO 840 K=1,NS
C				                                                                
CCCC	IF(K.EQ.1) DUMMY4=SETCOLOR(11)
CCCC	IF(K.EQ.2) DUMMY4=SETCOLOR(10)
CCCC	IF(K.EQ.3) DUMMY4=SETCOLOR(14)
CCCC	IF(K.EQ.4) DUMMY4=SETCOLOR(13)
CCCC	IF(K.EQ.5) DUMMY4=SETCOLOR(5)
C				                                                                
CCCC	DO 820 J=1,NAL
C				                                                                
CCCC	IF(GODS(1,J,K).EQ.0.0) GOTO 820
C	 
CCCC	IX=INT((GODS(3,J,K)-XHC)/MBX+1.E-5) 
CCCC	IT=INT((GODS(1,J,K)-GODS(2,J,K)/VRED-THC)/MBT+1.E-5)   
CCCC	dummy = setpixel(IX+1,IT-1)    !
CCCC      dummy = setpixel(IX-1,IT+1)    !
CCCC	dummy = setpixel(IX+1,IT+1)    ! new
CCCC	dummy = setpixel(IX-1,IT-1)    !
CCCC	dummy = setpixel(IX,IT)        !             
C
C	 *** ������ ���� � ������ ����� ��������� *** 
CCCC	IF(J.EQ.1.OR.J.EQ.JLEFT) CALL MOVETO(IX,IT,xz) 
CCCC	IF(J.NE.1.AND.GODS(1,J-1,K).EQ.0.0) CALL MOVETO(IX,IT,xz) !!! new edition
C	 
CCCC	  DUMMY=LINETO(IX,IT)
C
CCCC  820 CONTINUE
C	 *** KOHE� ��K�A O�EPE�HO�O ��������� ***                               
C				                                                                
CCCC  840 CONTINUE
C 	 *** KOHE� ��K�� �CTO�H�KOB ***                  
C				                                                                
CCCC	READ(*,*)
C				                                                           
CCCC	WRITE(*,6000)                                                
CCCC 6000 FORMAT(//1X,'������ ������������ ��������� � ������ ��������� ?
CCCC	#   (Y/N) - '\)                                               
CCCC	READ(*,'(A)') CHRED                                          
CCCC	IF(CHRED.EQ.'Y') GOTO 7000                                   
CCCC	GOTO 845                                                     
CCCC 7000 WRITE(*,8000)                                                
CCCC 8000 FORMAT(1X,'������� ����� �������� �������� �������� - '\)
CCCC	READ(*,9000) VRED                                         
CCCC 9000 FORMAT(F5.2)                                         
CCCC      CALL clearscreen($GCLEARSCREEN)
CCCC	GOTO 760                                                     
C		                                                          
CCCC  845 DUMMY = setvideomode($DEFAULTMODE)
C
  850 CONTINUE
C		                                                             
	CLOSE(6,STATUS='KEEP')
	CLOSE(5,STATUS='KEEP')
C
C
	END                                                               
