C                                                                       
      SUBROUTINE STEP(XS,ZS,AL0,DS,VF2,N2,M2,HX2,HZ2,XH,XK,MIZ,ZM,      
     #          XI,ZI,IVB,HXB,NRB,ZRB,X1,Z1,T1,RXZ,ISH,KAV)             
C     =================================================                 
C     =    �O��PO�PAMMA �O�A�OBO�O PAC�ETA ���A       =                 
C     =    HA OCHOBE  XS,ZS,AL0,DS  �  V0,V0X,V0Z,    =                 
C     =    A TAK�E O���X �APAMETPOB CKOPOCTHO�O       =                 
C     =    PA�PE�A VF2,N2,M2,XH,XK,MIZ,ZM,HX2,HZ2,    =                 
C     =    IVB,NRB,ZRB,HXB       XI,ZI                =                 
C     =    O�PA�AETC� K �O��PO�PAMMAM RUNGE,LINT      =                 
C     =================================================                 
C                                                                     
      DIMENSION VF2(100,200),ZRB(300),RXZ(2,1024),XI(200),ZI(100)        
      REAL XS,ZS,AL0,DS,HX2,HZ2,XH,XK,MIZ,ZM,HXB,X1,Z1,T1               
      INTEGER N2,M2,IVB,NRB,ISH                                         
      LOGICAL KAV                                                       
C                                                                       
      DIMENSION ETA(4),FI(4),PSI(4)                                     
      REAL KSI(4)                                                       
      REAL X,Z,AL,T,V0,V0X,V0Z,KSI0,ETA0,FI0,PSI0                       
C                                                                       
C+    DO 2 I=1,500                                                      
C+    RXZ(1,I)=0.0    B�HEC�� B �O�OBH�� �PO�-M�                        
C+    RXZ(2,I)=0.0                                                      
C+  2 CONTINUE                                                          
C                                                                       
      ISH=1                                                             
      X=XS                                                              
      Z=ZS                                                              
      T=0.0                                                             
      AL=AL0                                                            
             write (6,3000) AL,T,X,Z,ISH                                                          
C     *** 1 - METKA BO�BPATA ��� C�E����E�O �A�A ***                    
C                                                                       
C     *** �AHECEH�E TEK���X KOOP��HAT ���A B MACC�B ***                 
C                                                                       
    1 RXZ(1,ISH)=X                                                      
      RXZ(2,ISH)=Z                                                    
C                                                   
      CALL LINT(VF2,N2,M2,XI,ZI,X,Z,V0,V0X,V0Z,KAV)                     
C                                                                       
C     *** �POBEPKA HAXO��EH�� TEK��E� TO�K� ���A (X,Z) B  ***           
C     *** O��ACT� O�PE�E�EH�� CKOPOCT�  (KAV / .NOT.KAV)  ***           
      IF(.NOT.KAV) GOTO 11                                              
C                                                                       
      CALL RUNGE(AL,KSI0,ETA0,FI0,PSI0,V0,V0X,V0Z)                      
      KSI(1)=KSI0                                                       
      ETA(1)=ETA0                                                       
       FI(1)=FI0                                                        
      PSI(1)=PSI0                                                       
C                                                                       
      I1=1                                                              
      DO 10 J=1,3                                                       
      CD=DS*0.5                                                         
      IF(J.EQ.3) CD=DS                                                  
C                                                                       
      IF(.NOT.KAV) GOTO 10                                              
      I1=I1+1                                                           
      XST=X+KSI0*CD                                                     
      ZST=Z+ETA0*CD                                                     
      ALS=AL+FI0*CD                                                     
      CALL LINT(VF2,N2,M2,XI,ZI,XST,ZST,V0,V0X,V0Z,KAV)                 
      IF(.NOT.KAV) GOTO 10                                              
      CALL RUNGE(ALS,KSI0,ETA0,FI0,PSI0,V0,V0X,V0Z)                     
      KSI(I1)=KSI0                                                      
      ETA(I1)=ETA0                                                      
       FI(I1)=FI0                                                       
      PSI(I1)=PSI0                                                      
   10 CONTINUE                                                          
C                                                                       
C     *** EC�� TEK��A� TO�KA ���A - BHE O��ACT�, �XO��M B KOHE� ***     
      IF(.NOT.KAV) GOTO 11                                              
      GOTO 13                                                           
C++11 PRINT 12, XS,AL0                                                  
C++12 FORMAT(/1X,'XS = ',F6.2,5X,'AL0 = ',F6.4,5X,'OUT OF REGION')      
   11 GOTO 140                                                          
C                                                                       
C     *** PAC�ET �O�O�EH�� TO�K� ���A � BPEMEH� �OC�E �A�A ***          
   13 X1=X+DS*(KSI(1)+2*KSI(2)+2*KSI(3)+KSI(4))/6                       
      Z1=Z+DS*(ETA(1)+2*ETA(2)+2*ETA(3)+ETA(4))/6                       
      AL1=AL+DS*(FI(1)+2*FI(2)+2*FI(3)+FI(4))/6                         
      T1=T+DS*(PSI(1)+2*PSI(2)+2*PSI(3)+PSI(4))/6                       
C                                                                       
C     ### EC�� OTPA�A��A� �PAH��A HE �A�AHA, O�XO��M �TOT ��OK ###      
      IF(IVB.EQ.0) GOTO 30                                              
C                                                                       
C     *** �POBEPKA BO�MO�HOCT� �EPECE�EH�� OTPA�A��E� �PAH��� ***       
C                                                                       
      NLX=INT((X1-XH)/HXB+1.E-5)+1                                      
      IF(NLX.GT.NRB-1) NLX=NLX-1                                        
      IF(Z1.GE.ZRB(NLX)+(ZRB(NLX+1)-ZRB(NLX))/HXB*(X1-XH-(NLX-1)*HXB))  
     #   GOTO 20                                                        
      GOTO 30                                                           
   20 XB=(Z-ZRB(NLX)+(ZRB(NLX+1)-ZRB(NLX))/HXB*                         
     #   (XH+(NLX-1)*HXB)-(Z1-Z)/(X1-X)*X)/                             
     #   ((ZRB(NLX+1)-ZRB(NLX))/HXB-(Z1-Z)/(X1-X))                      
      ZB=Z+(Z1-Z)/(X1-X)*(XB-X)                                         
      T1=T1-SQRT((X1-XB)**2+(Z1-ZB)**2)*                                
     #   (PSI(1)+2*PSI(2)+2*PSI(3)+PSI(4))/6                            
      X1=XB                                                             
      Z1=ZB                                                             
      IF(ZRB(NLX+1).GT.ZRB(NLX)) AL1=                                   
     # -AL1+2*ATAN((ZRB(NLX+1)-ZRB(NLX))/HXB)                           
      IF(ZRB(NLX+1).LE.ZRB(NLX)) AL1=                                   
     # -AL1-2*ATAN((ZRB(NLX)-ZRB(NLX+1))/HXB)                           
      GOTO 130                                                          
C     *** �POBEPKA � O�PA�OTKA OTPA�EH�� �AKOH�EHA ***                  
C                                                                       
C     +++ ��A�HOCT�KA �EPECE�EH�� ���OM �PAH�� O��ACT� +++              
C                                                                       
C     *** B�XO� HA �OBEPXHOCT� HA����EH�� * KOHE� TPAEKTOP�� ***        
   30 IF(Z1.GT.MIZ) GOTO 40                                             
      X1=X1-(Z1-MIZ)*(X1-X)/(Z1-Z)                                      
      T1=T1-(Z1-MIZ)*(T1-T)/(Z1-Z)                                      
      Z1=MIZ                                                            
      GOTO 150                                                          
C                                                                       
C     *** B�XO� �A H��H�� �PAH��� O��ACT� �A�AH�� CKOPOCT� ***          
   40 IF(Z1.GT.ZM) GOTO 50                                              
      GOTO 70                                                           
C++50 PRINT 60, XS,AL0                                                  
C++60 FORMAT(/1X,'XS = ',F6.2,5X,'AL0 = ',F6.4,5X,                      
C++  #       'CROSSING OF Z-BOUNDARY')                                  
   50 GOTO 140                                                          
C                                                                       
C     *** B�XO� �A �PAB�� KPA� �PO���� ***                              
   70 IF(X1.GT.XK) GOTO 80                                              
      GOTO 100                                                          
C++80 PRINT 90, XS,AL0                                                  
C++90 FORMAT(/1X,'XS = ',F6.2,5X,'AL0 = ',F6.4,5X,'XK-LIMIT')           
   80 GOTO 140                                                          
C                                                                       
C     *** B�XO� �A �EB�� �PAH��� �PO���� ***                            
  100 IF(X1.LT.XH-1.E-5) GOTO 110                                       
      GOTO 130                                                          
C+110 PRINT 120, XS,AL0                                                 
C+120 FORMAT(/1X,'XS = ',F6.2,5X,'AL0 = ',F6.4,5X,'XH-LIMIT')           
  110 GOTO 140                                                          
C                                                                       
C     *** �O��OTOBKA K C�E����EM� �A�� (O�HOB�EH�E �APAMETPOB) ***      
C     *** B��O�H�ETC� �P� KAV=.TRUE.                           ***      
  130 X=X1                                                              
      Z=Z1                                                              
      T=T1                                                              
      AL=AL1                                                            
      ISH=ISH+1
3000	FORMAT(F7.4,F6.3,1X,F7.3,1X,F6.3,I6)
                  write (6,3000) AL,T,X,Z,ISH 
C				                                              
      GOTO 1                                                            
C     *** BO�BPAT B HA�A�O ��� B��O�HEH�� C�E����E�O �A�A ***           
C                                                                       
C     *** O�PA�OTKA B�XO�A ���A �� O��ACT� O�PE�E�EH�� CKOPOCT� ***     
C     <<< 140 = L5 >>>   *** B��O�H�ETC� �P� .NOT.KAV ***               
C                                                                       
  140 KAV=.FALSE.                                                       
      GOTO 160                                                          
C-140 T(J)=0.0 \    T � X HE MO��T ��T� �C�O���OBAH�, T.K. O��CAH�      
C---  X(J)=0.0 /    B �AHHO� �O��PO�PAMME KAK �POCT�E �EPEMEHH�E        
C+++  L(J)=0.0                                                          
C+++  XC(J)=0.0                                                         
C+++  ALAR(J)=AL0                                                       
C                                                                       
C     +++ ��A�HOCT�KA �AKOH�EHA +++                                     
C                                                                       
C     <<< 150 = L4: >>>  ��EC� MO�HO PA�O�PAT�C� C KOH�OM ���A          
  150 ISH=ISH+1                                                         
      RXZ(1,ISH)=X1                                                     
      RXZ(2,ISH)=Z1                                                     
        x=X1                                                                  
        z=z1
	  T=T1 
	  write (6,3000) AL,T,X,Z,ISH 
C	                                                                              
  160 CONTINUE                                                          
C               write (6,3000) (XL(J,I),ZL(J,I)),I=1,AL0,XS,ISH                                                
      RETURN                                                            
      END                                                               
