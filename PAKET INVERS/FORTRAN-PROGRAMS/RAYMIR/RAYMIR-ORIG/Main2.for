LARGE
C			  
C		                 24.03.92.	  
C	==============================================================    
C	=	         ПPOГPAMMA *RAYMIR*                             =    
C	= PEШEHИE ПPЯMOЙ KИHEMATИЧECKOЙ ЗAДAЧИ ДЛЯ ДBУMEPHO-         =    
C	= HEOДHOPOДHOЙ ГPAДИEHTHOЙ CPEДЫ, ПOДCTИЛAEMOЙ OTPAЖAЮЩEЙ    =    
C	= ГPAHИЦEЙ.PACCЧИTЫBAЮTCЯ BPEMЯ И TPAEKTOPИИ PACПPOCTPAHEHИЯ =    
C	= PEФPAГИPOBAHHOЙ И OTPAЖEHHOЙ BOЛH.                         =    
C	= TPAEKTOPИИ ЛУЧEЙ ИЗOБPAЖAЮTCЯ B ГPAФИЧECKOM BИДE.          =    
C	=	                                                        =    
C	==============================================================    
C			  
C	ЯЗЫK ПPOГPAMMИPOBAHИЯ   FORTRAN-Microsoft Version 5.00
C	ИCПOЛЬЗУETCЯ БИБЛИOTEKA   GRAPHICS
C	OPГAHИЗAЦИЯ - ИГГ CO AH CCCP, ЛAБ.59
C	COPYRIGHT (C), 1989, BY I.F.SHELUDKO, IGG SB USSR AS	  
C	PROGRAMMA RABOTALA NEUSTOJCHIVO.VNESENY IZMENENIA V BLOKE
C     RISOVKI LUCHEJ.STR.     go to 850		  
C		              BXOДHЫE ДAHHЫE
CC$$$$*****************************************************************
CC$$$$  В ПРОГР, ВНЕСЕНЫ ИЗМЕНЕНИЯ - в вводном пакете задаются
CС$$$$	 УГОЛ ВХОДА - ПРОГР. УНИВЕРСАЛЬНАЯ 
C@@@@@ ПРОГР.для рефр.и отр.волн. отр.гр.рисуется только вместе с 
C@@@@@ с изолиниями!!!!!
C	***************************************************************** 
C	*	              PACПPEДEЛEHИE CKOPOCTИ                       * 
C	* ------------------------------------------------------------- * 
C	*	  MOЖET БЫTЬ ЗAДAHO PAЗЛИЧHЫMИ CПOCOБAMИ	               * 
C	*	  В ДАННОЙ ВЕРСИИ ПРОГРАММЫ (Version 2.0)                  * 
C	*    BBOДИTCЯ ЗAДAHHOE HA ПPЯMOУГOЛЬHOЙ CETKE PACПPEДEЛEHИE     * 
C	*    CKOPOCTИ, HE TPEБУЮЩEE ДOПOЛHИTEЛЬHOЙ OБPAБOTKИ (ПEPECЧETA * 
C	*    HA ДPУГУЮ CETKУ, CГЛAЖИBAHИЯ), ИCПOЛЬЗУEMOE HEПOCPEДCTBEH- * 
C	*    HO ДЛЯ TPACCИPOBAHИЯ ЛУЧEЙ	                               * 
C	*    VF2(M2,N2) - REAL, MACCИB ЗHAЧEHИЙ CKOPOCTИ, (KM/C)	       * 
C	*    N2,M2 - INTEGER, ЧИCЛO УЗЛOB CETKИ ПO OCИ X И Z COOTBET-   * 
C	*	                CTBEHHO (N2<200, M2<100)	               * 
C	*	                                                           * 
C	*	## KOOPДИHATЫ УЗЛOB PACПPEДEЛEHИЯ CKOPOCTИ MOГУT БЫTЬ      * 
C	*	ЗAДAHЫ B CЛEДУЮЩEM BИДE:	                               * 
C	*  A) XH,XK - REAL, ЛEBAЯ И ПPABAЯ ГPAHИЦЫ ПPOФИЛЯ ПO OCИ X,/KM/* 
C	*	MIZ    - REAL, УPOBEHЬ ПPИBEДEHИЯ ДAHHЫX ПO OCИ Z, /KM/	   * 
C	*	HX2,HZ2 - REAL, ШAГ CETKИ ЗAДAHИЯ CKOPOCTИ ПO OCИ X И Z,   * 
C	*	                COOTBETCTBEHHO, /KM/	                   * 
C	*  Б) CЛУЧAЙ Б.(МОЖЕТ БЫТЬ ЗАКРЫТ КОММЕНТАРИЯМИ)	               * 
C	*	XI(N2) - REAL, KOOPДИHATЫ УЗЛOB CETKИ ПO OCИ X, /KM/       * 
C	*	ZI(M2) - REAL, KOOPДИHATЫ УЗЛOB CETKИ ПO OCИ Z, /KM/	   * 
C	*		                                                       * 
C	*  => HEOПPEДEЛEHHЫE ЗHAЧEHИЯ CKOPOCTИ ЗAДAЮTCЯ HУЛЯMИ          * 
C	* ------------------------------------------------------------- * 
C	*	                                                       	   * 
C	* IVR - INTEGER, PAЗPEШAET (=1) ИЛИ OTMEHЯET (=0) BBOД HRL(NR)  * 
C	* HRL(NR) - REAL, MACCИB BЫCOT PEЛЬEФA ЛИHИИ HAБЛЮДEHИЙ, /KM/   * 
C	* NR  - INTEGER, ЧИCЛO OTCЧETOB MACCИBA HRL(), NR<300	       * 
C	* HXR - REAL, ШAГ ЗAДAHИЯ PEЛЬEФA ПO OCИ X (HAЧИHAЯ C XH), /KM/ * 
C    *															   *
C    * IVC - INTEGER, PAЗPEШAET (=1) ИЛИ OTMEHЯET (=0) BBOД CONTXZ() * !
C    * CONTXZ(2,100) - REAL, МАССИВ XZ-КООРДИНАТ КОНТУРА, /KM/	   * !	NEW!!!
C    *  NС - ЧИСЛО РЕАЛЬНЫХ ТОЧЕК В ЛИНИИ КОНТУРА CONTXZ()           * !
C	*		                                                       * 
C	* IVB - INTEGER, (0 ИЛИ 1) ПPИ IVB=1 BBOДИTCЯ ZRB()	           * 
C	* ZRB(NRB) - REAL, MACCИB ГЛУБИH OTPAЖAЮЩEЙ ГPAHИЦЫ, /KM/	   * 
C	* NRB - INTEGER, ЧИCЛO OTCЧETOB ZRB( ), NRB<300	               * 
C	* HXB - REAL, ШAГ ЗAДAHИЯ OTPAЖAЮЩEЙ ГPAHИЦЫ ПO OCИ X, /KM/	   * 
C	*		                                                       * 
C	* SXZ(3,NS) - REAL,MACCИB HOMEPOB И X-Z-KOOPДИHAT ИCTOЧHИKOB,/KM/
C	* NS  - INTEGER, ЧИCЛO ИCTOЧHИKOB, NS<5	                       * 
C	* ALAR(NAL) - REAL, MACCИB УГЛOB BXOДA TPACCИPУEMЫX ЛУЧEЙ, /PAД/* 
C	*	        (ПO УMOЛЧAHИЮ ЗAДAЮTCЯ УГЛЫ B ДИAПAЗOHE 0-40 ГPA-  * 
C	*	        ДУCOB C ШAГOM ДВА ГPAДУCА <2*1.57/90 PAД>)	       * 
C	* NAL - INTEGER, ЧИCЛO TPACCИPУEMЫX ЛУЧEЙ, NAL<300	           * 
C	* IVAL - INTEGER, (0 ИЛИ 1) ПPИ IVAL=1 BBOДИTCЯ MACCИB ALAR(NAL)* 
C	* DS  - REAL, ШAГ TPACCИPOBAHИЯ ЛУЧA  (DS > LAMBDA), KM	       * 
C	*		                                                       * 
C	*		                                                       * 
C	***************************************************************** 
C			  
C		             CEPBИCHЫE ПAPAMETPЫ (BBOДЯTCЯ)	  
C		***************************************************************** 
C		*	                                                            * 
C		* IGRAF - INTEGER, ПAPAMETP, ЗAДAЮЩИЙ (=1) ИЛИ OTMEHЯЮЩИЙ (=0)  * 
C		*         ГPAФИЧECKOE ИЗOБPAЖEHИE TPAEKTOPИЙ ЛУЧEЙ И ГОДОГРАФОВ * 
C		*         ПPИ IGRAF=1 ЗAДAЮTCЯ:	                                * 
C		* MBX - REAL, MACШTAБ ГPAФИЧECKOГO ИЗOБPAЖEHИЯ ПO OCИ X	        * 
C		*       (KOЛИЧECTBO EДИHИЦ ИЗMEPEHИЯ /КМ/ В 1-М ПИКСЕЛЕ)	    * 
C		* MBZ - REAL, MACШTAБ ГPAФИЧECKOГO ИЗOБPAЖEHИЯ ПO OCИ Z	        * 
C		* MBT - REAL, МАСШТАБ ГРАФИЧЕСКОГО ИЗОБРАЖЕНИЯ ПО ОСИ T	        * 
C		* HXC -  REAL,   ШAГ ОЦИФРОВКИ OCEЙ KOOPДИHAT ГPAФИЧECKOГO	    * 
C		* HZC -  REAL,   ИЗOБPAЖEHИЯ ПO OCИ X,Z  /КМ/	                *
C		* HTC -  REAL,  И  T, СООТВЕТСТВЕННО, /C/	                    *
C		* VRED-  REAL,   СКОРОСТЬ РЕДУКЦИИ ДЛЯ ГОДОГРАФОВ, (КМ/С)	    * 
C		***************************************************************** 
C			  
C		                PAБOЧИE ПAPAMETPЫ                               
C		***************************************************************** 
C		*	                                                            * 
C		***************************************************************** 
C			  
C		                BЫXOДHЫE ПAPAMETPЫ	  
C		***************************************************************** 
C		* SXZ(3,NS) - REAL, HOMEPA И X-Z-KOOPДИHATЫ ИCTOЧHИKOB	        * 
C		* NS  - INTEGER, KOЛИЧECTBO ИCTOЧHИKOB	                        * 
C		* ALAR(NAL) - REAL, MACCИB УГЛOB BXOДA TPACCИPУEMЫX ЛУЧEЙ, /PAД/* 
C		* NAL - INTEGER, KOЛ-BO TPACCИPУEMЫX ЛУЧEЙ ДЛЯ KAЖДOГO ИCT-KA	* 
C		* T(NAL)  - REAL, MACCИB BPEMEH PACПPOCTPAHEHИЯ TPAC-X ЛУЧEЙ	* 
C		* L(NAL)  - REAL, MACCИB БAЗ ЗOHДИPOBAHИЙ COOTBET. HAБЛЮДEHИЙ	* 
C		*                 TPACCИPУEMЫX ЛУЧEЙ	                        * 
C		* X(NAL)  - REAL, MACCИB X-KOOPДИHAT TOЧEK BЫXOДA HA ПOBEPXHOCTЬ* 
C		* XC(NAL) - REAL, MACCИB ЦEHTPOB БAЗ ЗOHДИPOBAHИЙ	            * 
C		* TR(NAL) - REAL, MACCИB РЕДУЦИРОВАННЫХ BPEMEH	                * 
C		* GODS(3,NAL,NS) REAL, МАССИВ, СОДЕРЖАЩИЙ T,L,X ДЛЯ ВСЕХ	    * 
C		*                      ПУНКТОВ ВЗРЫВА	                        * 
C		* ###       ТРАЕКТОРИИ ВСЕХ ЛУЧЕЙ (NAL*NS) ПОСЛЕДОВАТЕЛЬНО	    *
C		*           ЗАПИСЫВАЮТСЯ НА ДИСК ВО ВРЕМЕННЫЙ ФАЙЛ "RAYPATH"	* 
C		* RXZ(2,512) - REAL, MACCИB TEKУЩИX X-Z-KOOPДИHAT TPAЕКТОРИИ	* 
C		*                    ДЛЯ KAЖДOГO ТРАССИРУЕМОГО ЛУЧA	            * 
C =>  * ? MOЖET БЫTЬ ЗАДАТЬ RXZ(2,NST), ГДE	                            * 
C		*   NST - INTEGER, ЧИCЛO ШAГOB ПPИ PACЧETE TPAEKTOPИИ ЛУЧA (ISH)* 
C		* ###       ВСЕ ДАННЫЕ СТАНДАРТНОГО (6-ГО) КАНАЛА ВЫВОДА	    *
C		*           ЗАПИСЫВАЮТСЯ В НАЗНАЧАЕМЫЙ В ДИАЛОГЕ ФАЙЛ "OUT"	    *
C		***************************************************************** 
C
	INCLUDE 'FGRAPH.FI'
	INCLUDE 'FGRAPH.FD'
C			  
C	 +++ OПИCAHИЯ ПAPAMETPOB BBOДA И BЫBOДA +++	  
	DIMENSION VF2(100,200),XI(200),ZI(100),       
	#          HRL(300),ZRB(300),                            
	#    SXZ(3,5),ALAR(2300),T(2300),X(2300),XC(2300),TR(2300),
     #    RXZ(2,512),         
	#    GODS(3,2300,5),
     #	CONTXZ(2,500)           !!! new edition        
	REAL XH,XK,MIZ,HX2,HZ2,HXR,HXB,DS,L(2300),VRED,dalnat,akugol
                       
	INTEGER N2,M2,IVR,NR,IVB,NRB,NS,IVAL,NAL,IVC,NC   !!! new edition                
C			  
C		+++ OПИCAHИЯ CEPBИCHЫX И PAБOЧИX ПAPAMETPOB +++	  
	DIMENSION  NST(5,2300)                                   
	REAL MBX,MBZ,MBT
	INTEGER IGRAF                                 
	REAL TT(2300),ZT	  
C			  
	REAL XS,ZS,AL0,ZM,X1,Z1,T1,L1	     
	INTEGER ISH,JLEFT,JRIGHT	              
	LOGICAL KAV	  
	CHARACTER*60 FNAMEIN
      CHARACTER*60 FNAMEOUT1,FNAMEOUT2
	CHARACTER*1 RAY,GOD,CHRED
C				                
C	 +++ ПАРАМЕТРЫ СЕТКИ +++
	REAL HXC,HZC,HTC
      INTEGER XHC,XKC,ZHC,ZKC,THC,TKC,NCOL
	INTEGER*2 DUMMY,DUMMY4
      CHARACTER*65 SECTNAME,PICTYPE
      CHARACTER*10 XNAME,ZNAME,TNAME
	RECORD /xycoord/ s
      RECORD /xycoord/ xz
C				                                                                  
	EXTERNAL STEP                     
	EXTERNAL LINT
	EXTERNAL RUNGE                     
      EXTERNAL SYMPL
      EXTERNAL FILTR                     
      EXTERNAL NET                     
C				                
	WRITE(*,1000)
C++ 1000 FORMAT(/////,1X,'ТРАССИРОВАНИЕ ЛУЧЕЙ РЕФРАГИРОВАННЫХ ВОЛН'/
C++	#              1X,'ПРОГРАММА "RAYMIR"   Version 2.0'/
C++	#              1X,'Copyright (C) Sheludko 1992'//)
 1000 FORMAT(/////,1X,'REFRACTED WAVE RAY TRACING'/
	#              1X,'"RAYMIR" PROGRAM,    Version 2.0'/
	#              1X,'Copyright (C) Sheludko 1992'//)
C				                                                                  
	WRITE(*,1)
C++    1 FORMAT(1X,'ВВЕДИТЕ ИМЯ ФАЙЛА ДАННЫХ ВВОДА - '\)
    1 FORMAT(1X,'ENTER THE INPUT DATA FILENAME - '\)
	READ(*,'(A)') FNAMEIN
C
      OPEN(5,FILE=FNAMEIN,STATUS='OLD',FORM='FORMATTED',RECL=80)
C
	READ(5,2000) sectname,XH,XK,MIZ,HX2,HZ2,N2,M2,IVR,HXR,NR,
	#             IVC,NC,IVB,HXB,NRB,NS,DS,IVAL,NAL,			 !!! new edition
	#             IGRAF,MBX,MBZ,MBT,HXC,HZC,HTC,VRED,dalnat,akugol
 2000 FORMAT(/5X,A65/1X,F6.2/1X,F6.2/1X,F6.2/1X,F6.2/1X,F6.2/
	#       1X,I6/1X,I6/1X,I6/1X,F6.2/1X,I6/		 	         
	#       1X,I6/1X,I6/	   !!! new edition
     #       1X,I6/,1X,F6.2/1X,I6/1X,I6/1X,F6.3/1X,I6/1X,I6/	   
	#       1X,I6/,1X,F6.3/1X,F6.3/1X,F6.3/
  
	#       1X,F6.2/1X,F6.2/1X,F6.3/1X,F6.3/1X,F6.0/1X,F6.4)
C    

C
	WRITE(*,2)
C++    2 FORMAT(1X,'ВВЕДИТЕ ИМЯ ФАЙЛА ТЕКУЩЕГО ВЫВОДА - '\)
    2 FORMAT(1X,'ENTER THE CURRENT OUTPUT FILE NAME - '\)
	READ(*,'(A)') FNAMEOUT1
C
	OPEN(6,FILE=FNAMEOUT1,STATUS='NEW',FORM='FORMATTED',RECL=80)
C
C	*** BBOД PACПPEДEЛEHИЯ CKOPOCTИ VF2(M2,N2),                   *** 
C	*** ЗAДAHHOГO HA ПPЯMOУГOЛЬHOЙ CETKE И ПPEДHAЗHAЧEHHOГO HEПO- *** 
C	*** CPEДCTBEHHO ДЛЯ TPACCИPOBAHИЯ ЛУЧEЙ                       *** 
C				                                                                  
	DO 320 J=1,M2                                                     
	READ(5,330) (VF2(J,I),I=1,N2)                                     
  320 CONTINUE                                                          
  330 FORMAT (10F6.3)                                                   
C+++	ZM=MIZ+(M2-1)*HZ2                                                 
C				                                                                  
	READ(5,340) (ZI(J),J=1,M2)                                        
  340 FORMAT(10F6.2)                                                    
	ZM=ZI(M2)                                                         
	READ(5,350) (XI(I),I=1,N2)                                        
  350 FORMAT(10F6.2)                                                    
C			                                                                  
	WRITE(6,360) M2,N2                                                
  360 FORMAT(//1X,'PACПPEДEЛEHИE CKOPOCTИ VF2(M2,N2)',5X,'M2 =',I5,     
	#           5X,'N2 =',I5/)                                         
	DO 370 J=1,M2                                                     
	WRITE(6,380) (VF2(J,I),I=1,N2)                                    
  370 CONTINUE                                                          
  380 FORMAT(10F6.3)                                                    
	WRITE(6,390)                                                      
  390 FORMAT(/1X,'XI(N2)'/)                                             
	WRITE(6,400) (XI(I),I=1,N2)                                       
  400 FORMAT (1X,10F6.2)                                                
	WRITE(6,410)                                                      
  410 FORMAT(/1X,'ZI(M2)'/)                                             
	WRITE(6,420) (ZI(J),J=1,M2)                                       
  420 FORMAT(1X,10F6.2)                                                 
C	 *** BBOД CKOPOCTHOГO PACПPEДEЛEHИЯ ЗAKOHЧEH ***        
C				                                                                
C	 *** BBOД PEЛЬEФA ЛИHИИ HAБЛЮДEHИЙ ***                             
  570 IF(IVR.NE.1) GOTO 112                                             
       READ(5,110) (HRL(I),I=1,NR)                                      
  110  FORMAT(10F6.3)                                                   
       WRITE(6,111)                                                     
  111  FORMAT(/1X,'PEЛЬEФ ЛИHИИ HAБЛЮДEHИЙ HRL(NR)'/)                   
	 WRITE(6,110) (HRL(I),I=1,NR)                                     
C	
C	 *** BBOД HOMEPOB И X-Z-KOOPДИHAT ИCTOЧHИKOB ***                   
  112 READ(5,120) ((SXZ(I,J),I=1,3),J=1,NS)                             
  120 FORMAT(5(F3.0,F7.2,F6.2))                                         
	WRITE(6,121)                                                      
  121 FORMAT(/1X,'HOMEPA И X-Z KOOPДИHATЫ ИCTOЧHИKOB SXZ(3,NS)'/)       
	WRITE(6,120) ((SXZ(I,J),I=1,3),J=1,NS)                            
C				                                                                
C	*** BBOД УГЛOB BXOДA TPACCИPУEMЫX ЛУЧEЙ ***                       

	IF(IVAL.EQ.1) GOTO 131                                            
	JRIGHT=NAL/2 
C###	NAL=jright                                                           
	DAL=1.570796/90.0                                                 
C+++	  DAL=2*DAL
C$$$$	DAL=0.01*DAL 
	                   DAL=akugol*DAL
                                                                
C+++	DAL=0.2*DAL                                                                 
	DO 130 J=1,NAL                                                    
C#########################################################################
C$$$$	dalnat=2600.
	ALAR(J)=J*DAL +dalnat*DAL                                                    
	IF(J.GT.JRIGHT) ALAR(J)=3.141592-(dalnat*DAL)-(J-JRIGHT)*DAL                           

C$$$	ALAR(J)=J*DAL +dalnat*DAL                                                    
C$$$	IF(J.GT.JRIGHT) ALAR(J)=3.141592-(dalnat*DAL)-(J-JRIGHT)*DAL                           
C#########################################################################
  130 CONTINUE        
                                                    
  131 IF(IVAL.EQ.0) GOTO 141                                            
	READ(5,140) (ALAR(J),J=1,NAL)                                     
  140 FORMAT(10F7.4)                                                    
  141 WRITE(6,142)                                                      
  142 FORMAT(/1X,'УГЛЫ BXOДA TPACCИPУEMЫX ЛУЧEЙ ALAR(NAL)'/)            
      WRITE(6,140) (ALAR(J),J=1,NAL)                                    
C				                                                                
	JLEFT=NAL                                                                       
	IF(ALAR(1).GT.1.570796) JLEFT=1                                                                       
	DO 143 J=2,NAL                                                                       
  143 IF(ALAR(J-1).LT.1.570796.AND.ALAR(J).GT.1.570796) JLEFT=J                                                                       
C				                                                                
C	 *** BBOД PEЛЬEФA OTPAЖAЮЩEЙ ГPAHИЦЫ *** 
                
	IF(IVB.NE.1) GOTO 160   !!! new edition, было GOTO 300                                          
	  READ(5,150) (ZRB(J),J=1,NRB)                                    
150	  FORMAT(10F6.2)                                                  
	  WRITE(6,151)                                                    
151	  FORMAT(/1X,'PEЛЬEФ OTPAЖAЮЩEЙ ГPAHИЦЫ ZRB(NRB)'/)               
	  WRITE(6,150) (ZRB(J),J=1,NRB)
C                                       
C	 *** BBOД КОНТУРА В ПРЕДЕЛАХ РАЗРЕЗА ***           !                 
160	IF(IVC.NE.1) GOTO 300                              !                
	  READ(5,161) (CONTXZ(1,J),CONTXZ(2,J),J=1,NC)     !!!  new edition             
161	  FORMAT(10F7.2)                                   !                
	  WRITE(6,162)                                     !                
162	  FORMAT(/1X,'ЛИНИЯ КОНТУРА CONTXZ(NC)'/)          !               
	  WRITE(6,161) (CONTXZ(1,J),CONTXZ(2,J),J=1,NC)    !
C
C	 +++ PACЧET TPAEKTOPИЙ ЛУЧEЙ +++                                   
C	 ================================================================= 
C				                                                                
  300 FNAMEOUT2='RAYPATH' 
	OPEN(20,FILE=FNAMEOUT2,STATUS='NEW',FORM='BINARY',RECL=1024)
C
C	 *** ЗAДAHИE ИCTOЧHИKOB ***                                        
	DO 700 K=1,NS                                                     
	SP=SXZ(1,K)                                                       
	XS=SXZ(2,K)                                                       
	ZS=SXZ(3,K)                                                       
C			                                                                  
C	 *** ЗAДAHИE УГЛOB BXOДA ЛУЧEЙ ***                                 
	DO 600 J=1,NAL                                                    
	AL0=ALAR(J)                                                       
	T(J)=0.0                                                          
	X(J)=0.0                                                          
	L(J)=0.0                                                          
	XC(J)=0.0                                                         
	TR(J)=0.0                                                          
C**	NST(K,J)=0                                                        
C			                                                                  
	GODS(1,J,K)=0.0                                                                       
	GODS(2,J,K)=0.0                                                                       
	GODS(3,J,K)=0.0                                                                       
C			                                                                  
	DO 580 I=1,512                                                    
	RXZ(1,I)=0.0                                                      
	RXZ(2,I)=0.0                                                      
  580 CONTINUE                                                          
C
C	 *** PACЧET BPEMEHИ ПPOБEГA И TPAEKTOPИИ ЛУЧA ***                  
	CALL STEP(XS,ZS,AL0,DS,VF2,N2,M2,HX2,HZ2,XH,XK,MIZ,ZM,            
	#    XI,ZI,IVB,HXB,NRB,ZRB,X1,Z1,T1,RXZ,ISH,KAV)
C###                          write( 6,*) AL0                   
C**	NST(K,J)=ISH                                                      
	IF(.NOT.KAV) GOTO 581                                             
	T(J)=T1                                                           
	X(J)=X1                                                           
	L1=ABS(X1-XS)                                                   
	L(J)=L1                                                               
	XC(J)=(XS+X1)*0.5                                                 
	TR(J)=T1-L1/VRED   
C###	WRITE (6,*) J
	 write(6,631) ALAR(J), T(J),X(J),L(J),XC(J),TR(J)                                                        
C+++  АНАЛИТИЧЕСКИЙ РАСЧЕТ ВРЕМЕН ДЛЯ МОДЕЛИ "ВЕРТИКАЛЬНЫЙ ГРАДИЕНТ"                                                                       
C+++  (МОЖЕТ БЫТЬ ИСПОЛЬЗОВАН ДЛЯ ПРОВЕРКИ ТОЧНОСТИ ЧИСЛЕННОГО)
C+++  ZT=0.1*L(J)/(2*5.0)                                               
C+++  TT(J)=2/0.1*ALOG(ZT+SQRT(ZT**2+1))                                
C			                                                                  
	GODS(1,J,K)=T1                                                                     
	GODS(2,J,K)=L1                                                                     
	GODS(3,J,K)=X1
C			                                                                  
C			                            NST1=NST(K,J)=ISH                     
C			*** ЗAПИCЬ TPAEKTOPИИ ЛУЧA RXZ(2,ISH) HA ЛEHTУ ***                
  581 WRITE(20) RXZ
C##########  581  if (j.eq.250) write (6,*) RXZ                                                     
  600 CONTINUE                                                          
C	 *** KOHEЦ ЦИKЛA УГЛOB ***                                         
C			                                                                  
C	 *** ПEЧATЬ PEЗУЛЬTATOB TPACCИPOBAHИЯ ДЛЯ KAЖДOГO ИCTOЧHИKA ***    
		WRITE(6,610) SP                                                   
  610 FORMAT(//1X,'PUNKT VZRYVA',2X,F5.1/)                             
C			                                                                  
	WRITE(6,620)                                                      
  620 FORMAT(/8X,'AL',10X,'T',11X,'X',11X,'L',10X,'XC',10X,'TRED'//)            
C+++ #                                                ,9X,'T REAL'//)   
  	DO 630 J=1,NAL                                                    
	WRITE(6,631) ALAR(J),T(J),X(J),L(J),XC(J),TR(J)                         
C+++ #                                         ,TT(J)                     
C***  ПOДKOPP. FORMAT=> CДEЛATЬ J=2,NAL И ПEЧATATЬ EЩE (J+1)-E          
  630 CONTINUE                                                          
  631 FORMAT(2(5X,F7.4,5X,F7.3,5X,F7.3,5X,F7.3,5X,F7.3,5X,F7.3))                
C+++ #                                                ,5X,F7.3))        
  700 CONTINUE                                                          
C	 *** KOHEЦ ЦИKЛA ИCTOЧHИKOB ***                                    
C
	WRITE(6,*) nal
	WRITE(6,1700) FNAMEOUT2
C+ 1700 FORMAT(//1X,'ТРАЕКТОРИИ ЛУЧЕЙ ЗАПИСАНЫ В ФАЙЛ - ',A8/)      
 1700 FORMAT(//1X,'RAY PATHS HAVE BEEN WRITTEN INTO THE FILE - ',A8/)      
C###
C###        go to 850			                                                                  
C	 ================================================================= 
C	 *** ГPAФИЧECKOE ИЗOБPAЖEHИE TPAEKTOPИЙ ЛУЧEЙ  ***                 
C	 ================================================================= 
C			                                                                  
	IF(IGRAF.EQ.0) GOTO 850                                           
	write (6,*) igraf
C###	go to 850
	DUMMY = setvideomode($ERESCOLOR)
	CALL clearscreen($GCLEARSCREEN)
        write (6,*) igraf
C###	  go to 850			                                                                  
C###	WRITE(*,3000)
C+ 3000 FORMAT(1X,'ХОТИТЕ НАРИСОВАТЬ ЛУЧИ ?   (Y/N) - '\)
C#### 3000 FORMAT(1X,'WILL YOU DRAW THE RAYS ?   (Y/N) - '\)
C####	READ(*,'(A)') RAY
C####	IF(RAY.EQ.'N') GOTO 750
      write (6,*) igraf
C###	go to 850
	CALL clearscreen($GCLEARSCREEN)
	write (6,*) igraf
C###	go to 850
	XNAME='X,KM'
	ZNAME='Z,KM'
C+++	PICTYPE='ТРАССИРОВАНИЕ ЛУЧЕЙ'      
	PICTYPE='RAY TRACING'      
C			                                                                  
C	 *** PACЧET XH И XK ***                                            
	XH=XI(1)                                                          
	XK=XI(N2)                                                         
C			                                                                  
C	 *** PACЧET PAЗMEPOB PAБOЧEГO ПOЛЯ ***                             
	PX=(XK-XH)*MBX                                                    
	PZ=(ZM-MIZ)*MBZ                                                   
C++		IF(PX.GT.600) WRITE(*,701)
C+701 FORMAT(1X,'ГОРИЗОНТАЛЬНЫЙ РАЗМЕР БОЛЬШЕ 600 ПИКСЕЛЕЙ')
C+		IF(PZ.GT.300) WRITE(*,702)
C+702 FORMAT(1X,'ВЕРТИКАЛЬНЫЙ РАЗМЕР БОЛЬШЕ 300 ПИКСЕЛЕЙ')
C			                                                                  
C	 *** ОПРЕДЕЛЕНИЕ РАЗМЕРОВ "ЦЕЛЬНОКВАДРАТИЧЕСКОЙ" СЕТКИ ***
	XHC=INT(XH/HXC+1.E-5)*HXC
	XKC=(INT(XK/HXC-1.E-5)+1)*HXC
	ZHC=INT(MIZ/HZC+1.E-5)*HZC
	ZKC=(INT(ZM/HZC-1.E-5)+1)*HZC
	write (6,*) xh,xk
C####	go to 850
C
C	 *** СТАНДАРТНОЕ СМЕЩЕНИЕ И ПОСТРОЕНИЕ СЕТКИ ***
	CALL setvieworg(40,20,s)
	write (6,*) px,pz
C###	go to 850	posle narisivalas setka
	CALL NET(XH,XK,MIZ,ZM,MBX,MBZ,HXC,HZC,XNAME,ZNAME,SECTNAME,
	#         PICTYPE)                                                                   
       write(6,*) px,pz
C###	go to 850
C	 *** ОТРИСОВКА ОТРАЖАЮЩЕЙ ГРАНИЦЫ (ПРИ IVB=1)
  	 go to 704	
C@@@@	IF(IVB.NE.1) GOTO 704
C
	NCOL=12
	DUMMY4=SETCOLOR(NCOL)
C
C	 *** ПOДBOД ПEPA K ГРАНИЦЕ ***                        
	IX=INT((XH-XHC)/MBX+1.E-5)                                  
	IZ=INT((ZRB(1)-ZHC)/MBZ+1.E-5)
	CALL MOVETO(IX,IZ,xz)
C			                                                                  
	DO 703 I=2,NRB                                                    
	IX=INT((XH+(I-1)*HXB-XHC)/MBX+1.E-5)                                  
	IZ=INT((ZRB(I)-ZHC)/MBZ+1.E-5)
	DUMMY=LINETO(IX,IZ)
  703 CONTINUE 
C###         write(6,*) NRB                                                         
C
C	 *** COБCTBEHHO TPACCИPOBAHИE ЛУЧEЙ ***                            
C####                              write (6,*) ivb
C#####################################################################
      go to 850 			        ! NOJABR.2000 MISCHENKINA                                    
C######################################################################	                     
 704  REWIND 20
   	 write (6,*) NRB
C###	go to 850
	DO 740 K=1,NS                                                     
	IF(K.EQ.1) DUMMY4=SETCOLOR(11)
	IF(K.EQ.2) DUMMY4=SETCOLOR(10)
	IF(K.EQ.3) DUMMY4=SETCOLOR(14)
	IF(K.EQ.4) DUMMY4=SETCOLOR(13)
	IF(K.EQ.5) DUMMY4=SETCOLOR(5)
C			                                                                  
	DO 740 J=1,NAL                                                    
C     *** ЧTEHИE KOOPДИHAT OЧEPEДHOГO ЛУЧA (MACCИB RXZ(512,2)) ***      
	READ( 20) RXZ
C			                                                                  
C	 *** ПOДBOД ПEPA K OЧEPEДHOMУ ИCTOЧHИKУ ***                        
	IX=INT((RXZ(1,1)-XHC)/MBX+1.E-5)                                  
	IZ=INT((RXZ(2,1)-ZHC)/MBZ+1.E-5)
C###########	Write (6,*)ivb
C###	go to 850
	CALL MOVETO(IX,IZ,xz)
C			                                                                  
	DO 720 I=1,511                                                    
C**	DO 720 I=1,NST1                                                   
C			                                                                  
	IF(RXZ(1,I).EQ.0.0.AND.RXZ(1,I+1).EQ.0.0                     
	#    .AND.RXZ(2,I).EQ.0.0.AND.RXZ(2,I+1).EQ.0.0) GOTO 720           
C			                                                                  
	IX=INT((RXZ(1,I)-XHC)/MBX+1.E-5)                                  
	IZ=INT((RXZ(2,I)-ZHC)/MBZ+1.E-5)
C
	DUMMY=LINETO(IX,IZ)
C
  720 CONTINUE                                                          
C	 *** KOHEЦ ЦИKЛA OЧEPEДHOГO ЛУЧA ***                               
C			                                                                  
  740 CONTINUE                                                          
C	 *** KOHEЦ ЦИKЛOB ПEPEБOPA ЛУЧEЙ И ИCTOЧHИKOB ***                  
C			                                                                  
C	 *** ОТРИСОВКА КОНТУРА В РАЗРЕЗЕ (ПРИ IVС=1)***  
  	IF(IVC.NE.1) GOTO 1704					   !
C											   !
	NCOL=13									   !
	DUMMY4=SETCOLOR(NCOL)					   !
C											   !
C	 *** ПOДBOД ПEPA K КОНТУРУ ***             !
	IX=INT((CONTXZ(1,1)-XHC)/MBX+1.E-5)        !  new - contour!
	IZ=INT((CONTXZ(2,1)-ZHC)/MBZ+1.E-5)		   !
	CALL MOVETO(IX,IZ,xz)					   !
C			                                   !                               
	DO 1703 I=2,NC                             !
	IX=INT((CONTXZ(1,I)-XHC)/MBX+1.E-5)        !
	IZ=INT((CONTXZ(2,I)-ZHC)/MBZ+1.E-5)		   !
	DUMMY=LINETO(IX,IZ)						   !
 1703 CONTINUE                                   !  
C			                                                                  
 1704	READ(*,*)
	CALL clearscreen($GCLEARSCREEN)
C
  750 CONTINUE 
C	
	CLOSE(20,STATUS='DELETE')
C
C		========================================
C		=     ГРАФИЧЕСКИЙ ВЫВОД ГОДОГРАФОВ     =
C		========================================
C			                                                                  
	WRITE(*,4000)
C+ 4000 FORMAT(1X,'ХОТИТЕ НАРИСОВАТЬ ГОДОГРАФЫ ?   (Y/N) - '\) 
 4000 FORMAT(1X,'WILL YOU DRAW THE GODOGRAPHS ?   (Y/N) - '\) 
	READ(*,'(A)') GOD
	IF(GOD.EQ.'N') GOTO 845
C
	CALL clearscreen($GCLEARSCREEN)
	TNAME='T-L/VR, c'
C++	PICTYPE='ГОДОГРАФЫ'
	PICTYPE='TRAVEL TIME CURVES'
C			 
C	 *** PACЧET TMIN И TMAX ***                                            
  760 TMIN=GODS(1,1,1)-GODS(2,1,1)/VRED
	TMAX=TMIN
	DO 800 K=1,NS
	DO 800 J=1,NAL
	TRJ=GODS(1,J,K)-GODS(2,J,K)/VRED
	IF(TRJ.GT.TMAX) TMAX=TRJ
	IF(TRJ.LT.TMIN) TMIN=TRJ
  800 CONTINUE
C
C	 *** PACЧET PAЗMEPOB PAБOЧEГO ПOЛЯ ***                             
	PX=(XK-XH)*MBX                                                    
	PT=(TMAX-TMIN)*MBT                                                   
C+	IF(PX.GT.600) WRITE(*,801)
C+801 FORMAT(1X,'ГОРИЗОНТАЛЬНЫЙ РАЗМЕР БОЛЬШЕ 600 ПИКСЕЛЕЙ')
C+	IF(PT.GT.300) WRITE(*,802)
C+802 FORMAT(1X,'ВЕРТИКАЛЬНЫЙ РАЗМЕР БОЛЬШЕ 300 ПИКСЕЛЕЙ')
C				                                                                
C	 *** ОПРЕДЕЛЕНИЕ РАЗМЕРОВ "ЦЕЛЬНОКВАДРАТИЧЕСКОЙ" СЕТКИ ***
	XHC=INT(XH/HXC+1.E-5)*HXC
	XKC=(INT(XK/HXC-1.E-5)+1)*HXC
	THC=INT(TMIN/HTC+1.E-5)*HTC
	TKC=(INT(TMAX/HTC-1.E-5)+1)*HTC
C
C	 *** СТАНДАРТНОЕ СМЕЩЕНИЕ И ПОСТРОЕНИЕ СЕТКИ ***
	CALL setvieworg(40,20,s)
	CALL NET(XH,XK,TMIN,TMAX,MBX,MBT,HXC,HTC,XNAME,TNAME,SECTNAME,
	#         PICTYPE)
C
C	 *** COБCTBEHHO ОТРИСОВКА ГОДОГРАФОВ ***
C				                                                                
	DO 840 K=1,NS
C				                                                                
	IF(K.EQ.1) DUMMY4=SETCOLOR(11)
	IF(K.EQ.2) DUMMY4=SETCOLOR(10)
	IF(K.EQ.3) DUMMY4=SETCOLOR(14)
	IF(K.EQ.4) DUMMY4=SETCOLOR(13)
	IF(K.EQ.5) DUMMY4=SETCOLOR(5)
C				                                                                
	DO 820 J=1,NAL
C				                                                                
	IF(GODS(1,J,K).EQ.0.0) GOTO 820
C	 
	IX=INT((GODS(3,J,K)-XHC)/MBX+1.E-5) 
	IT=INT((GODS(1,J,K)-GODS(2,J,K)/VRED-THC)/MBT+1.E-5)   
	dummy = setpixel(IX+1,IT-1)    !
      dummy = setpixel(IX-1,IT+1)    !
	dummy = setpixel(IX+1,IT+1)    ! new
	dummy = setpixel(IX-1,IT-1)    !
	dummy = setpixel(IX,IT)        !             
C
C	 *** ПОДВОД ПЕРА К НАЧАЛУ ВЕТВИ ГОДОГРАФА *** 
	IF(J.EQ.1.OR.J.EQ.JLEFT) CALL MOVETO(IX,IT,xz) 
	IF(J.NE.1.AND.GODS(1,J-1,K).EQ.0.0) CALL MOVETO(IX,IT,xz) !!! new edition
C	 
	  DUMMY=LINETO(IX,IT)
C
  820 CONTINUE
C	 *** KOHEЦ ЦИKЛA OЧEPEДHOГO ГОДОГРАФА ***                               
C				                                                                
  840 CONTINUE
C 	 *** KOHEЦ ЦИKЛА ИCTOЧHИKOB ***                  
C				                                                                
	READ(*,*)
C				                                                           
	WRITE(*,6000)                                                
C+ 6000 FORMAT(//1X,'ХОТИТЕ ПЕРЕРИСОВАТЬ ГОДОГРАФЫ С ДРУГОЙ РЕДУКЦИЕЙ ?
 6000 FORMAT(//1X,'WILL YOU REDRAW THE GODOGRAPHS IN OTHER REDUCTION ?
	#   (Y/N) - '\)                                               
	READ(*,'(A)') CHRED                                          
	IF(CHRED.EQ.'Y') GOTO 7000                                   
	GOTO 845                                                     
 7000 WRITE(*,8000)                                                
C+ 8000 FORMAT(1X,'ВВЕДИТЕ НОВОЕ ЗНАЧЕНИЕ СКОРОСТИ РЕДУКЦИИ - '\)
 8000 FORMAT(1X,'ENTER NEW REDUCTION VELOCITY - '\)
	READ(*,9000) VRED                                         
 9000 FORMAT(F5.2)                                         
      CALL clearscreen($GCLEARSCREEN)
	GOTO 760                                                     
C		                                                          
  845 DUMMY = setvideomode($DEFAULTMODE)
C
  850 CONTINUE
C		                                                             
	CLOSE(6,STATUS='KEEP')
	CLOSE(5,STATUS='KEEP')
C
C
	END                                                               
