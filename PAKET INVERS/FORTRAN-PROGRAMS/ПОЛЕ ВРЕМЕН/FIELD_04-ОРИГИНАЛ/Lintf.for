C
C     ======================================================
C     =                     LINTF                          =
C     =   ����ணࠬ�� ��㬥୮� �������� ���௮��樨,    =
C     =   �ᯮ��㥬�� �� ����஥���� ���� �६��         =
C     ======================================================
C
C                    �室�� ��ࠬ����
C     *****************************************************************
C     *                                                               *
C     *                                                               *
C     *                                                               *
C     *****************************************************************
C
C                    ��室�� ��ࠬ����
C     *****************************************************************
C     * XMIN,XMAX,TMIN,TMAX,LMIN,LMAX,L0(G),G,                        *
C     * NW(G),XWW(100,G),TWW(100,G) ��� izoline's file                *
C     *                                                               *
C     *                                                               *
C     *****************************************************************
C
      subroutine LINTF(N,NPV,NST,T,L,X,
     &                 IL0AR,G,L0,DL,
     &                 IDXDTAR,DXL,DTL,DX,DT,LJ,DX2,DT2,HX,HX2,
     &                 XMIN,XMAX,TMIN,TMAX,LMIN,LMAX,NW,XWW,TWW)
C
C     *** ���ᠭ�� �室��� ��ࠬ��஢ ***
      integer N
      integer NPV(N),NST(N)
      real L(N),T(N),X(N)
      integer IL0AR,G,IDXDTAR   
      real L0(50),DL,DXL(50),DTL(50),DX,DT,LJ,DX2,DT2,HX,HX2
C
C     *** ���ᠭ�� ��室��� ��ࠬ��஢ ***
      real XMIN,XMAX,TMIN,TMAX,LMIN,LMAX
      integer NW(50)
      dimension XWW(100,50),TWW(100,50)
C
C     *** ���ᠭ�� ࠡ��� ��ࠬ��஢ ***
      integer K,U,U2,E,S,F,W,W1,MET
      integer P(50)
      real XWK(1024),TWK(1024)
C
C     *** ��।������ �࠭�� ������ ������� ���� �६�� ***
      XMIN=X(1)
      XMAX=X(1)
      TMIN=T(1)
      TMAX=T(1)
      LMIN=L(1)
      LMAX=L(1)
      do 10 j=1,N
      if(X(J).lt.XMIN) XMIN=X(J)
      if(X(J).gt.XMAX) XMAX=X(J)
      if(T(J).lt.TMIN) TMIN=T(J)
      if(T(J).gt.TMAX) TMAX=T(J)
      if(L(J).lt.LMIN) LMIN=L(J)
      if(L(J).gt.LMAX) LMAX=L(J)
   10 continue
      XMIN=INT(XMIN/HX)*HX
      XMAX=(INT(XMAX/HX-1.E-5)+1)*HX
      TMIN=INT(TMIN)
      TMAX=INT(TMAX-1.E-5)+1
C     *** ����� ��।������ �࠭�� ������ ������� ���� �६�� ***
C
C     *** ������� ���ᨢ� L0(G), �� �᫮��� IL0AR=0 ***
      if(IL0AR.eq.1) goto 30
      LMIN=(INT(LMIN/DL-1.E-5)+1)*DL
      G=INT((LMAX-LMIN)/DL)+1
      do 20 J=1,G
   20 L0(J)=LMIN+(J-1)*DL
C
C     *** ������� ���ᨢ�� DXL(G),DTL(G) �� IDXDTAR=0 ***
   30 if(IDXDTAR.eq.1) goto 50
      do 40 J=1,G
      DXL(J)=DX
      DTL(J)=DT
      if(L0(J).le.LJ) goto 40
      DXL(J)=DX2
      DTL(J)=DT2
   40 continue
C
C     *** 㯮�冷祭�� ������ �� �����⠭�� L ***
C     *** � ࠧ������� �⮩ ��᫥����⥫쭮�� �� ����ᮢ� ���� ***
   50 G=G+1
      DL=L0(G-1)-L0(G-2)
      L0(G)=L0(G-1)+DL
C
      K=1
      RI=0.0
      P(G-1)=N
      P(G)=N
      do 110 J=1,N-1
      R=L(J)
      J1=J+1
      do 80 I=J1,N
      if(L(I).GE.R) goto 80
      R=L(I)
      M=I
   80 continue
      if(L(J).eq.R) M=J
      C4=L(J)
      L(J)=R
      L(M)=C4
      C5=X(J)
      X(J)=X(M)
      X(M)=C5
      C3=T(J)
      T(J)=T(M)
      T(M)=C3
      C1=NPV(J)
      NPV(J)=NPV(M)
      NPV(M)=C1
      C2=NST(J)
      NST(J)=NST(M)
      NST(M)=C2
   85 if(RI.le.L0(K).and.L0(K).lt.R) goto 90
      goto 100
C     *** ��।������ �࠭�� ����� ***
   90 P(K)=J-1
      K=K+1
      if(L0(K).lt.R) goto 85
  100 if(R.le.L0(K)) RI=R
  110 continue
C     *** ����� 㯮�冷祭�� ������ ***
C
C     === ᮡ�⢥��� �������� ���௮���� ===
C   =                                          =
      U=0           
      W=0
      MET=0
C
C     *** 横� ����஢ �������� ***
      do 300 K=1,G-1
      if(K.le.2) U=0
      if(K.gt.2) U=P(K-2)
      if(K.le.G-2) U2=P(K+2)
      if(K.gt.G-2) U2=P(G)
C      
C     +++ ��⪠ ������ ��� 㬥��襭�� ���ࢠ��� ���௮��樨 ***
  120 E=1
C
C     *** 横� ��ॡ�� �祪 � ���孥� ᤢ������ ����� ***
      do 190 S=P(K)+1,U2
C
C     *** 横� ��ॡ�� �祪 � ������ ����� ***
      do 140 F=U+1,P(K)
      if(abs(X(S)-X(F)).lt.DXL(K).and.abs(T(S)-T(F)).lt.DTL(K)) goto 130
      goto 140
  130 XWK(E)=X(S)-(X(S)-X(F))*(L(S)-L0(K))/(L(S)-L(F))
      TWK(E)=T(S)-(T(S)-T(F))*(L(S)-L0(K))/(L(S)-L(F))
      E=E+1
      MET=1
  140 continue
C
C     +++ ��⮬���᪮� 㢥��祭�� ���ࢠ��� ���௮��樨, +++
C     +++ �᫨ �� ������� �� ����� �窨-���⭥� +++
      if(MET.eq.0) goto 150
      goto 180
  150 do 170 F=U+1,P(K)
      if(abs(X(S)-X(F)).lt.DXL(K)*1.3
     &   .and.abs(T(S)-T(F)).lt.DTL(K)*1.3) goto 160
      goto 170
  160 XWK(E)=X(S)-(X(S)-X(F))*(L(S)-L0(K))/(L(S)-L(F))
      TWK(E)=T(S)-(T(S)-T(F))*(L(S)-L0(K))/(L(S)-L(F))
      E=E+1
      MET=1
  170 continue
C
  180 MET=0
  190 continue
C
      W=E-1
C
C     *** ���室 �� ��⮬���᪮� 㬥��襭�� ���ࢠ��� ���௮��樨, ***
C     *** �᫨ ������⢮ ����祭��� �祪 �ॢ�蠥� ࠧ���             ***
C     *** ��१�ࢨ஢������ ���� E.GT.1024  (E.GT.512)                              ***
      if(E.gt.1024) goto 245		 !!! NEW
C++      if(E.gt.512) goto 245	 !!! OLD
C
C     *** 㯮�冷祭�� ����祭��� �祪 �� �����⠭�� ���न���� x *** 
      do 210 J=1,W
      R=XWK(J)
      J1=J+1
      do 200 I=J1,W
      if(XWK(I).ge.R) goto 200
      R=XWK(I)
      I0=I
  200 continue
      if(XWK(J).eq.R) I0=J
      C=XWK(J)
      XWK(J)=R
      XWK(I0)=C
      C1=TWK(J)
      TWK(J)=TWK(I0)
      TWK(I0)=C1
  210 continue
C
C     *** ��।����� ������ �ᯮ�������� �祪 (< HX2*0.5 ) ***
      W1=W-1
      do  240 J=1,W1
  220 if(abs(XWK(J)-XWK(J+1)).ge.HX2*0.5.or.J.ge.W) goto 240
      TWK(J+1)=(TWK(J)+TWK(J+1))*0.5
      XWK(J+1)=(XWK(J)+XWK(J+1))*0.5
      do 230 J1=J,W-1
      TWK(J1)=TWK(J1+1)
      XWK(J1)=XWK(J1+1)
  230 continue
      W=W-1
      if(J.le.W-1) goto 220
  240 continue
C
C     *** ����஫� ������⢠ ����祭��� �祪              ***
C     *** �᫨ �� ����� 100 (max ���. ࠧ���. ������),   ***
C     *** ��⮬���᪮� 㬥��襭�� ���ࢠ��� ���௮��樨 ***
      if(W.le.100) goto 250
  245 DTL(K)=0.75*DTL(K)
      DXL(K)=0.75*DXL(K)
      goto 120
C
  250 NW(K)=W
C
C     *** ���㫥��� "墮�⮢" ***
      do 260 J1=W+1,100
      TWK(J1)=0.0
      XWK(J1)=0.0
  260 continue
C
C     *** ����뫪� ���祭�� � ��室�� ���ᨢ� XWW(WW,G),TWW(WW,G) ***
      do 290 J=1,100
      if(J.le.W) goto 270
      goto 280 
  270 XWW(J,K)=XWK(J)
      TWW(J,K)=TWK(J)
      goto 290
  280 XWW(J,K)=0.0
      TWW(J,K)=0.0
  290 continue
C
  300 continue
C     *** ����� 横�� ��ॡ�� �������� (K) ***
C
      G=G-1
C==
      write(6,304)  
  304 format(/1X,'����� �/� LINTF')
      write(6,305) G
  305 format(1X,'G=',I2)
      write(6,306)       
  306 format(1X,'NW(G)')
      write(6,307) (NW(J),J=1,G)
  307 format(1X,10I4)
      write(6,2307) (DXL(J),J=1,G)	   !!!NEW
      write(6,2307) (DTL(J),J=1,G)	   !!!NEW
 2307 format(1X,10F7.3)				   !!!NEW
C
      write(6,310)      
  310 format(1X,'XWW(100,G)')   
      do 320 K=1,G
      write(6,325) (XWW(J,K),J=1,NW(K))
  320 continue
  325 format(1X,10F7.1)
      write(6,330)      
  330 format(1X,'TWW(100,G)')   
      do 340 K=1,G
      write(6,345) (TWW(J,K),J=1,NW(K))
  340 continue
  345 format(1X,10F7.2)
      write(6,350)  
  350 format(1X,'����� ���� �/� LINTF'/)
C==
      return
      end
