C                           11.03.92.
C     =================================================================
C     =                                                               = 
C     =    DRAWING THE NET OF COORDINATE SYSTEM                       =
C     =================================================================
C
      INCLUDE 'FGRAPH.FI'
      SUBROUTINE NET(XH,XK,YH,YK,HX2,HY2,HXC,HYC,xname,yname,sectname,
     #               pictype)
C
      INCLUDE 'FGRAPH.FD'
C
      REAL XH,XK,YH,YK,HX2,HY2,HXC,HYC
      INTEGER XHC,XKC,YHC,YKC,HXI,HYI
      INTEGER NIC,MIC,N,M
      INTEGER*2 dummy,dummy2
      INTEGER ncol
      INTEGER*2 rows
      RECORD /xycoord/ xy
C++      RECORD /rccoord/ shell
      TYPE (rccoord) shell
      CHARACTER*10 xname,yname
      CHARACTER*65 sectname,pictype
C
C --> CHARACTER*5 figx
      CHARACTER*4 figx
      CHARACTER*4 figy
C							
      rows=settextrows(25)
C
C     ***  ��p������ p����p� "楫쭮����p��筮�" �⪨  ***
C     ***  �p����� �⪨ � ��⨭�� ���p������  ***
      XHC = INT(XH/HXC+1.E-5)*HXC
      XKC = (INT(XK/HXC-1.E-5)+1)*HXC
      YHC = INT(YH/HYC+1.E-5)*HYC
      YKC = (INT(YK/HYC-1.E-5)+1)*HYC
C     ***  �����p� �⪨ � ���ᥫ�� ***
      NIC = INT((XKC-XHC)/HX2+1.E-5)
      MIC = INT((YKC-YHC)/HY2+1.E-5)
C
C     ***  ��� �⪨ � ���ᥫ��  ***
      HXI = INT(HXC/HX2+1.E-5)
      HYI = INT(HYC/HY2+1.E-5)
C     ***   ��᫮ ����� �⪨  ***
      N = NIC/HXI+1
      M = MIC/HYI+1
C
C     *** ��p�������� ᤢ��� �p�� ����p������ �⭮�⥫쭮  ***
C     *** ��砫� �⪨ (� ���ᥫ��)                         ***
      IDX=INT((XH-XHC)/HX2+1.E-5)
      IDY=INT((YH-YHC)/HY2+1.E-5)
C
      dummy2 = settextcolor(15)
      ncol=15
      dummy = setcolor(ncol)
C
C     ***  ���㥬 ��� ***
      DO 10 I=1,N
C     IX = XHI+(I-1)*HXI
      IX = (I-1)*HXI
C     IY1= YHI-1
      IY1= 0
C     IY2 = YHI+MIC-1
      IY2 = MIC-1
      CALL moveto(IX,IY1,xy)      
      dummy = lineto(IX,IY2)
   10 CONTINUE
C
      DO 20 J=1,M
C     IY = YHI+(J-1)*HYI
      IY = (J-1)*HYI
C     IX1= XHI-1
      IX1= 0
C     IX2 = XHI+NIC-1
      IX2 = NIC-1
      CALL moveto(IX1,IY,xy)
      dummy = lineto(IX2,IY)
   20 CONTINUE
C
      CALL settextposition(1,(40+IX2-IX1)/8+6,shell)
      CALL outtext(xname)
      CALL settextposition((20+IY2-IY1)/14+2,1,shell)
      CALL outtext(yname)
C     ***  ���p��뢠�� ��� ***
      DO 40 I=1,N
      IX=40+(I-1)*HXI
      IXT=IX/8-1
      XC=XHC+HXC*(I-1)
     	XC=XC*1000.							  !!!NEW
      IXC=INT(XC+1.E-5)
      CALL settextposition(1,IXT,shell)
C++      WRITE(*,30) XC
C++   30 FORMAT(F5.1\)   
C --> WRITE(figx,'(F5.1)') XC
      WRITE(figx,'(I4)') IXC
      CALL outtext(figx)
   40 CONTINUE
C
      DO 60 J=1,M
      IY=20+(J-1)*HYI
      IYT=IY/14+1
      YC=YHC+HYC*(J-1)
	YC=YC*1000.							   !!!NEW
      IYC=INT(YC+1.E-5)					   !!!NEW
      CALL settextposition(IYT,1,shell)
C++      WRITE(*,50) YC
C++   50 FORMAT(F4.1\)
C --> WRITE(figy,'(F4.1)') YC					  !!!OLD
      WRITE(figy,'(I4)') IYC				   !!!NEW
      CALL outtext(figy)
   60 CONTINUE
C
C     *** �������� p��p��� ***
C++   ncol=15
C++   dummy2 = settextcolor(ncol)
C+++      CALL settextposition(23,10,shell)
      CALL settextposition(19,10,shell)
      CALL outtext(pictype)
C+++      CALL settextposition(24,10,shell)
      CALL settextposition(20,10,shell)
      CALL outtext(sectname)
C
C
      RETURN
      END
