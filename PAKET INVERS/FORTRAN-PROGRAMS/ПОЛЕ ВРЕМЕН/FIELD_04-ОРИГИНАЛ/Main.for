$LARGE
C
C     ======================================================
C     =                   TFIELD                           =
C     =  �ணࠬ�� ����஥��� ��㬥୮�� ���� �६��       =
C     =  t(x,l) � ��������� l=const �� �᭮�� ������䮢   =
C     =  �५�������� ����                                 =
C     ======================================================
C
C     ������⮬ ࠡ��� �ணࠬ�� ����� ����᪮� 
C     ����ࠦ���� ���� �६�� �� ��࠭� ������ � 䠩�
C     �᫥���� ������, ᮤ�ঠ騩 �������⠭�� ������
C     ���祭�� �������� ���� �६�� ��ନ஢���� � ����,
C     �ਣ����� ��� ����� � �ணࠬ��  DEAD6 (INVERS Version 6.0)
C
C     ��� �ணࠬ��஢���� FORTRAN-Microsoft Version 5.0.
C     ���饭�� � ���譨� ������⥪� ����᪮�� ���� C:\FONT\helvb.fon.
C     ��� �뢮�� ����砥���� ����᪮�� ����ࠦ���� �� �����
C     ����室��� �ᯮ�짮���� �����-���� �⠭����� ����᪨�
C     �����, ���ਬ��, PIZAZ-2.
C
C     ���ࠡ��稪 ����쪮 �.�.
C     �������� ��㡨���� ᥩᬨ�᪨� ��᫥�������
C     ������� ���䨧��� �� ���
C     ����ᨡ���, 630090, �����
C
C     ======================================================
C
C                        ������� ������
C     ******************************************************************  
C     *   PROFILE - character*65, �������� ��䨫�                     *
C     *         N - integer, �᫮ ������� (�����஢����)           *
C     *             ���ᨢ ������ �����஢����:                        *
C     *             [NPV(N),NST(N),T(N),L(N),X(N)]                     *
C     *             NPV(N) - integer, ����� �㭪⮢ ���뢠            *
C     *             NST(N) - integer, ����� �⠭権 �ਥ��            *
C     *             T(N) - real, �६��� �஡��� �����, /c/            *
C     *             L(N) - real, ���� �����஢����, /��/               *
C     *             X(N) - real, ���न���� 業�஢ �����஢����, /��/ *
C     *       KPV - integer, ������⢮ �㭪⮢ ���뢠                 *            
C     *  MPV(KPV) - integer, ���祭� ����஢ �㭪⮢ ���뢠           *
C     *       KST - integer, ������⢮ �⠭権 �ਥ��                 *
C     *  MST(KST) - integer, ���祭� ����஢ �⠭権 �ਥ��           *
C     *  XPV(KPV) - real, x-���न���� �㭪⮢ ���뢠 �� ��䨫�, /��/ *     
C     *  XST(KST) - real, x-���न���� �⠭権 �ਥ�� �� ��䨫�, /��/ *
C     *  HPV(KPV) - real, ����� ५�� �� �㭪�� ���뢠, /��/       *
C     *  HST(KST) - real, ����� ५�� �� �⠭��� �ਥ��, /��/      *
C     *  ------------------------------------------------------------- *
C     *             �������⥫쭠� ���ଠ��,                         *
C     *             ����室���� �� �������� ���ࠢ��                  *
C     *             �� ��஢���� ५�� � ᪮����                 * 
C     *             ������த���� ���孥� ��� ࠧ१�               *
C     * 1).                                                            *
C     *        VV - real, ��।������ ���祭�� ᪮��� �� �����孮�� *
C     *                   ������� ��� �ᥣ� ��䨫�, ��/�           *
C     *        VK - real, ���祭�� �࠭�筮� ᪮��� ��� �����ண�   *
C     *                   "��।�������" �५�����饣� ��ਧ��� �     *
C     *                   �।��� ��� ������ ����, ��/�              *
C     *        VM - real, �।��� ���祭�� �࠭�筮� ᪮��� ��       *
C     *                   �����孮�� ���⨨, ��/�                     *
C     *      ZMIN - real, ��᮫�⭠� �⬥⪠ ��ਧ��⠫쭮�� �஢��    *
C     *                   �ਢ������ ������, /��/                      *
C     * 2).                                                            *
C     * VVPV(KPV) - real, ᪮��� �� �����孮�� ������� � �㭪�� *
C     *                   ���뢠, ��/�                                 *     
C     * VVST(KST) - real, ᪮��� �� �����孮�� ������� � �㭪�� *
C     *                   �ਥ��, ��/�                                 *          
C     *        VF - real, ��।������ ᪮���� �� �����孮��          *
C     *                   "�㭤�����" (����袥 �ᠤ���), �ᯮ��㥬�� *
C     *                   ��� �ਡ��������� ���� ���ࠢ��, ��/�     * 
C     * HFPV(KPV) - real, ��᮫��� �⬥⪨ ५�� "�㭤�����",     *
C     *                   � �㭪�� ���뢠, /��/                       *
C     * HFST(KST) - real, ��᮫��� �⬥⪨ ५�� "�㭤�����"      *
C     *                   (������ �ᠤ���) � �㭪�� �ਥ��, /��/     *
C     * 3).                                                            *
C     * VFPV(KPV) - real, �࠭�筠� ᪮���� ����� �����孮��         *
C     *                    "�㭤�����" � �㭪�� ���뢠, ��/�         *
C     * VFST(KST) - real, �࠭�筠� ᪮���� �� �����孮��            *
C     *                   "�㭤�����" � �㭪�� �ਥ��, ��/�          *
C     *ZMIPV(KPV) - real, ��᮫��� �⬥⪨ ����� �ਢ������ ������   *
C     *                   � �㭪�� ���뢠, /��/                       *     
C     *ZMIST(KST) - real, ��᮫��� �⬥⪨ ����� �ਢ������ ������   *
C     *                   � �㭪�� �ਥ��, /��/                       *          
C     *                                                                *
C     *VGL(2,KVG) - real, �࠭��� ᪮��� �� �ਨ ��������        *
C     *                   �᫮���� �५������� �࠭�� � ������ ��� � *
C     *   L <-> VG        ���ࢠ�� 㤠����� �� ���筨��, �� ������  *
C     *             ᮮ⢥�����騥 �५������� ��������, (��-��/�) *
C     *       KVG - integer, �᫮ ⠪�� ���ࢠ���                    *
C     *                                                                *
C     *      #####  � 㢥��祭��� ����� �ᯮ��㥬�� ��㯯� ������    *
C     *      #####  �����⠥� �஢��� ���४⭮�� �������� ���ࠢ��  * 
C     * -------------------------------------------------------------- *
C     *             ��ࠬ���� ��ࠡ�⪨ �                              *
C     *             ����஥��� ���� �६��                             *
C     *     L0(G) - real, ���祭� ��� �����஢����, ᮮ⢥��⢥���    *
C     *                   ����� �㦭� ����ந�� �������� ����        *
C     *                   �६��, /��/                                 *
C     *         G - integer, �᫮ �������� ���� �६��                *
C     *        DL - real, 蠣 �������� ���� �६�� �� ��⮬���᪮�  *
C     *                   ��।������ ���ᨢ� L0(G), /��/              *
C     *        DX - real, ���ࢠ� ���᪠ �祪 �� �� x �� ��������  *
C     *                   ���௮��樨, /��/                           *  
C     *        DT - real, ���ࢠ� ���᪠ �祪 �� �� t �� ��������  *
C     *                   ���௮��樨, /�/                            *
C     *        LJ - real, �������� (���� ����.),��᫥ ���ன ��������*
C     *                   ���७�� ���ࢠ�� ���௮��樨, /��/     *
C     *   DX2,DT2 - real, ���७�� ���ࢠ�� ���᪠ �祪           *
C     *    DXL(G) - real, ���ᨢ ���ࢠ��� ���᪠ �祪 �� �� x,     *
C     *                   ᮮ⢥�����騩 ���ᨢ� �᪮��� ��������     *
C     *                   L0(G), /��/                                  *
C     *    DTL(G) - real, ��������� ���ᨢ ���ࢠ��� ���᪠ �祪   *
C     *                   �� �� t, /�/                                *
C     *        HX - real, 蠣 ����⨧�樨 ���� �६�� �� �� x ��   *
C     *                   �ନ஢���� ��室���� 䠩�� �᫥����       *
C     *                   ������, /��/                                 *
C     *       HX2 - real, 蠣 ����⨧�樨 ���� �६�� �� �� x ��   *
C     *                   ����᪮� ���ᮢ�� �������� ���� �६��   *
C     *        R0 - real, ��ࠬ��� ᣫ�������� �� ᯫ���-���பᨬ�樨*
C     *                   �������� ���� �६��                         *
C     *     DXMAX - real, ��ࠬ��� �����騩 ���ᨬ��쭮 �����⨬��     *
C     *                   㤠����� �祪 �� ᯫ���-���பᨬ�樨      *
C     *                   (��稭�� � ���ண� �믮������ ���. ���-�)  *
C     *       HXC - real, 蠣 ���஢�� ����. �⪨ �� �� x, /��/    *
C     *       HTC - real, 蠣 ���஢�� ����. �⪨ �� �� t, /ᥪ/   *
C     * -------------------------------------------------------------- *
C     *             ��ࢨ�� ��ࠬ����                                *
C     *             ������� ��ࠬ���� �ࠢ�����                      *
C     *             ������ � �뢮��� (������ �                        *
C     *             ����᪨� ����ࠦ����),                          *
C     *             � ⠪�� ०���� ��ࠡ�⪨                          *
C     *   FNAMEIN - character, ��� 䠩��, ᮤ�ঠ饣� ����� �����     *
C     *   FNAMEOU - character, ��� 䠩�� ������ �஬����筮�� �뢮��  *
C     *  FNAMEOUT - character, ��� 䠩�� ��室��� ������               *
C     *     IVVAR - integer, (=1/0) ����஫���� ���� ���祭�� ᪮���*
C     *             �� �����孮�� �������, �� =1 �������� ���ᨢ� *
C     *             VVPV() � VVST(), �� =0 �������� ������ ���祭�� VV*
C     *     IHFAR - integer, (=1/0) ����஫���� ���� ��㡨� �㭤�����*
C     *             �� =1 �������� ���ᨢ� HFPV() � HFST(), ����     *
C     *             ����� ���                                          *
C     *     IVFAR - integer, (=1/0) ����஫���� ���� ���祭�� ᪮���*
C     *             �� �����孮�� �㭤�����, �� =1 �������� ���ᨢ� *
C     *             VFPV() � VFST(), �� =0 �������� ������ ���祭�� VF*
C     *     IZMAR - integer, (=1/0) ����஫���� ���� ����� �ਢ������ *
C     *             ������, �� =1 �������� ���ᨢ� ZMIPV() � ZMIST(), * 
C     *             �� =0 �������� ����� �஢��� ZMIN                *
C     *      IVGL - integer, (=1/0) ����஫���� ���� �࠭���� ᪮��-*
C     *             ⥩ �� �᫮���� �५������� �࠭��� � ����. ��� *
C     *             �� =1 �������� ���ᨢ VGL(2,KVG), �� =0 ���祭�� *
C     *             VK � VM                                            *
C     *     ICORR - integer, (=1/0) ������ (=1) ��� �⬥��� (=0)      *
C     *             ����  � �������� ���ࠢ��                        *
C     *     IWIDE - integer, (=1/0) ������ (=1) ��� �⬥��� (=0)      *
C     *             "�ப��" ���� �祪 ��� ���௮��樨;            *
C     *             ��ଠ��� ०�� IWIDE=0                           *
C     *    RE10S2 - integer, (=1/2) ������ ���� ���� ���ࠢ��    *
C     *             �� =1 ���ࠢ�� �������� ⮫쪮 �� ५��,         *
C     *             �� =2 ���ࠢ�� �������� � �� ५�� � �� �ᠤ��   *
C     *     IL0AR - integer, (=1/0) ����஫���� ���� L0(G)            *
C     *             �� =1 �������� ���ᨢ L0(G), �� =0 �������� 蠣  *
C     *             �������� DL ��� ��⮬���᪮�� ���� ���ᨢ� L0 *
C     *   IDXDTAR - integer, (=1/0) ����஫���� ���� ���ࢠ��� ���᪠*
C     *             �祪 �� �������� ���௮��樨,                   *
C     *             �� =1 �������� ���ᨢ� DXL(G),DTL(G)              *
C     *             �� =0 - 䨪�஢���� ���祭�� DX,DT,JL,DX2,DT2   *
C     *       IDX - integer, (=1/0) ������ ��� �⬥��� ���� DXMAX,    *
C     *             ����஫���� ०�� ���௮��樨 ����� �窠�� ����.*
C     ******************************************************************
C
C                        �������� ������
C     ******************************************************************
C     * 1. 䠩� �஬����筮� ���� ���� FNAMEOU                    *
C     * 2. 䠩�  �������� ���� �६�� FNAMEOUT, ᪮�������� � ����    *
C     *    �室���� 䠩�� ��� �ணࠬ�� DEAD6 (INVERS Version 6.0)     *
C     ******************************************************************
C
C     MAIN - �������� �ணࠬ��
C
     	USE MSFLIB
c++      INCLUDE 'FGRAPH.FI'
c++      INCLUDE 'FGRAPH.FD'
C                                                                       
C     xxxxxxxx  ���ᠭ�� �室��� ������  xxxxxxxxxxxxxxxxxxxxxxx
C     +++  �᭮���� ���� �室��� ������ +++
      character*65 PROFILE
      integer N
      integer NPV(10000),NST(10000)
      real T(10000),L(10000),X(10000)
      integer KPV,KST,MPV(100),MST(1280)
      dimension XPV(100),XST(1280),HPV(100),HST(1280)
      real VV,VK,VM,ZMIN
C
C     +++  �������⥫쭠� ���ଠ�� ��� �������� ���ࠢ��  +++
      dimension VVPV(100),VVST(1280)
      real VF
      dimension HFPV(100),HFST(1280)
      dimension VFPV(100),VFST(1280)
      dimension ZMIPV(100),ZMIST(1280)
      integer KVG
      dimension VGL(2,10)
C
C     +++  ��ࠬ���� ��ࠡ�⪨ � ����஥��� ���� �६�� +++
      integer G
      real DL
      real L0(50)
      real DX,DT,LJ,DX2,DT2
      dimension DXL(50),DTL(50)
      real HX,HX2,R0
      integer IDX
      real DXMAX
C
C     +++  �ࢨ�� ��ࠬ����  +++
      character*70 FNAMEIN,FNAMEOU,FNAMEOUT  
      integer IVVAR,IHFAR,IVFAR,IZMAR,IVGL,ICORR,IWIDE,RE1OS2
      integer IL0AR,IDXDTAR
C
C     xxxxxxxx  ���ᠭ�� ��室��� ������  xxxxxxxxxxxxxxxxxxxxxxx
      integer N1      
      real X1(200),T1(50,200)
C     xxxxxxxx  ���ᠭ�� �������⥫��� ������ ��� ��室���� 䠩�� xxx
      real HX1,HZ1,MBZ,DXF,DZF
C
C     xxxxxxxx  ���ᠭ�� ࠡ��� ��ࠬ��஢  xxxxxxxxxxxxxxxxxxxx
      integer NW(50)
      dimension XWW(100,50),TWW(100,50)
      dimension XW(100),TW(100)
      integer NA
      real XA(640),TA(640)
C     +++ �ᯮ��㥬� �p� p�ᮢ�� +++
      character*1 PFIELD,CHMB,PZOND
      real MBX,MBT,PX,PT
C     /// ��p����p� �⪨ ///
      character*65 PICTYPE
      character*10 XNAME,TNAME
      real HXC,HTC
      integer XHC,XKC,THC,TKC
      integer*2 dummy,dummy4
      record /xycoord/ s
      record /xycoord/ xt
      TYPE (rccoord) shell
c++      record /xycoord/ shell
C     /// ��ࠬ���� ���⮢ ///
      integer*2 rows
      character*20 fonttype 
      integer*2 dummyf
      character*2 charl
C**      character*3 figl,charkm		!!!OLD
      character*4 figl				!!!NEW
      character*3 charkm				!!!NEW
C
      external CORRECT
      external LINTF
      external LINTF_W
      external SPL1V2
      external NET
C     
C
C     ================ ���� ������� ===================================
      write(*,10000)
c+++ 10000 format(////1x,'��������� ���������� ���� ������'/,
10000 format(////1x,'PROGRAM OF TRAVEL-TIME FIELD CONSTRUCTING'/,
     &           1x,'"TFIELD"   Version 3.2'/,
     &           1x,'Copyright (C) Sheludko I.F. 1992-1993'/////)
C
      write(*,11000)
c+++ 11000 format(1x,'������� ��� ����� ������ ����� - ')
11000 format(1x,'ENTER THE INPUT DATA FILE NAME - ')
      read(*,'(A)') FNAMEIN
C
      write(*,13000)
c+++ 13000 format(1x,'������� ��� ����� ������ ������ - ')
13000 format(1x,'ENTER THE OUTPUT DATA FILE NAME - ')
      read(*,'(A)') FNAMEOUT
C
      write(*,12000)
c+++ 12000 format(1x,'������� ��� ����� �������������� ������ - ')
12000 format(1x,'ENTER THE INTERMEDIATE OUTPUT FILE NAME - ')
      read(*,'(A)') FNAMEOU
C
      open(5,file=FNAMEIN,status='old',form='formatted',recl=80)
      open(6,file=FNAMEOU,status='new',form='formatted',recl=80)
C     =================================================================
C
C     =============  ���� �����  ========================== 
C    =                                                     =
      read(5,10)
      write(6,10)
   10 format(/)
      read(5,'(A65)') PROFILE
      write(6,'(A65)') PROFILE
C
C     ***  ���� ������ �����஢����  ***
      read(5,20) N
      write(6,20) N
   20 format(/1x,I4/)
      read(5,10)
      do 30 J=1,N
      read(5,40) NPV(J),NST(J),T(J),L(J)
      write(6,40) NPV(J),NST(J),T(J),L(J)
   30 continue
   40 format(1x,I4,1x,I4,2x,F6.3,2x,F7.3)
C
C     **********************************************************
C     ***  ���� x-���न��� �㭪⮢ ���뢠 � �⠭権 �ਥ��  ***
C     ***  � ⠪�� ��㣮� ���ଠ樨 � ���, ����室���� ���  ***
C     ***  ��।������ ���ࠢ�� ���ࠢ��                     ***
C
C     +++  ���� �ࢨ��� ��ࠬ��஢,����஫������  +++
C     +++  ����/�뢮� ������ ��� ���� ���ࠢ��    +++
      read(5,50) IVVAR,IHFAR,IVFAR,IZMAR
      write(6,50) IVVAR,IHFAR,IVFAR,IZMAR
   50 format(//1X,I1//1X,I1//1X,I1//1X,I1/)
C
C     +++ ��砫� ᮡ�⢥��� ����� ���ଠ樨 +++
      read(5,60) KPV,KST
      write(6,60) KPV,KST
   60 format(//1X,I4/1X,I4)
C
C     +++ (�᫮���) ���� ������ ��।������ ��ࠬ��஢ �।�,     +++
C     +++ (�᫨ �� �������� ���ᨢ�, �) ���������� ᮮ⢥������� +++
C     +++ ���ᨢ�� ��������묨 ���祭�ﬨ                          +++
      read(5,70) VV
      write(6,70) VV
      if(IHFAR.eq.0) RE1OS2=1
      read(5,70) VF
      write(6,70) VF
      read(5,70) ZMIN
      write(6,70) ZMIN
   70 format(/1x,F6.3)
      do 80 J=1,KPV
      if(IVVAR.eq.0) VVPV(J)=VV
      if(IVFAR.eq.0) VFPV(J)=VF
      if(IZMAR.eq.0) ZMIPV(J)=ZMIN
   80 continue
      do 90 J=1,KST
      if(IVVAR.eq.0) VVST(J)=VV
      if(IVFAR.eq.0) VFST(J)=VF
      if(IZMAR.eq.0) ZMIST(J)=ZMIN
   90 continue
C                                             
C     +++ �᫮��� ���� ���ᨢ��, �ଠ�� ����� ������� +++
C     +++ �� �������樨 �ࢨ��� ��ࠬ��஢            +++
      read(5,100)
  100 format(/)
      do 105 J=1,KPV
      if((IVVAR.eq.0.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #    read(5,110) MPV(J),XPV(J),HPV(J)
      if((IVVAR.eq.1.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #    read(5,120) MPV(J),XPV(J),HPV(J),VVPV(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #    read(5,130) MPV(J),XPV(J),HPV(J),VVPV(J),HFPV(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.0))
     #    read(5,140) MPV(J),XPV(J),HPV(J),VVPV(J),HFPV(J),VFPV(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #    read(5,150) MPV(J),XPV(J),HPV(J),HFPV(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.0))
     #    read(5,160) MPV(J),XPV(J),HPV(J),HFPV(J),VFPV(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #    read(5,170) MPV(J),XPV(J),HPV(J),ZMIPV(J)                
      if((IVVAR.eq.1.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #    read(5,180) MPV(J),XPV(J),HPV(J),VVPV(J),ZMIPV(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #    read(5,190) MPV(J),XPV(J),HPV(J),VVPV(J),HFPV(J),ZMIPV(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.1))
     #read(5,200) MPV(J),XPV(J),HPV(J),VVPV(J),HFPV(J),VFPV(J),ZMIPV(J)
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #    read(5,210) MPV(J),XPV(J),HPV(J),HFPV(J),ZMIPV(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.EQ.1))
     #    read(5,220) MPV(J),XPV(J),HPV(J),HFPV(J),VFPV(J),ZMIPV(J)
  105 continue
      read(5,106)
  106 format(/)
      do 107 J=1,KST
      if((IVVAR.eq.0.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #    read(5,110) MST(J),XST(J),HST(J)
      if((IVVAR.eq.1.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #    read(5,120) MST(J),XST(J),HST(J),VVST(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #    read(5,130) MST(J),XST(J),HST(J),VVST(J),HFST(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.0))
     #    read(5,140) MST(J),XST(J),HST(J),VVST(J),HFST(J),VFST(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #    read(5,150) MST(J),XST(J),HST(J),HFST(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.0))
     #    read(5,160) MST(J),XST(J),HST(J),HFST(J),VFST(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #    read(5,170) MST(J),XST(J),HST(J),ZMIST(J)                
      if((IVVAR.eq.1.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #    read(5,180) MST(J),XST(J),HST(J),VVST(J),ZMIST(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #    read(5,190) MST(J),XST(J),HST(J),VVST(J),HFST(J),ZMIST(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.1))
     #read(5,200) MST(J),XST(J),HST(J),VVST(J),HFST(J),VFST(J),ZMIST(J)
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #    read(5,210) MST(J),XST(J),HST(J),HFST(J),ZMIST(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.EQ.1))
     #    read(5,220) MST(J),XST(J),HST(J),HFST(J),VFST(J),ZMIST(J)
  107 continue
C
C     +++ �᫮��� �뢮� ���ᨢ�� +++
      write(6,100)
      do 108 J=1,KPV
      if((IVVAR.eq.0.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #   write(6,110) MPV(J),XPV(J),HPV(J)
      if((IVVAR.eq.1.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #   write(6,120) MPV(J),XPV(J),HPV(J),VVPV(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #   write(6,130) MPV(J),XPV(J),HPV(J),VVPV(J),HFPV(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.0))
     #   write(6,140) MPV(J),XPV(J),HPV(J),VVPV(J),HFPV(J),VFPV(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #   write(6,150) MPV(J),XPV(J),HPV(J),HFPV(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.0))
     #   write(6,160) MPV(J),XPV(J),HPV(J),HFPV(J),VFPV(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #   write(6,170) MPV(J),XPV(J),HPV(J),ZMIPV(J)                
      if((IVVAR.eq.1.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #   write(6,180) MPV(J),XPV(J),HPV(J),VVPV(J),ZMIPV(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #   write(6,190) MPV(J),XPV(J),HPV(J),VVPV(J),HFPV(J),ZMIPV(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.1))
     #write(6,200) MPV(J),XPV(J),HPV(J),VVPV(J),HFPV(J),VFPV(J),ZMIPV(J)
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #   write(6,210) MPV(J),XPV(J),HPV(J),HFPV(J),ZMIPV(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.EQ.1))
     #   write(6,220) MPV(J),XPV(J),HPV(J),HFPV(J),VFPV(J),ZMIPV(J)
  108 continue
      write(6,106)
      do 109 J=1,KST
      if((IVVAR.eq.0.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #   write(6,110) MST(J),XST(J),HST(J)
      if((IVVAR.eq.1.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #   write(6,120) MST(J),XST(J),HST(J),VVST(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #   write(6,130) MST(J),XST(J),HST(J),VVST(J),HFST(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.0))
     #   write(6,140) MST(J),XST(J),HST(J),VVST(J),HFST(J),VFST(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.0))
     #   write(6,150) MST(J),XST(J),HST(J),HFST(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.0))
     #   write(6,160) MST(J),XST(J),HST(J),HFST(J),VFST(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #   write(6,170) MST(J),XST(J),HST(J),ZMIST(J)                
      if((IVVAR.eq.1.and.IHFAR.eq.0.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #   write(6,180) MST(J),XST(J),HST(J),VVST(J),ZMIST(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #   write(6,190) MST(J),XST(J),HST(J),VVST(J),HFST(J),ZMIST(J) 
      if((IVVAR.eq.1.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.eq.1))
     #write(6,200) MST(J),XST(J),HST(J),VVST(J),HFST(J),VFST(J),ZMIST(J)
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.0.and.IZMAR.eq.1))
     #   write(6,210) MST(J),XST(J),HST(J),HFST(J),ZMIST(J) 
      if((IVVAR.eq.0.and.IHFAR.eq.1.).and.(IVFAR.eq.1.and.IZMAR.EQ.1))
     #   write(6,220) MST(J),XST(J),HST(J),HFST(J),VFST(J),ZMIST(J)
  109 continue
C
  110 format(1x,I4,2x,F8.3,2x,F6.3)
  120 format(1x,I4,2x,F8.3,2x,F6.3,2x,F5.3)
  130 format(1x,I4,2x,F8.3,2x,F6.3,2x,F5.3,2x,F6.3)
  140 format(1x,I4,2x,F8.3,2x,F6.3,2x,F5.3,2x,F6.3,2x,F5.3)
  150 format(1x,I4,2x,F8.3,2x,F6.3,2x,5x,2x,F6.3)
  160 format(1x,I4,2x,F8.3,2x,F6.3,2x,5x,2x,F6.3,2x,F5.3)
  170 format(1x,I4,2x,F8.3,2x,F6.3,2x,5x,2x,6x,2x,5x,2x,F6.3)
  180 format(1x,I4,2x,F8.3,2x,F6.3,2x,F5.3,2x,6x,2x,5x,2x,F6.3)
  190 format(1x,I4,2x,F8.3,2x,F6.3,2x,F5.3,2x,F6.3,2x,5x,2x,F6.3)
  200 format(1x,I4,2x,F8.3,2x,F6.3,2x,F5.3,2x,F6.3,2x,F5.3,2x,F6.3)
  210 format(1x,I4,2x,F8.3,2x,F6.3,2x,5x,2x,F6.3,2x,5x,2x,F6.3)
  220 format(1x,I4,2x,F8.3,2x,F6.3,2x,5x,2x,F6.3,2x,F5.3,2x,F6.3)
C
C   /// ����� ����� ��⠢���               /      read(5,225) \  ///
C       ��� ���ᠭ�� ������ � ����� ����� \  225 format(14/) /
C
C     +++ �᫮��� ���� ���ᨢ�� �����祭 +++
C     *** �����祭 ���� ���ଠ樨 � �㭪�� ���뢠 � �ਥ�� ***
C     **********************************************************
C                                            
C     +++ ���� ���祭�� �࠭�筮� ᪮���       +++
C     +++ �� ����ਪ�஢�� �५������� �࠭��� +++
      read(5,230) IVGL
      write(6,230) IVGL
  230 format(/1x,I1/)
C
C++   if(IVGL.eq.0) goto 240
C++   goto 260
  240 read(5,250) VK,VM
      write(6,250) VK,VM
  250 format(1x,F5.3/1x,F5.3)
      KVG=2
      VGL(1,1)=120.0
      VGL(2,1)=6.4
      VGL(1,2)=180.0
      VGL(2,2)=8.0
C
C+260 if(IVGL.eq.1) goto 270
C++   goto 305
C+270 read(5,280) KVG
      read(5,280) KVG
      write(6,280) KVG
  280 format(1x,I2/)
      do 290 K=1,KVG
      read(5,300) VGL(1,K),VGL(2,K)
      write(6,300) VGL(1,K),VGL(2,K)
  290 continue
  300 format(1x,F7.3,2x,F5.3)
C
  305 read(5,310) ICORR,IWIDE,RE1OS2
      write(6,310) ICORR,IWIDE,RE1OS2
  310 format(/1x,I1//1x,I1//1x,I1/)    
C     *** ���� �ᥩ ���ଠ樨, ����室���� ��� ���� ���ࠢ��, �����祭 ***
C
C     *** ���� ��ࠬ��஢ ��ࠡ�⪨ � ����஥��� ���� �६�� ***
C     +++ ���� ����� �������� ��� 蠣� �� ������� +++
      read(5,320) IL0AR
      write(6,320) IL0AR
  320 format(//1x,I1/)
C
      if(IL0AR.eq.0) goto 330
      goto 350
  330 read(5,340) DL
      write(6,340) DL
  340 format(1x,F6.3)
C
  350 if(IL0AR.eq.1) goto 360
      goto 390
  360 read(5,370) G
      write(6,370) G
  370 format(1x,I2/)
      read(5,380) (L0(J),J=1,G)
      write(6,380) (L0(J),J=1,G)
  380 format(1x,10F7.3)
C
C     +++ ���� ���ࢠ��� ���᪠ �祪 �� �������� ���௮��樨 +++
  390 read(5,400) IDXDTAR
      write(6,400) IDXDTAR
  400 format(1x,I1/)
C
      if(IDXDTAR.eq.0) goto 410
      goto 430
  410 read(5,420) DX,DT,LJ,DX2,DT2
      write(6,420) DX,DT,LJ,DX2,DT2
  420 format(1X,F7.3/2X,F6.3/1X,F7.3/1X,F7.3/2X,F6.3)
C
  430 if(IDXDTAR.eq.1) goto 440
      goto 470
  440 read(5,445)
      write(6,445)
  445 format(/)
      do 450 J=1,G
      read(5,460) L0(J),DXL(J),DTL(J)
      write(6,460) L0(J),DXL(J),DTL(J)
  450 continue
  460 format(1x,F7.3,2X,F7.3,2X,F6.3)
C
C     +++ ���� ��ࠬ��� ᣫ�������� �� ᯫ���-���பᨬ�樨 +++
C     +++ � 蠣� ����⨧�樨 �������� ���� �६��           +++
  470 read(5,480) R0,HX,HX2
      write(6,480) R0,HX,HX2
  480 format(/1X,F6.4/1X,F6.3/1X,F6.3)
C
C     +++ ���� ��ࠬ��� ������� ⨯� ���பᨬ�樨 ��������    +++
C     +++ � ���ᨬ��쭮�� 㤠����� �祪, � ���ண� ��稭����� +++
C     +++ �������� ���௮����                                 +++
      read(5,485) IDX,DXMAX
      write(6,485) IDX,DXMAX
  485 format(6X,I1/1X,F6.3)
C                                                                
C     +++ ���� ��ࠬ��஢ ���न��⭮� �⪨ +++
      read(5,475) HXC,HTC  
      write(6,475) HXC,HTC
  475 format(1X,F6.2/1X,F6.3)
C =                                                             =
C  =================== ����� ����� ����� =======================
C
C     *** ��।������ ���न��� 業�஢ �����஢���� ***
      write(6,495)
      do 490 J=1,N
      JP=NPV(J)
      JS=NST(J)
      INDP=0
      INDS=0
      do 486 I=1,KPV
      if(JP.eq.MPV(I)) INDP=1
      if(JP.eq.MPV(I)) IP=I
  486 continue
      do 487 I=1,KST
      if(JS.eq.MST(I)) INDS=1
      if(JS.eq.MST(I)) IS=I
  487 continue
      if(INDP.eq.0) write(6,501) NPV(J)
      if(INDP.eq.0) write(*,501) NPV(J)
      if(INDS.eq.0) write(6,502) NST(J)
      if(INDS.eq.0) write(*,502) NST(J)
      X(J)=(XPV(IP)+XST(IS))*0.5
      write(6,500) NPV(J),NST(J),T(J),L(J),X(J)
  490 continue
  495 format(/3x,'��/��',6x,'T',7x,'L',7x,'X')
  500 format(1x,I4,'/',I4,2x,F5.2,2x,F6.2,2x,F7.2)
c+++  501 format(1x,'� ᯨ᪥ ��� �㭪� ���뢠 - ',I3)
  501 format(1x,'in a list is absent the shot point - ',I3)
c++   502 format(1x,'� ᯨ᪥ ��� �㭪� �ਥ�� - ',I3)
  502 format(1x,'in a list is absent the receiver point - ',I3)
C
C     ===========================================================
C     *** ��।������ � ���� ���ࠢ�� ***
      if(ICORR.eq.0) goto 530
      call CORRECT(N,NPV,NST,T,L,KPV,MPV,HPV,ZMIPV,VVPV,HFPV,VFPV,
     #                           KST,MST,HST,ZMIST,VVST,HFST,VFST,
     #                           KVG,VGL,RE1OS2)                        
C
      write(6,510)
c+++  510 format(/2x,'����� � ��⮬ ���ࠢ��'/
c+++     #       /2x,'��/��',5x,'T',7x,'L',6x,'X')
  510 format(/2x,'Data after correction'/
     #       /2x,'PV/ST',5x,'T',7x,'L',6x,'X')
      do 520 J=1,N
      write(6,500) NPV(J),NST(J),T(J),L(J),X(J)
  520 continue   
C     ===========================================================
C
C     ===========================================================
C     *** �������� ���௮���� ***
  530 if(IWIDE.NE.1) call LINTF(N,NPV,NST,T,L,X,
     #                         IL0AR,G,L0,DL,
     #                        IDXDTAR,DXL,DTL,DX,DT,LJ,DX2,DT2,HX,HX2,
     #                       XMIN,XMAX,TMIN,TMAX,LMIN,LMAX,NW,XWW,TWW)    
      if(IWIDE.EQ.1) call LINTF_W(N,NPV,NST,T,L,X,
     #                           IL0AR,G,L0,DL,
     #                         IDXDTAR,DXL,DTL,DX,DT,LJ,DX2,DT2,HX,HX2,
     #                        XMIN,XMAX,TMIN,TMAX,LMIN,LMAX,NW,XWW,TWW)
C
      write(6,540)
c+++  540 format(1x,'������� �������� ���௮��樨')
  540 format(1x,'Linear interpolation result')
      do 550 J=1,G
      write(6,560) (XWW(I,J),I=1,NW(J))
      write(6,570) (TWW(I,J),I=1,NW(J))
  550 continue
C++  560 format(1x,10f7.1)
C++  570 format(2x,10f7.2)
  560 format(1x,10f7.3)
  570 format(2x,10f7.3)
C     ===========================================================
C
C     ===========================================================
C     *** ᯫ���-���பᨬ��� �������� ***
C     *** �믮������ � ��� �⠯�       ***
C
C     ++++++++   1-� �⠯   +++++++++++++++++++++++++++++++++++++
C     ++++  p�����뢠���� ���p��� ���祭�� ��� INVERS +++++++
C     +++ ������� �祪 ���பᨬ�樨 +++
      NA=INT((XMAX-XMIN)/HX+1.E-5)+1
      do 600 I=1,NA 
      XA(I)=XMIN+(I-1)*HX
      TA(I)=0.0
  600 continue
      write(6,605)
c+++  605 format(1x,'������� ᯫ���-���பᨬ�樨'/
c+++     #       1x, 'X-���न���� �祪')
  605 format(1x,'Spline-approximation result'/
     #       1x, 'X-coordinates of points')
      write(6,560) (XA(I),I=1,NA)
      write(6,606)
c+++  606 format(1x,'���᫥��� �६���')
  606 format(1x,'Calculated times')
C     +++ 横� ��ॡ�� �������� +++
      do 640 J=1,G
      do 607 I=1,NA 
      TA(I)=0.0
  607 continue
      NJ=NW(J)
      if(NJ.eq.0) goto 615
      DO 610 I=1,NJ
      XW(I)=XWW(I,J)
      TW(I)=TWW(I,J)
  610 continue
      call SPL1V2(NJ,XW,TW,R0,NA,XA,TA,IDX,DXMAX)
C
C     +++ ��१����� ���பᨬ�஢����� �������� � ���ᨢ T1(,) +++
  615 do 620 I=1,NA
      T1(J,I)=TA(I)
      TA(I)=0.0
  620 continue
C
      write(6,630) L0(J)
  630 format(1x,'L0(J) = ',f6.1)
      write(6,650) (T1(J,I),I=1,NA)
C
  640 continue
  650 format(10f6.2)
      N1=NA
      do 660 I=1,N1
      X1(I)=XA(I)
  660 continue
C     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
C     ++++++++   2-� �⠯   +++++++++++++++++++++++++++++++++++++
C     +++++++ p�����뢠���� �������� ��� �p���᪮�� ��p��� +++
      PFIELD='Y'
      PZOND='N'
      CHMB='N'
      write(*,670)
c+++  670 format(1x,'��� ���ᮢ��� ���� �६�� ?   (Y/N) - '\)
  670 format(1x,'Will you draw the time field ?   (Y/N) - '\)
      read(*,'(A)') PFIELD
      if(PFIELD.eq.'N') goto 920 
      open(20,file='IZOLINES',status='new',form='binary',recl=640)
C
      NA=int((XMAX-XMIN)/HX2+1.E-5)+1
      do 680 I=1,640
      XA(I)=0.0
      TA(I)=0.0
  680 continue
      do 690 I=1,NA
      XA(I)=XMIN+(I-1)*HX2
  690 continue
C     +++ 横� ��p���p� �������� +++
      do 720 J=1,G
      do 700 I=1,640
      TA(I)=0.0
  700 continue
      NJ=NW(J)
      if(NJ.eq.0) goto 720
      do 710 I=1,NJ
      XW(I)=XWW(I,J)
      TW(I)=TWW(I,J)
  710 continue
      call SPL1V2(NJ,XW,TW,R0,NA,XA,TA,IDX,DXMAX)
C
C     +++ ������ �������� �� ��� +++
      write(20) TA
  720 continue
      write(*,730)
c+++  730 format(//1x,'�������� ���� ����ᠭ� � 䠩� "IZOLINES"')
  730 format(//1x,'isolines has been written into the file "IZOLINES"')
C     +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C     ===========================================================
C
C     ===========================================================
C     *** ᮧ����� �p���᪮�� ��p��� ���� �p���� ***
C
c+++      PICTYPE=' ���� ������'
c+++      XNAME='X, ��'
c+++      TNAME='T, ᥪ'
c+++      charl='l='
c+++      charkm=' km'
      PICTYPE=' TIME FIELD'
      XNAME='X, m '
      TNAME='T,msec'
      charl='l='
      charkm=' m '
C
      dummy = setvideomode($ERESCOLOR)
      call clearscreen($GCLEARSCREEN)
      rows = settextrows(25)
      jfont = registerfonts( 'C:\FONT\helvb.fon')
      fonttype = "t'helv'" // 'h10w5b'
      dummyf = setfont(fonttype)
C     +++ ������� ����⠡�� p��㭪� +++
      MBX=abs((XMAX-XMIN)/450)
      MBT=abs((TMAX-TMIN)/250)
  800 write(*,810) MBX,MBT
c+++  810 format(/1x,'����⠡ �� �� x ࠢ�� ',F7.4,' �� �� ���ᥫ�'/
c+++     #        1x,'����⠡ �� �� t ࠢ�� ',F7.4,' ᥪ �� ���ᥫ�'/
c+++     #       /1x,'��� �������� ����⠡ ?   (Y/N) - '\)
  810 format(/1x,'x-axis scale is ',F7.4,' m per pixel'/
     #        1x,'t-axis scale is ',F7.4,' sec per pixel'/
     #       /1x,'will you change the scale ?   (Y/N) - '\)
      read(*,'(A)') CHMB
      if(CHMB.eq.'Y') goto 820
      goto 850
  820 write(*,830)
c+++  830 format(/1x,'������ ���� ����⠡ �� ��  x - '\)
  830 format(/1x,'assign a new scale on x-axis - '\)
      read(*,835) MBX
  835 format(f7.4)                                             
      write(*,840)
c+++  840 format(/1x,'������ ���� ����⠡ �� ��  t - '\) 
  840 format(/1x,'assign a new scale on t-axis - '\)
      read(*,845) MBT
  845 format(f7.4)                                             
C
C     +++ p���� p����p�� p���祣� ���� +++   
  850 PX=(XMAX-XMIN)*MBX
      PT=(TMAX-TMIN)*MBT
      if(PX.gt.600) write(*,851)
  851 format(1x,'horizontal dimension is greater than 600 pixels')
      if(PT.gt.300) write(*,852)
  852 format(1x,'vertical dimension is greater than 300 pixels')
C
C     +++ ��p�������� p����p�� "楫쭮����p���᪮�" �⪨ +++
C     --- ������� 蠣� �⪨ ---
      XHC=INT(XMIN/HXC+1.E-5)*HXC
      XKC=(INT(XMAX/HXC-1.E-5)+1)*HXC
      THC=INT(TMIN/HTC+1.E-5)*HTC
      TKC=(INT(TMAX/HTC-1.E-5)+1)*HTC
C
      call clearscreen($GCLEARSCREEN)
C     +++ �⠭���⭮� ᬥ饭�� � ����஥��� �⪨ +++
      call setvieworg(40,20,s)
      call NET(XMIN,XMAX,TMIN,TMAX,MBX,MBT,HXC,HTC,XNAME,TNAME,PROFILE,
     #         PICTYPE)
C
C     *** ᮡ�⢥��� ���ᮢ�� �������� ***
      rewind 20
C***      dummy4=setcolor(9)							  	 ! new
C
      do 900 J=1,G                                                     
      if(NW(J).eq.0) goto 900
C
      NCOL=J-INT(J/5.0-0.01)*5    						 ! new
      NCOL=9+NCOL											 ! new
      dummy4 = setcolor(NCOL)								 ! new
C                                                                       
C     +++ �⥭�� ���祭�� ��।��� �������� TA(640) ***      
      read(20) TA
C                                                                       
      do 890 I=1,NA-1                                                  
      if(TA(I).eq.0.0.and.TA(I+1).eq.0.0) goto 890                
C                                                                       
      if(TA(I).eq.0.0.and.TA(I+1).ne.0.0) goto 860                
C     +++ ������ ��� � ��砫� �������� +++                        
      goto 861
  860 IX=INT((XA(I+1)-XHC)/MBX+1.E-5)                                  
      IT=INT((TA(I+1)-THC)/MBT+1.E-5)
      call moveto(IX,IT,xt)
      goto 890
C                                                                       
  861 if(TA(I).ne.0.0.and.TA(I+1).eq.0.0) goto 865
C     +++ ������ ��� � ����� �������� � �� ��ન஢�� +++                        
      goto 870
  865 IX=INT((XA(I)-XHC)/MBX+1.E-5)                                  
      IT=INT((TA(I)-THC)/MBT+1.E-5)
      call moveto(IX+8,IT-5,xt)
      call outgtext(charl) 
C**      LI=INT(L0(J)+1.E-5)					 !!!OLD
      IL=INT((L0(J)+1.E-5)*1000.0)  			 !!!NEW
C**      write(figl,'(i3)') LI				 !!!OLD
      write(figl,'(i4)') IL					 !!!NEW
      call outgtext(figl) 
      call outgtext(charkm) 
      goto 890
C                                                                       
  870 if(TA(I).ne.0.0.and.TA(I+1).ne.0.0) goto 880                
      goto 890                                                          
  880 IX=INT((XA(I)-XHC)/MBX+1.E-5)                                  
      IT=INT((TA(I)-THC)/MBT+1.E-5)
      dummy=lineto(IX,IT)
C
  890 continue                                                          
C     +++ ����� 横�� ��।��� �������� +++                            
  900 continue                                                          
C
C==>  *** posting the interpolation points at izolines *** ! new
C														 ! new
C***    dummy4=setcolor(12)								 ! new
C														 ! new
      do 1900 J=1,G                                        ! new             
      if(NW(J).eq.0) goto 1900							 ! new
      NCOL=J-INT(J/5.0-0.01)*5    						 ! new
      NCOL=9+NCOL											 ! new
      dummy4 = setcolor(NCOL)								 ! new
C														 ! new
	do 1890 I=1,NW(J)                                    ! new              
C     +++ ������ ��� � current point +++                  ! new      
      IX=INT((XWW(I,J)-XHC)/MBX+1.E-5)                     ! new             
      IT=INT((TWW(I,J)-THC)/MBT+1.E-5)					 ! new
C*      call moveto(IX,IT,xt)								 ! new
C                                                          ! new             
	dummy = setpixel(IX+1,IT-1)     					 ! new
      dummy = setpixel(IX-1,IT+1)    						 ! new
	dummy = setpixel(IX+1,IT+1)    						 ! new
	dummy = setpixel(IX-1,IT-1)    						 ! new
	dummy = setpixel(IX,IT)                     		 ! new
C														 ! new
 1890 continue                                             ! new             
C     +++ ����� 横�� ��।��� �������� +++               ! new             
 1900 continue                                             ! new             
C==>                                                                     
C     +++ �뢮� ������ �����஢���� +++
C++      call settextposition(23,1,shell)
      call settextposition(19,1,shell)
      read(*,*)
C++      call settextposition(24,1,shell)
      call settextposition(20,1,shell)
      write(*,901) 
c+++  901 format(7x,'��� ���ᮢ��� ��室�� ����� ?   (Y/N) -      '\) 
  901 format(7x,'Will you post the initial data ?   (Y/N) -          '\) 
      read(*,'(A)') PZOND
      if(PZOND.eq.'N') goto 903 
      do 902 J=1,N
      IX=INT((X(J)-XHC)/MBX+1.E-5)                                  
      IT=INT((T(J)-THC)/MBT+1.E-5)
C**      NCOL=NPV(J)-INT(NPV(J)/5.0-0.01)*5			   !!!NEW
      NCOL=NPV(J)/50-INT(NPV(J)/50/5.0-0.001)*5		   !!!NEW
      NCOL=9+NCOL
      dummy4 = setcolor(NCOL)
      dummy = setpixel(IX,IT)                     
      dummy = setpixel(IX+1,IT)                     
      dummy = setpixel(IX+1,IT+1)                     
      dummy = setpixel(IX,IT+1)                     
  902 continue                                                          
C     +++ ������ ��� ��������� ����⠡�� +++
C++      call settextposition(23,1,shell)
      call settextposition(19,1,shell)
  903 read(*,*)
      CHMB='N'
C++      call settextposition(24,1,shell)
      call settextposition(20,1,shell)
      write(*,910) 
c+++  910 format(7x,'����⠡ ��㤠祭 ?   (Y/N) -                        '\) 
  910 format(7x,'the scale is not suitable, is not it ?   (Y/N) -    '\) 
      read(*,'(A)') CHMB
      if(CHMB.eq.'Y') goto 800
C
      close(20,status='delete')
      dummy4 = settextcolor(15)
C++      call settextposition(24,7,shell)
C++      call settextposition(20,1,shell)
      call settextposition(19,1,shell)
      call outtext(PROFILE)
      read(*,*)
C mode      call clearscreen($GCLEARSCREEN)		  !!!NEW
C												  
C mode      dummy = setvideomode($DEFAULTMODE)	  !!!NEW
C     ===========================================================
C     ===========================================================
C     *** �ନ஢���� 䠩�� ��室��� ������ (��� ��."DEAD6") ***
C
  920 XMIN=X1(1)
      XMAX=X1(N1)
      HX1=HX*0.5
      HZ1=1.0
      DXF=2*HX1
      DZF=2*HZ1
      MBZ=0.5     
      open(7,file=FNAMEOUT,status='new',form='formatted',recl=80)
      write(7,930) PROFILE,G,N1,XMIN,XMAX,HX,ZMIN
  930 format(5x,'������ ����� � ���������  "DEAD6"  Version 6.0'/
     #  4x,A65/
     # 1x,i7,5x,'G - �᫮ �������� ���� �p����, integer'/
     # 1x,i7,4x,'NN - �᫮ ����⮢ ���� �p���� �� �� x, integer'/
     # 1x,f8.3,2x,
     # 'MIX - ����� �࠭�� ������� ���� �६�� �� �� x, real [��] '/
     # 1x,f8.3,2x,
     # 'MAX - �ࠢ�� �࠭�� ������� ���� �६�� �� �� x, real [��]'/
     # 1x,f7.3,3x,'HX1 - 蠣 ������� ���� �p���� �� �� x, real [��]'/
     # 1x,f7.3,3x,
     # 'MIZ - �p����� �p�������� ���� �p���� � p��p��� �� �� z, [��]')
      write(7,931)
  931 format(/9x,'L0(G) - ���ᨢ ��� �����஢���� (10F8.3)') 
      write(7,941) (L0(J),J=1,G)
      write(7,932)
  932 format(/7x,'T(G,NN) - ���� �६�� (10F7.3)') 
      do 940 J=1,G
      write(7,942) (T1(J,I),I=1,N1)
  940 continue
  941 format(10f8.3)
  942 format(10f7.3)
      write(7,950) HX1,HZ1,MBX,MBZ,DXF,DZF,DXF,DZF,DZF,
     #             0,0,0,1,0
  950 format(/
     # 1x,f7.3,3x,
     #   'HX2 - 蠣 �� �� x ࠢ����୮� �⪨ ���௮��樨, real [��]'/
     # 1x,f7.3,3x,
     #   'HZ2 - 蠣 �� �� z ࠢ����୮� �⪨ ���௮��樨, real [��]'/
     # 1x,f7.4,3x,
     #   'MBX = HXP / ����⠡� ����᪮�� ����ࠦ���� �� ��� x � z'/
     # 1x,f7.4,3x,
     #   'MBZ = HZP \ (�᫮ �� � ����� ���ᥫ�), real [��]'/
     # 1x,f7.3,2x,'DXF1 - \ ࠧ���� ���� ��।���饣� ᣫ��������,[��]'/
     # 1x,f7.3,2x,'DZF1 - /         1-� ����                       '/
     # 1x,f7.3,2x,'DXF2 - \                                        '/
     # 1x,f7.3,2x,'DZF2 - /         2-� ����                       '/
     # 1x,f7.3,3x,'ZF2 - �஢���, ࠧ�����騩 ������ ࠧ��筮�� ',
     #            'ᣫ��������, [��]'/
     # 1x,i7,1x,'IFRAG - ������(=1) ��� �⬥��� (=0) ���� ',
     #          '���.��ࠧ� ����. ࠧ१�'/
     # 1x,i7,1x,'ISYMP - ������(=1) ��� �⬥��� (=0) ���� ',
     #          '���.��ࠧ� ���.���. ࠧ१�'/
     # 1x,i7,1x,'IFLTR - ������(=1) ��� �⬥��� (=0) ���� ',
     #          '���.��ࠧ� ᣫ����. ࠧ१�'/
     # 1x,i7,3x,
     #   'KOD - ࠧp�蠥� (=1) ��� ���p�頥� (=0) �p������. �����'/
     # 1x,i7,2x,
     #   'KOD1 - p��p�蠥� (=1) ��� ���p�頥� (=0) ���� VPMAX(G)'/
     # /4x,'VPMAX(33) - ���ᨢ ',
     #    '��࠭�祭�� ���⭮�� ����. �ࠤ���� ���� �६�� 10F6.3')
C													
C**      write(*,960) FNAMEOUT						  !!!NEW
  960 format(/1x,'������� ����� ��� "DEAD6" ������� � ���� - ',A70)
      close(7,status='keep')
C
      close(6,status='keep')
      close(5,status='keep')
C
C
      stop
      end
