C                                                                       
C     =========================================================         
C     =        SPL1V2(N,X,T,R0,NA,XA,TA,IDX,DXMAX)            =
C     =   OPOPAMMA OHOMEPHO CAH APOKCMA        =         
C     =   APAMETP CABAH R0 AAETC P BBOE          =         
C     =          01.10.87. (version 2.0 1992 £.)              =         
C     =========================================================         
C                     BXOHE AHHE                                    
C     *************************************************************     
C     * N - INTEGER,KOECTBO TOEK AAH HK T(X)         *     
C     * X(N) - REAL, X - KOOPHAT TOEK AAH HK T(X)    *     
C     * T(N) - REAL, MACCB CKPETHX HAEH HK           *     
C     * NA - INTEGER, KOECTBO TOEK OPEEEH APOKCMPO-  *     
C     *               BAHHX HAEH YHK                     *     
C     * XA(NA) - REAL, MACCB X - KOOPHAT AHHX TOEK          *     
C     * R0 - REAL, APAMETP CABAH                           *
C     * IDX - INTEGER,   (=0)    *
C     *                (=1)  DXMAX                            *
C     * DXMAX - REAL,     -  *
C     *               ,    --*
C     *                     *
C     *                                *
C     *************************************************************     
C
C                    BXOHE AHHE                                    
C     *************************************************************     
C     * NA - INTEGER, KOECTBO TOEK OPEEEH APOKCMPO-  *     
C     *               BAHHX HAEH YHK                     *     
C     * XA(NA) - REAL, MACCB X - KOOPHAT AHHX TOEK          *     
C     * TA(NA) - REAL, MACCB HAEH HK, OEHHX        *     
C     *                OCPECTBOM CAH-APOKCMA           *     
C     *************************************************************     
C                   PAOE APAMETP                                   
C     *************************************************************     
C     * A(N,N)  \                                                 *     
C     * H(N,N)  --  MATP, COEME  OPMPOBAH        *     
C     * H1(N,N) /   OCHOBHO MATP KOEHTOB                *     
C     * F(N)  -   BEKTOP PABO ACT CCT. PABHEH             *     
C     * MU(N) -                                                   *     
C     * M0(N) -                                                   *     
C     *              engl.- transposed matrix                     *
C     * AV(N*N) -  MATPA, TPAHCOPMPOBAHHA B BEKTOPH OPM  *     
C     * MP(N) -                                                   *     
C     *************************************************************     
C                                                                                       
      SUBROUTINE SPL1V2(N,X,T,R0,NA,XA,TA,IDX,DXMAX)
      DIMENSION T(N),X(N),XA(NA),TA(NA)
      DIMENSION A(100,100),AV(10000),H(100,100),H1(100,100)             
      REAL MU(100),M0(100),MP(100),R0                                   
      DOUBLE PRECISION AV,F(100)
      INTEGER I,J,IDX
      REAL A0,XC,TC,HI,HI1,DXMAX                                              
C
      EXTERNAL DGAUSS
C                                                                       
C     *** EC TOEK MEHE 4-X, TO MHEM CAH-APOKCMA ***     
      IF(N.LT.4) GOTO 111                                               
C                                                                       
      DO 1 I=1,N                                                        
      DO 1 J=1,N                                                        
      A(J,I)=0.0                                                        
      H(J,I)=0.0                                                        
    1 CONTINUE                                                          
C                                                                       
C     *** AAHE MATP _A  _H ***                                    
      DO 3 I=1,N                                                        
      IF(I.NE.1) HI=X(I)-X(I-1)                                         
      IF(I.NE.N) HI1=X(I+1)-X(I)                                        
      IF(I.NE.1.AND.I.NE.N) GOTO 2                                      
      A(I,I)=1.0                                                        
      GOTO 3                                                            
    2 A(I,I)=(HI+HI1)/3                                                 
      A(I-1,I)=HI/6                                                     
      A(I+1,I)=HI1/6                                                    
      H(I,I)=(-1.)/HI+(-1.)/HI1                                         
      H(I-1,I)=1./HI                                                    
      H(I+1,I)=1./HI1                                                   
    3 CONTINUE                                                          
      A(1,2)=0.0                                                        
      A(N,N-1)=0.0                                                      
C                                                                       
      DO 4 I=1,N                                                        
      DO 4 J=1,N                                                        
      H1(J,I)=H(I,J)                                                    
      H1(J,I)=(1./R0)*H1(J,I)                                           
    4 CONTINUE                                                          
C                                                                       
C     *** BCEHE MATP _A=_A+_H*_H1  ***                          
      DO 6 I=1,N                                                        
      DO 6 KJ=1,N                                                       
      S=0.0                                                             
      DO 5 J=1,N                                                        
    5 S=S+H(J,I)*H1(KJ,J)                                               
      A(KJ,I)=A(KJ,I)+S                                                 
    6 CONTINUE                                                          
C                                                                       
C     *** BCEHE BEKTOPA PABO ACT _F=_H*_T  ***                 
      DO 8 I=1,N                                                        
      S=0.0                                                             
      DO 7 J=1,N                                                        
    7 S=S+H(J,I)*T(J)                                                   
      F(I)=S                                                            
    8 CONTINUE                                                          
C                                                                       
C     *** PEOPAOBAHE MATP  CCTEM _A B BEKTOPH OPM _AV ***  
C     ***  POEP DGAUSS                                     ***  
      DO 9 I=1,N                                                        
      DO 9 J=1,N                                                        
      IAV=(I-1)*N+J                                                     
      AV(IAV)=A(J,I)                                                    
    9 CONTINUE               
C                                           
      ERRM=1.E-7                                                                       
C     *** PEEHE CCTEM HEHX PABHEH ***                        
      CALL DGAUSS(N,AV,F,ERRM,F,EPS)
C                                                                       
      DO 11 I=1,N                                                       
      MU(I)=F(I)                                                        
   11 CONTINUE                                                          
C                                                                       
C     *** BCEHE BEKTOPA _M0=_T-_H1*_MU  ***                        
      DO 13 I=1,N                                                       
      S=0.0                                                             
      DO 12 J=1,N                                                       
   12 S=S+H1(J,I)*MU(J)                                                 
      M0(I)=T(I)-S                                                      
   13 CONTINUE                                                          
C                                                                       
C     *** PACET APOKCMPOBAHHX HAEH ***                        
      DO 15 I=2,N                                                       
      HI=X(I)-X(I-1)                                                    
      DO 15 IA=1,NA                                                     
      XC=XA(IA)
C
C     ***    ,     DXMAX ***
C     ***    ***
      IF(IDX.EQ.1.AND.HI.GT.DXMAX) GOTO 131
      GOTO 132
  131 IF(XC.GE.X(I-1).AND.XC.LT.X(I)) TA(IA)=
     #T(I-1)+(T(I)-T(I-1))/(X(I)-X(I-1))*(XA(IA)-X(I-1))
      GOTO 15
C
  132 IF(XC.GE.X(I-1).AND.XC.LT.X(I)) GOTO 14                           
      GOTO 15                                                           
   14 TA(IA)=MU(I-1)*(X(I)-XC)**3/6/HI+                                 
     #MU(I)*(XC-X(I-1))**3/6/HI+                                        
     #(M0(I-1)-MU(I-1)*HI**2/6)*(X(I)-XC)/HI+                           
     #(M0(I)-MU(I)*HI**2/6)*(XC-X(I-1))/HI                              
   15 CONTINUE                                                          
C                                                                       
C     *** P N>3 EPECKA HA METK 101 ***                            
      GOTO 101                                                          
C     *** HEHA HTEPO B CAE N<3 ***                        
  111 IF(N.EQ.2.OR.N.EQ.3) GOTO 18                                      
      GOTO 101
   18 DO 20 I=2,N                                                       
      DO 20 IA=1,NA                                                     
      IF(XA(IA).GE.X(I-1).AND.XA(IA).LT.X(I)) GOTO 19
      GOTO 20                                                           
   19 TA(IA)=T(I-1)+(T(I)-T(I-1))/(X(I)-X(I-1))*(XA(IA)-X(I-1))         
   20 CONTINUE                                                          
C                                                                       
C+++   21 WRITE (6,22)                                                      
C+++   22 FORMAT(/1X,'N<4  POBOTC HEHA HTEPO'/)            
C                                                                       
  101 CONTINUE                                                          
C     *** EAT PETATOB APOKCMA ***                          
C+++      WRITE(6,23)                                                       
C+++   23 FORMAT(/1X,'PETAT OHOMEPHO CAH-APOKCMA'/)          
C+++      WRITE(6,24)                                                       
C+++   24 FORMAT(3X,'X',5X,'T',5X,'X',5X,'T',5X,'X',5X,'T',                 
C+++     #5X,'X',5X,'T',5X,'X',5X,'T'/)                                     
C+++      WRITE(6,25)(XA(I),TA(I),I=1,NA)                                   
C+++   25 FORMAT(10F6.2)                                                    
C                                                                       
C                                                                       
      RETURN                                                            
      END                                                               
