C
C     =============================================================
C     =                     CORRECT                               =
C     = ����ணࠬ�� ��।������ � ����� ���ࠢ�� �� ��஢����   =
C     = ५�� � ᪮���� ������த���� ���孥� ��� ࠧ१� =
C     =                 (��� ������ ���)                          =
C     =============================================================
C
C     � ����ணࠬ�� ��ᬠ�ਢ����� ��� ���樨
C     1). ���ࠢ�� �������� ⮫쪮 �� ५��, ����� � ���孥�
C         ��� ࠧ१� ��� �ᠤ���, (�.�. ��� �஬����筮�
C         �ਢ��������� �५�����饩 �࠭��� (�㭤�����))
C     2). ���ࠢ�� �������� � �� ५�� � �� �ᠤ��
C
C                        �室�� ��ࠬ����
C     *************************************************************
C     *                                                           *
C     *                                                           *
C     *                                                           *
C     *                                                           *
C     *************************************************************
C
C
C                        ��室�� ��ࠬ����
C     ************************************************************* 
C     * T(N) - real,  ������� �६��� �����࠭���� ����    *
C     *               � ��⮬ ���ࠢ��, /�/                      *
C     * N - integer,  �᫮ �������                            *
C     *************************************************************
C
      subroutine CORRECT(N,NPV,NST,T,L,
     &                   KPV,MPV,HPV,ZMIPV,VVPV,HFPV,VFPV,
     &                   KST,MST,HST,ZMIST,VVST,HFST,VFST,
     &                   KVG,VGL,RE1OS2)
C     
C     *** ���ᠭ�� �室��� ��ࠬ��஢ ***
      integer N
      integer NPV(N),NST(N)
      real T(N),L(N)
      integer KPV,KST,MPV(KPV),MST(KST)
      dimension HPV(KPV),ZMIPV(KPV),VVPV(KPV),HFPV(KPV),VFPV(KPV),
     &          HST(KST),ZMIST(KST),VVST(KST),HFST(KST),VFST(KST)
      integer KVG
      dimension VGL(2,10)
      integer RE1OS2
C
C     *** ���ᠭ�� ࠡ��� ��ࠬ��஢ ***
      integer PV,ST
      real HP,HS,ZMINP,ZMINS,VVP,VVS,HFP,HFS,VFP,VFS,VG,
     &     TCORP,TCORS,TCORR
C
      do 60 I=1,N
C
C     *** ��९�᢮���� ������ ࠡ�稬 ��ࠬ��ࠬ ***
      IPV=NPV(I)
      IST=NST(I)
C
      do 1 J=1,KPV
    1 if(IPV.eq.MPV(J)) PV=J
      do 2 J=1,KST
    2 if(IST.eq.MST(J)) ST=J
C  
      HP=HPV(PV)
      HS=HST(ST)
      ZMINP=ZMIPV(PV)
      ZMINS=ZMIST(ST)
      VVP=VVPV(PV)
      VVS=VVST(ST)
      if(RE1OS2.eq.1) goto 5
      HFP=HFPV(PV)
      HFS=HFST(ST)
      VFP=VFPV(PV)
      VFS=VFST(ST)
C
C     ***  �롮� �࠭�筮� ᪮��� � ����ᨬ��� �� ���� �����஢���� ***
    5 do 10 K=2,KVG
      if(L(I).ge.VGL(1,K-1).and.L(I).lt.VGL(1,K)) VG=VGL(2,K-1)
   10 continue
      if(L(I).lt.VGL(1,1)) VG=VGL(2,1)
      if(L(I).ge.VGL(1,KVG)) VG=VGL(2,KVG)
C
C     *** ��।������ ���ࠢ�� �� ५�� (� ��砥 ������⢨� �ᠤ���) ***
      if(RE1OS2.eq.1) goto 20
      goto 30
   20 TCORP=(HP-ZMINP)*sqrt(1-(VVP/VG)**2)/VVP
      TCORS=(HS-ZMINS)*sqrt(1-(VVS/VG)**2)/VVS
      TCORR=TCORP+TCORS
C
C     *** ��।������ ���ࠢ�� �� ५�� � �ᠤ�� ***
   30 if(RE1OS2.eq.2) goto 40
      goto 50
C     *** ���ࠢ�� � �㭪�� ���뢠 ***
   40 if(HFP.ge.ZMINP) TCORP=(HP-HFP)*sqrt(1-(VVP/VG)**2)/VVP+
     &                    (HFP-ZMINP)*sqrt(1-(VFP/VG)**2)/VFP
C     +++ �᫨ ��� ZMINP ������� �ᠤ��, � ��� "����������" � VFP +++
      if(HFP.lt.ZMINP) TCORP=(HP-ZMINP)*sqrt(1-(VVP/VG)**2)/VVP+
     &                     (ZMINP-HFP)*(sqrt(1-(VVP/VG)**2)/VVP-
     &                                  sqrt(1-(VFP/VG)**2)/VFP)
C     *** ���ࠢ�� �� �⠭��� �ਥ�� ***
      if(HFS.ge.ZMINS) TCORS=(HS-HFS)*sqrt(1-(VVS/VG)**2)/VVS+
     &                    (HFS-ZMINS)*sqrt(1-(VFS/VG)**2)/VFS
C     +++ �᫨ ��� ZMINS ������� �ᠤ��, � ��� "����������" � VFS +++
      if(HFS.lt.ZMINS) TCORS=(HS-ZMINS)*sqrt(1-(VVS/VG)**2)/VVS+
     &                     (ZMINS-HFS)*(sqrt(1-(VVS/VG)**2)/VVS-
     &                                  sqrt(1-(VFS/VG)**2)/VFS)
C     *** �㬬�ୠ� ���ࠢ�� ***
      TCORR=TCORP+TCORS
C
C     ***  ���⠭�� ���ࠢ�� �� �������� �६�� ***
   50 T(I)=T(I)-TCORR
C
   60 continue
C
C
      return
      end

