# Microsoft Developer Studio Generated NMAKE File, Format Version 4.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

!IF "$(CFG)" == ""
CFG=FELD16 - Win32 Debug
!MESSAGE No configuration specified.  Defaulting to FELD16 - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "FELD16 - Win32 Release" && "$(CFG)" != "FELD16 - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "FELD16.mak" CFG="FELD16 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "FELD16 - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "FELD16 - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "FELD16 - Win32 Debug"
RSC=rc.exe
F90=fl32.exe

!IF  "$(CFG)" == "FELD16 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
OUTDIR=.\Release
INTDIR=.\Release

ALL : "$(OUTDIR)\FELD16.exe"

CLEAN : 
	-@erase ".\Release\FELD16.exe"
	-@erase ".\Release\Correct.obj"
	-@erase ".\Release\Dgauss.obj"
	-@erase ".\Release\Spl1v2.obj"
	-@erase ".\Release\Main.obj"
	-@erase ".\Release\Lintf_w.obj"
	-@erase ".\Release\Lintf.obj"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Ox /I "Release/" /c /nologo
# ADD F90 /Ox /I "Release/" /c /nologo
F90_PROJ=/Ox /I "Release/" /c /nologo /Fo"Release/" 
F90_OBJS=.\Release/
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/FELD16.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /machine:I386
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)/FELD16.pdb" /machine:I386 /out:"$(OUTDIR)/FELD16.exe" 
LINK32_OBJS= \
	"$(INTDIR)/Correct.obj" \
	"$(INTDIR)/Dgauss.obj" \
	"$(INTDIR)/Spl1v2.obj" \
	"$(INTDIR)/Main.obj" \
	"$(INTDIR)/Lintf_w.obj" \
	"$(INTDIR)/Lintf.obj"

"$(OUTDIR)\FELD16.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "FELD16 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "$(OUTDIR)\FELD16.exe"

CLEAN : 
	-@erase ".\Debug\FELD16.exe"
	-@erase ".\Debug\Dgauss.obj"
	-@erase ".\Debug\Main.obj"
	-@erase ".\Debug\Lintf_w.obj"
	-@erase ".\Debug\Lintf.obj"
	-@erase ".\Debug\Spl1v2.obj"
	-@erase ".\Debug\Correct.obj"
	-@erase ".\Debug\FELD16.ilk"
	-@erase ".\Debug\FELD16.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Zi /I "Debug/" /c /nologo
# ADD F90 /Zi /I "Debug/" /c /nologo
F90_PROJ=/Zi /I "Debug/" /c /nologo /Fo"Debug/" /Fd"Debug/FELD16.pdb" 
F90_OBJS=.\Debug/
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/FELD16.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
# ADD LINK32 kernel32.lib /nologo /subsystem:console /debug /machine:I386
LINK32_FLAGS=kernel32.lib /nologo /subsystem:console /incremental:yes\
 /pdb:"$(OUTDIR)/FELD16.pdb" /debug /machine:I386 /out:"$(OUTDIR)/FELD16.exe" 
LINK32_OBJS= \
	"$(INTDIR)/Dgauss.obj" \
	"$(INTDIR)/Main.obj" \
	"$(INTDIR)/Lintf_w.obj" \
	"$(INTDIR)/Lintf.obj" \
	"$(INTDIR)/Spl1v2.obj" \
	"$(INTDIR)/Correct.obj"

"$(OUTDIR)\FELD16.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.for{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f90{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

################################################################################
# Begin Target

# Name "FELD16 - Win32 Release"
# Name "FELD16 - Win32 Debug"

!IF  "$(CFG)" == "FELD16 - Win32 Release"

!ELSEIF  "$(CFG)" == "FELD16 - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\Correct.for

"$(INTDIR)\Correct.obj" : $(SOURCE) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\Dgauss.for

"$(INTDIR)\Dgauss.obj" : $(SOURCE) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\Lintf.for

"$(INTDIR)\Lintf.obj" : $(SOURCE) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\Lintf_w.for

"$(INTDIR)\Lintf_w.obj" : $(SOURCE) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\Spl1v2.for

"$(INTDIR)\Spl1v2.obj" : $(SOURCE) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\Main.for

"$(INTDIR)\Main.obj" : $(SOURCE) "$(INTDIR)"


# End Source File
################################################################################
# Begin Source File

SOURCE=.\AM1_V06NF.dat

!IF  "$(CFG)" == "FELD16 - Win32 Release"

!ELSEIF  "$(CFG)" == "FELD16 - Win32 Debug"

!ENDIF 

# End Source File
# End Target
# End Project
################################################################################
