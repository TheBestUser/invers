# Microsoft Developer Studio Generated NMAKE File, Format Version 4.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Standard Graphics Application" 0x0108

!IF "$(CFG)" == ""
CFG=ray_w - Win32 Debug
!MESSAGE No configuration specified.  Defaulting to ray_w - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "ray_w - Win32 Release" && "$(CFG)" != "ray_w - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "ray_w.mak" CFG="ray_w - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ray_w - Win32 Release" (based on\
 "Win32 (x86) Standard Graphics Application")
!MESSAGE "ray_w - Win32 Debug" (based on\
 "Win32 (x86) Standard Graphics Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "ray_w - Win32 Debug"
RSC=rc.exe
MTL=mktyplib.exe
F90=fl32.exe

!IF  "$(CFG)" == "ray_w - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
OUTDIR=.\Release
INTDIR=.\Release

ALL : "$(OUTDIR)\ray_w.exe"

CLEAN : 
	-@erase ".\Release\ray_w.exe"
	-@erase ".\Release\Lint.obj"
	-@erase ".\Release\Runge.obj"
	-@erase ".\Release\Net.obj"
	-@erase ".\Release\Step.obj"
	-@erase ".\Release\Main2.obj"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Ox /I "Release/" /c /nologo /MWs
# ADD F90 /Ox /I "Release/" /c /nologo /MWs
F90_PROJ=/Ox /I "Release/" /c /nologo /MWs /Fo"Release/" 
F90_OBJS=.\Release/
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /win32
MTL_PROJ=/nologo /D "NDEBUG" /win32 
# ADD BASE RSC /l 0x419 /d "NDEBUG"
# ADD RSC /l 0x419 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/ray_w.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:windows /machine:I386 /nodefaultlib:"console.lib"
# ADD LINK32 kernel32.lib /nologo /subsystem:windows /machine:I386 /nodefaultlib:"console.lib"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:windows /incremental:no\
 /pdb:"$(OUTDIR)/ray_w.pdb" /machine:I386 /nodefaultlib:"console.lib"\
 /out:"$(OUTDIR)/ray_w.exe" 
LINK32_OBJS= \
	"$(INTDIR)/Lint.obj" \
	"$(INTDIR)/Runge.obj" \
	"$(INTDIR)/Net.obj" \
	"$(INTDIR)/Step.obj" \
	"$(INTDIR)/Main2.obj"

"$(OUTDIR)\ray_w.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "ray_w - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "$(OUTDIR)\ray_w.exe"

CLEAN : 
	-@erase ".\Debug\ray_w.exe"
	-@erase ".\Debug\Net.obj"
	-@erase ".\Debug\Main2.obj"
	-@erase ".\Debug\Runge.obj"
	-@erase ".\Debug\Step.obj"
	-@erase ".\Debug\Lint.obj"
	-@erase ".\Debug\ray_w.ilk"
	-@erase ".\Debug\ray_w.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE F90 /Zi /I "Debug/" /c /nologo /MWs
# ADD F90 /Zi /I "Debug/" /c /nologo /MWs
F90_PROJ=/Zi /I "Debug/" /c /nologo /MWs /Fo"Debug/" /Fd"Debug/ray_w.pdb" 
F90_OBJS=.\Debug/
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /win32
MTL_PROJ=/nologo /D "_DEBUG" /win32 
# ADD BASE RSC /l 0x419 /d "_DEBUG"
# ADD RSC /l 0x419 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/ray_w.bsc" 
BSC32_SBRS=
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib /nologo /subsystem:windows /debug /machine:I386 /nodefaultlib:"console.lib"
# ADD LINK32 kernel32.lib /nologo /subsystem:windows /debug /machine:I386 /nodefaultlib:"console.lib"
LINK32_FLAGS=kernel32.lib /nologo /subsystem:windows /incremental:yes\
 /pdb:"$(OUTDIR)/ray_w.pdb" /debug /machine:I386 /nodefaultlib:"console.lib"\
 /out:"$(OUTDIR)/ray_w.exe" 
LINK32_OBJS= \
	"$(INTDIR)/Net.obj" \
	"$(INTDIR)/Main2.obj" \
	"$(INTDIR)/Runge.obj" \
	"$(INTDIR)/Step.obj" \
	"$(INTDIR)/Lint.obj"

"$(OUTDIR)\ray_w.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.for{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

.f90{$(F90_OBJS)}.obj:
   $(F90) $(F90_PROJ) $<  

################################################################################
# Begin Target

# Name "ray_w - Win32 Release"
# Name "ray_w - Win32 Debug"

!IF  "$(CFG)" == "ray_w - Win32 Release"

!ELSEIF  "$(CFG)" == "ray_w - Win32 Debug"

!ENDIF 

################################################################################
# Begin Source File

SOURCE="\��� ���������\Work\Step.for"

"$(INTDIR)\Step.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE="\��� ���������\Work\Main2.for"

"$(INTDIR)\Main2.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE="\��� ���������\Work\Net.for"

"$(INTDIR)\Net.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE="\��� ���������\Work\Runge.for"

"$(INTDIR)\Runge.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE="\��� ���������\Work\Lint.for"

"$(INTDIR)\Lint.obj" : $(SOURCE) "$(INTDIR)"
   $(F90) $(F90_PROJ) $(SOURCE)


# End Source File
################################################################################
# Begin Source File

SOURCE=.\L2c_56.dat

!IF  "$(CFG)" == "ray_w - Win32 Release"

!ELSEIF  "$(CFG)" == "ray_w - Win32 Debug"

!ENDIF 

# End Source File
# End Target
# End Project
################################################################################
